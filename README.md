This [owncloud](https://owncloud.org/) app was developed for the CRC 882 "From Heterogeneities to Inequalities", situated at Bielefeld University from 2011 to 2015. 
The application was part of a larger owncloud based *Virtual Research Environment* (VRE) and provided CRE researchers with means to document their work using the [DDI](http://www.ddialliance.org/) metadata standard. 

A demo system is available: https://vre-demo.ub.uni-bielefeld.de/

This application is free software under the GNU Affero General Public License. However, It should be noted that this app was developed to work with [owncloud version 5](https://en.wikipedia.org/w/index.php?title=OwnCloud&oldid=806815642) and is not compatible with later owncloud releases due to changes in the app plugin system. 
