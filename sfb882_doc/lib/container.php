<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
class Container {
	
	/*
    Returns true if the user $username has access to the container $uid, false otherwise.
    A user has access if
    1) s/he owns the container
    OR
    2) Another user who is member of a common group owns the container and has its permission set to 'shared'
    */
    public static function has_access($username, $uid) {
        $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_units` WHERE `uid` = ?' );
        $result = $stmt->execute(array($uid));
        if ($result->numRows() < 1) {
            throw new Exception("uid ".$uid." does not exist");
        }
        $unit = $result->fetchRow();
        if ($unit['username'] == $username) {
            return true;
        }
        if ($unit['permission'] != "shared") {
            return false;
        }
        //user does not own the unit, but it is shared. Find out if its owner and the user are in a common group.
        $user_groups = OC_Group::getUserGroups($username);
        $owner_groups = OC_Group::getUserGroups($unit['username']);
        if (count(array_intersect($user_groups, $owner_groups)) > 0) {
            return true;
        }
        return false;
    }

	/*
     * duplicates a container (including potential input/undo values) and returns the uid of the created duplicate
     * @param $uid uid of the container that should be duplicated
     * @param $parent_uid desired parent uid of the duplicate
     * @username the OC user who is conducting the dublication (only for metadata purposes)
     * @new_name new name for the duplicate, null will keep the original name
     */
    public static function duplicate_container($uid, $parent_uid, $username, $new_name) {
		$stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_units` WHERE `uid` = ?');
		$result = $stmt->execute(array($uid));
		if ($result->numRows() < 1) {
            throw new Exception("No container found in database for uid $uid!");
        }
        $time = time();
        $row = $result->fetchRow();
        $container_name = $row['container_name'];
        if (!is_null($new_name)) {
			$container_name = $new_name;
		}
        $stmt = OCP\DB::prepare('INSERT INTO `*PREFIX*ddi_units` (`parent_uid`,`username`,`displayname`,`container_name`,`research_type`,`location`,`container_type`,`posx`,`posy`,`color`,`permission`,`creation_date`,`lm_date`,`lm_user`,`lm_type`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)' );
		$result = $stmt->execute(
			array(
				$parent_uid, 
				$row['username'],
				$row['displayname'],
				$container_name,
				$row['research_type'],
				$row['location'], 
				$row['container_type'],
				$row['posx'],
				$row['posy'],
				$row['color'],
				$row['permission'],
				$time,
				$time,
				$username,
				'DUPLICATE'
			)
		);
		$insertid = OCP\DB::insertid('*PREFIX*ddi_units');
		$container_type = $row['container_type'];
		if (!in_array($container_type, Container_Export::$non_content_types)) {
			$stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_data_'.$container_type.'` WHERE `parent_uid` = ?');
			$result = $stmt->execute(array($uid));
			while ($data_row = $result->fetchRow()) {
				$stmt = OCP\DB::prepare('INSERT INTO `*PREFIX*ddi_data_'.$container_type.'` (`parent_uid`,`field`,`value`,`lm_date`,`lm_user`,`lm_type`) VALUES(?,?,?,?,?,?)' );
				$stmt->execute(
					array(
						$insertid,
						$data_row['field'], 
						$data_row['value'],
						$data_row['lm_date'],
						$data_row['lm_user'],
						$data_row['lm_type']
					)
				);
			}
			$stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_undo_'.$container_type.'` WHERE `parent_uid` = ?');
			$result = $stmt->execute(array($uid));
			while ($undo_row = $result->fetchRow()) {
				$stmt = OCP\DB::prepare('INSERT INTO `*PREFIX*ddi_undo_'.$container_type.'` (`parent_uid`,`field`,`value`,`lm_date`,`lm_user`,`lm_type`) VALUES(?,?,?,?,?,?)' );
				$stmt->execute(
					array(
						$insertid,
						$undo_row['field'], 
						$undo_row['value'],
						$undo_row['lm_date'],
						$undo_row['lm_user'],
						$undo_row['lm_type']
					)
				);
			}
			$stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_files` WHERE `parent_uid` = ?');
			$result = $stmt->execute(array($uid));
			while ($file_row = $result->fetchRow()) {
				$stmt = OCP\DB::prepare('INSERT INTO `*PREFIX*ddi_files` (`parent_uid`,`field`,`chksum`,`file_name`,`file_alias`,`lm_user`,`lm_date`,`public`,`active`,`file_origin`,`creation_date`,`description`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)' );
				$stmt->execute(
					array(
						$insertid,
						$file_row['field'], 
						$file_row['chksum'],
						$file_row['file_name'],
						$file_row['file_alias'],
						$file_row['lm_user'],
						$file_row['lm_date'],
						$file_row['public'],
						$file_row['active'],
						$file_row['file_origin'],
						$file_row['creation_date'],
						$file_row['description']
					)
				);
			}
		}
		return $insertid;
	}
	
	/*
     * Recursive dublication of a container and all its children
     */
    public static function recursive_duplicate($uid, $parent_uid, $username, $new_name) {
		$new_uid = Container::duplicate_container($uid, $parent_uid, $username, $new_name);
		//get the children
		$stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_units` WHERE `parent_uid` = ?');
		$result = $stmt->execute(array($uid));
		$children = array();
		while ($row = $result->fetchRow()) {
			$children[] = $row;
		}
		foreach ($children as $child) {
			Container::recursive_duplicate($child['uid'], $new_uid, $username, null);
		}
		return $new_uid;
	}
	
	/*
	 * creates a new container and all its children recursively
	 */
	public static function create_container($parent_uid, $username, $displayname, $container_name, $research_type, $location, $container_type, $posx, $posy, $color, $permission, $time) {
		if (!is_null($parent_uid)) { //subcontainer case
			$stmt = OCP\DB::prepare('INSERT INTO `*PREFIX*ddi_units` (`parent_uid`,`username`,`displayname`,`container_name`,`research_type`,`location`,`container_type`,`posx`,`posy`,`color`,`permission`,`creation_date`,`lm_date`,`lm_user`,`lm_type`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)' );
			$result = $stmt->execute(array($parent_uid, $username, $displayname, $container_name, $research_type, $location, $container_type, $posx, $posy, $color, $permission, $time, $time, $username, 'INSERT'));
		}
		else { //new top level container
			$stmt = OCP\DB::prepare('INSERT INTO `*PREFIX*ddi_units` (`username`,`displayname`,`container_name`,`research_type`,`location`,`container_type`,`posx`,`posy`,`color`,`creation_date`,`lm_date`,`lm_user`,`lm_type`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)' );
			$result = $stmt->execute(array($username, $displayname, $container_name, $research_type, $location, $container_type, $posx, $posy, $color, $time, $time, $username, 'INSERT'));
		}
		$insertid = OCP\DB::insertid('*PREFIX*ddi_units');
		//Load the assigned HTML to check for auto-create children
		$template_dom = Util::generate_template($container_type);
		$divs = $template_dom->getElementsByTagName('div');
		foreach ($divs as $div) {
			if ($div->hasAttribute('data-autocreate-children')) {
				$autocreate_children = explode(" ", $div->getAttributeNode('data-autocreate-children')->value);
				foreach ($autocreate_children as $child_type) {
					Container::create_container($insertid, $username, $displayname, $child_type, $research_type, 'fixed', $child_type, $posx, $posy, $color, $permission, $time);
				}
				break;
			}
		}	
		return $insertid;
	}
}
