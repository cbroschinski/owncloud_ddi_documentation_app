<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
Handles undo/redo. Note that these functions won't perform any
parameter checking (container_ownership...), so it is up to the calling ajax interface to do that.
*/
class Undo_Manager {
    
    public static function add_undo_value($container_type, $uid, $field_id, $value, $lm_time, $lm_user, $lm_type) {
        $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_undo_'.$container_type.'` WHERE `parent_uid` = ? AND `field` = ?');
        $result = $stmt->execute(array($uid, $field_id));  

        while ($row = $result->fetchRow()) {
	        if ($row['value'] == $value)
            //value already exists in the undo table, nothing to do here.
            return;
        }
        $stmt = OCP\DB::prepare('INSERT INTO `*PREFIX*ddi_undo_'.$container_type.'` (`parent_uid`,`field`,`value`, `lm_date`, `lm_user`, `lm_type`) VALUES(?,?,?,?,?,?)');
        $result = $stmt->execute(array($uid, $field_id, $value, $lm_time, $lm_user, $lm_type));  
    }
    
    public static function get_undo_value($uid, $field_id, $value) {
        $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_units` WHERE `uid` = ?');
        $result = $stmt->execute(array($uid)); 
        $row = $result->fetchRow();
        $container_type = $row['container_type'];
        
        $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_undo_'.$container_type.'` WHERE `parent_uid` = ? AND `field` = ?');
        $result = $stmt->execute(array($uid, $field_id)); 
        $last_entry = $value;
        while ($row = $result->fetchRow()) {
	        if ($row['value'] == $value) {
                break; 
            }
            $last_entry = $row['value'];
        }
        return $last_entry;
    }

    public static function get_redo_value($uid, $field_id, $value) {
        $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_units` WHERE `uid` = ?');
        $result = $stmt->execute(array($uid)); 
        $row = $result->fetchRow();
        $container_type = $row['container_type'];
        
        $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_undo_'.$container_type.'` WHERE `parent_uid` = ? AND `field` = ?');
        $result = $stmt->execute(array($uid, $field_id)); 
   
        while ($row = $result->fetchRow()) {
	        if ($row['value'] == $value) {
                if ($next_entry = $result->fetchRow()) {
                    return $next_entry['value'];
                }
                else return $value;
            }
        }
        return $value;
    }
    /*
    Checks if a container has a corresponding undo table. Will only find tables that follow the naming scheme
    'oc_ddi_undo'.$container_type 
    */
    public static function has_undo_table($uid) {
        $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_units` WHERE `uid` = ?' );
        $result = $stmt->execute(array($uid));
        if ($result->numRows() < 1) {
            throw new Exception("uid ".$uid." does not exist");
        }  
        $row = $result->fetchRow();
        $container_type = $row['container_type'];   
        $table_name = 'oc_ddi_undo_'.$container_type;
        $stmt = OCP\DB::prepare('SHOW TABLES');
        $result = $stmt->execute();
        while ($row = $result->fetchRow()) {
            if (in_array($table_name, $row)) {
                return true;
            }
        }
        return false;
    }

    /*
    Returns the history for a container and all its children (recursively), ordered by timestamp.
    The user must have access to a container to access its history, non-access will break recursion for that branch
    */
    public static function get_history($uid, $username) {
        $stmt = OCP\DB::prepare( 'SELECT * FROM `*PREFIX*ddi_units`');
        $result = $stmt->execute();
        $container_list = array();
        while($row = $result->fetchRow()) {
            $container_list[] = $row;
        }
        $recursion_list = Util::find_rows_by_field_value($container_list, 'uid', $uid);
        $undo_entries = array();
        while(count($recursion_list) > 0) {
            $current_container = array_shift($recursion_list);
            if (!Container::has_access($username, $current_container['uid'])) {
                continue;
            }
            $children = Util::find_rows_by_field_value($container_list, 'parent_uid', $current_container['uid']);
            $recursion_list = array_merge($recursion_list, $children);
            if (Undo_Manager::has_undo_table($current_container['uid'])) {
                $stmt = OCP\DB::prepare( 'SELECT field, value, lm_date, lm_user, lm_type FROM `*PREFIX*ddi_undo_'.$current_container['container_type'].'` WHERE `parent_uid` = ?');
                $result = $stmt->execute(array($current_container['uid']));
                while($row = $result->fetchRow()) {
                    if (isset($row['lm_date']) && isset($row['lm_user']) && isset($row['lm_type'])) {
                        $row['container_name'] = $current_container['container_name'];
                        $undo_entries[] = $row;
                    }
                }
            }
        }
        usort($undo_entries, array("Undo_Manager", "timestamp_compare"));
        return $undo_entries;
    }

    //Custom compare function for "undo" DB entries (sorting by timestamp)
    static function timestamp_compare($a, $b) {
        $atime = intval($a['lm_date']);
        $btime = intval($b['lm_date']);
       
        if ($atime == $btime) {
            return 0;
        }
        return ($atime < $btime) ? 1 : -1;   
    }
}
