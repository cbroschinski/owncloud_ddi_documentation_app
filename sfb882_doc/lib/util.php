<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
class Util {

    //Whitelist of unit properties which may be changed recursively
    public static $recursive_property_wl = array('permission', 'research_type');
    
    //Useful function to search arrays of DB rows for specific field values
    public static function find_rows_by_field_value($container_list, $field, $value) {
        $return_list = array();
        foreach ($container_list as $row) {
            if ($row[$field] == $value) {
                $return_list[] = $row;
            }
        }
        return $return_list;
    }
    
    public static function recursive_property_update($username, $uid, $property, $value) {
        if (!in_array($property, self::$recursive_property_wl)) {
            throw new Exception("Property $property not whitelisted for recursive update!");
        }
        $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_units` WHERE `username` = ? OR `permission` = ?');
        $result = $stmt->execute(array($username, 'shared'));
        if ($result->numRows() < 1) {
            throw new Exception("No containers found!");
        }
        $container_list = array();
        while ($row = $result->fetchRow()) {
            $container_list[] = $row;
        }

        $starting_container = array_shift(Util::find_rows_by_field_value($container_list, 'uid', $uid));
        $recursion_list = array($starting_container);

        $results = array();
        while(count($recursion_list) > 0) {
            $current_container = array_shift($recursion_list);
            $children = Util::find_rows_by_field_value($container_list, 'parent_uid', $current_container['uid']);
            $recursion_list = array_merge($recursion_list, $children);
            $stmt = OCP\DB::prepare("UPDATE `*PREFIX*ddi_units` SET `$property`=? WHERE `uid`=?");
            $result = $stmt->execute(array($value, $current_container['uid']));
            $results[$current_container['uid']] = $value; 
        }
        return $results;
    }

    /*
    Generates a generic template for a given container type as a DOM Document
    */
    public static function generate_template($container_type) {
        $dummy_template = new OCP\Template('sfb882_doc', 'sfb882_doc_dummy');
        $params = array('uid' => '', 'container_name' => '');
        $template_name = 'sfb882_doc_' . $container_type . '_container';
        $template = $dummy_template->inc($template_name, $params);
        if (strlen($template) == 0) {
			throw new Exception("Template generation failed: No template loaded for container type'".$template_name."'");
		}
        $template = '<?xml version="1.0" encoding="ISO-8859-1"?>' . $template;
        $template = html_entity_decode($template);
        $template_dom = new DOMDocument();
        $template_dom->loadXML($template);
        $template_dom->normalizeDocument();
        return $template_dom;
    }

    /*
    Takes a DOMElement and recursively searches for a child element with an attribute $attribute_name which has
    the value $attribute_value
    If such an element is found, it is returned, otherwise the return value is null
    */
    public static function get_element_by_attribute_value($dom_element, $attribute_name, $attribute_value) {
        $dom_nodes = $dom_element->getElementsByTagName('*');
        foreach($dom_nodes as $node) {
            if ($node->hasAttribute($attribute_name)) {    
                if ($node->getAttribute($attribute_name) === $attribute_value) {
                    return $node;
                }
            }
        }
        return null;
    }

    /*
    Checks if a string represents a RGB color
    */
    public static function is_RGB_color($text) {      
         $pattern = '%\#[ABCDEF1234567890]{6}%i';
         if (preg_match($pattern, $text)) {
            return true;
         }
         return false;
    }

    /*
    Checks if a string represents a 'CSS-ish' pixel position, such as '123px' or '51.32px'
    */
    public static function is_px_position($text) {
        $pattern = '%[\d]+[\.[\d]+]?px%';
        if (preg_match($pattern, $text)) {
            return true;
         }
         return false;
    }

    /*
    Useful function for text slugification, directly taken from OC\Files\Mapper (where it's unfortunately declared private...)
    */
    public static function slugify($text)
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\\pL\d]+~u', '-', $text);
		// trim
		$text = trim($text, '-');
		// transliterate
		if (function_exists('iconv')) {
			$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		}
		// lowercase
		$text = strtolower($text);
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		if (empty($text))
		{
			// TODO: we better generate a guid in this case
			return 'n-a';
		}
		return $text;
	}
    
    /*
    This function was adapted from \OC_Helper. Unfortunately, the developers used their
    own version of file_exists, which does not seem to work for directories outside the
    OC filesystem
    */
    public static function buildNotExistingFileName($path, $filename) {
		if($path==='/') {
			$path='';
		}
		if ($pos = strrpos($filename, '.')) {
			$name = substr($filename, 0, $pos);
			$ext = substr($filename, $pos);
		} else {
			$name = $filename;
			$ext = '';
		}

		$newpath = $path . '/' . $filename;
        $newname = $filename;
		$counter = 2;
		while (file_exists($newpath)) {
			$newname = $name . ' (' . $counter . ')' . $ext;
			$newpath = $path . '/' . $newname;
			$counter++;
		}

		return array('newpath' => $newpath, 'newname' => $newname);
	}
}
