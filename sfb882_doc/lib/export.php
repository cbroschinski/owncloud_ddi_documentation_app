<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Handles all exporting of container contents to text files. Note that these functions won't perform any
parameter checking (container_ownership...), so it is up to the calling ajax interface to do that.
*/
class Container_Export {
    
    public static $css_file_name = 'docbook-xsl.css';

    public static $non_content_types = array('top_level', 'study', 'syntax_files', 'pretests');
    
    public static $export_type_order = array('top_level', 'general_information', 'study', 'pre_field_work', 'pretests', 'pretest', 'field_phase', 'after_field_phase', 'data_processing', 'syntax_files', 'syntax_file', 'evaluation');
    
    public static $export_extension_map = array(
		'pdf' => 'pdf',
		'ps' => 'ps',
		'epub' => 'epub',
		'xhtml' => 'html'
    );
    
    public static $container_special_templates = array('pretest' => 'sfb882_doc_ddi_template_pretest'); 

    /*
    Lookup structure to tell if a container should be visible depending on its container_type and research_type
    (in_array => visible)
    */
    public static $research_type_visibility_restrictions = array(
		'pretest' => array(
			'quantitative_with_pretest', 
			'qualitative_with_pretest',
			'mixed_with_pretest'
		),
		'syntax_files' => array(
			'quantitative_with_pretest',
			'quantitative_without_pretest',
			'mixed_with_pretest',
			'mixed_without_pretest'    
		)
    );

    private static function find_rows_by_field_value($container_list, $field, $value) {
        $return_list = array();
        foreach ($container_list as $row) {
            if ($row[$field] == $value) {
                $return_list[] = $row;
            }
        }
        return $return_list;
    } 
    
    /*
     * Custom compare function to sort containers based on the predefined $export_type_order
     */
    static function container_type_compare($a, $b) {
        $a_priority = array_search($a['container_type'], self::$export_type_order);
        $b_priority = array_search($b['container_type'], self::$export_type_order);
       
        if ($a_priority == $b_priority) {
            return 0;
        }
        //Uncategorized container types will be moved to the end of the list
        if ($a_priority === FALSE) {
			return 1;
		}
		else if ($b_priority === FALSE) {
			return -1;
		}
        return ($a_priority < $b_priority) ? -1 : 1;   
    }

    public static function get_container_list($username) {
        $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_units` WHERE `username` = ? AND `status` = ?;');
        $result = $stmt->execute(array($username, 'active'));
        
        if ($result->numRows() < 1) {
            //Shouldn't happen if the calling function did its job properly...
            throw new Exception("No units found for username " . $username);
        }
        $container_list = array();

        while ($row = $result->fetchRow()) {
	        $container_list[] = $row;
        }   
        return $container_list;
    }
    
    
    public static function build_file_path($container_row, $file_row, $mask_whitespaces) {
		$raw_path = $container_row['container_name'] . '_' . $container_row['uid'].'/'.$file_row['field'].'/'.$file_row['file_name'];
		if ($mask_whitespaces) {
			return str_replace(' ', '%20', $raw_path);
		}
		return $raw_path;
	}

    public static function create_zip_archive($converted_file, $uid, $username) {
        $zipfile = new ZipArchive();
        
        $info = pathinfo($converted_file);
        $basename = basename($converted_file,'.'.$info['extension']);
        $zipfile_name = $basename.'.zip';
        \OC\Files\FileSystem::touch($zipfile_name);
        $zipfile_path = \OC\Files\FileSystem::getLocalFile($zipfile_name);
		
		if ($zipfile->open($zipfile_path, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE) !== true) {
			throw new Exception('Could not create new zip archive at path "' .$zipfile_path. '"');
		}
        else {
            $converted_file_path = \OC\Files\FileSystem::getLocalFile($converted_file);
            OCP\Util::writeLog('sfb882_doc', "Zipfile Export: Adding documentation file '$converted_file_path' to zip archive path ' $converted_file'", OCP\Util::DEBUG);
            $zipfile->addFile($converted_file_path, $converted_file);
        }
        //In case of html, add the generated CSS file  
        if ($info['extension'] === 'html') {
			$css_file = \OC\Files\FileSystem::getLocalFile(self::$css_file_name);
			if ($css_file !== null) {
				OCP\Util::writeLog('sfb882_doc', "Zipfile Export: Adding additional CSS file '$css_file' to zip archive", OCP\Util::DEBUG);
				$zipfile->addFile($css_file, self::$css_file_name);
			}
		}
        $container_list = Container_Export::get_container_list($username);
        $starting_container = array_shift(Util::find_rows_by_field_value($container_list, 'uid', $uid));
        $recursion_list = array($starting_container);
        while(count($recursion_list) > 0) {
            $current_container = array_shift($recursion_list);
            $children = Util::find_rows_by_field_value($container_list, 'parent_uid', $current_container['uid']);
            $recursion_list = array_merge($children, $recursion_list);
            $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_files` WHERE `parent_uid` = ? AND `active` = ? AND `public` = ?;');
            $result = $stmt->execute(array($current_container['uid'], 1, 1));
            while ($row = $result->fetchRow()) {
                /*
                This gets a bit tricky, because we have to map two different types of identifiers (uid/field name/chksum => filepath)
                - Since container names are not unique, we have to incorporate the uids into file paths
                - It is possible to store 2 different files with the same name in a file field, which is handled by modifying the file name
                (adding a '(n)') and bookkeeping the difference between 'file_name' (internal) and 'file_alias' (external). For the moment,
                we simply export the internal name as a workaround.
                */
                $current_file_path = Container_Export::build_file_path($current_container, $row, false);
	            $dest = 'files/' . $current_file_path;
                $source = SFB882_FILE_STORAGE_PATH .'/' . $row['file_name'];
                if (is_file($source)) {
                    OCP\Util::writeLog('sfb882_doc', "Zipfile Export: Adding support file '$source' to zip archive path '$dest'", OCP\Util::DEBUG);
					$zipfile->addFile($source, iconv('UTF-8', 'IBM850', $dest)); 
				}
				else {
					OCP\Util::writeLog('sfb882_doc', "Zipfile Export: Could not add file '$source' to zip archive path '$dest'", OCP\Util::ERROR);
				}
            }
        }
        $filename = basename($zipfile->filename);
        $zipfile->close();
        return $filename;    
    }
    
    //TODO: incorporate export_scope
    public static function export_to_plaintext($uid, $username, $export_scope, $link_to_files) {      
        $container_list = Container_Export::get_container_list($username);
        $starting_container = array_shift(Util::find_rows_by_field_value($container_list, 'uid', $uid));
        $starting_container['recursion_level'] = '=';
        $recursion_list = array($starting_container);
        $time_string = date('d_m_y_H_i_s');
        $LB_HYPHEN = PHP_EOL . '- ';
        
        $text_document = "Forschungsdatendokumentation".PHP_EOL."============================".PHP_EOL; //TODO: l10n!
        $text_document.= ":lang: de".PHP_EOL.":encoding: UTF-8".PHP_EOL.PHP_EOL; 
        $l=OC_L10N::get('sfb882_doc');
        
        while(count($recursion_list) > 0) {
            $current_container = array_shift($recursion_list);
            $children = Util::find_rows_by_field_value($container_list, 'parent_uid', $current_container['uid']);
            usort($children, array("Container_Export", "container_type_compare"));
            foreach ($children as &$child) {
				$child['recursion_level'] = $current_container['recursion_level'] . '=';
			}
            $recursion_list = array_merge($children, $recursion_list);
            $container_type = $current_container['container_type'];
            $research_type = $current_container['research_type'];
            if (array_key_exists($container_type, self::$research_type_visibility_restrictions)) {
                if (!in_array($research_type, self::$research_type_visibility_restrictions[$container_type])) {
                    continue;
                }  
            }
            if (strlen($current_container['recursion_level']) > 1) { //Omit title of top-level container
				$title_string = $current_container['recursion_level'].' '.$current_container['container_name'].PHP_EOL;
			}
            if (in_array($container_type, self::$non_content_types)) {
				$text_document .= $title_string;
                continue;
            }
            $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_data_'.$container_type.'` WHERE `parent_uid` = ? ');
            $data_result = $stmt->execute(array($current_container['uid']));
            $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_files` WHERE `parent_uid` = ? AND `active` = ? ');
            $files_result = $stmt->execute(array($current_container['uid'], 1));
            if ($data_result->numRows() + $files_result->numRows() < 1) {
                continue;
            }
            $entries = array();
            while ($row = $data_result->fetchRow()) {
                //OC uses vsprintf in the t() function, so we have to make sure that string params don't contain '%'s.
                if (strpos($row['value'], '%') === FALSE) {     
                    $value = (string)$l->t($row['value']);
                }
                else {
                    $value = $row['value'];
                }
                //Merge multi-value fields into a single item
                if (array_key_exists($row['field'], $entries)) {    
                    //Start a list by adding linebreak + hyphen to the first element
                    if (strpos($entries[$row['field']], $LB_HYPHEN) !== 0) {
                        $entries[$row['field']] = $LB_HYPHEN . $entries[$row['field']];
                    }
                    $entries[$row['field']] = $entries[$row['field']] . $LB_HYPHEN . $value;
                }
                else {
                    $entries[$row['field']] = $value; 
                }
            }
            while ($row = $files_result->fetchRow()) {
                $value = $row['file_alias'];
                if ($link_to_files && ($row['public'] === '1')) {
					$value = 'link:files/' . Container_Export::build_file_path($current_container, $row, true) . '[' . $row['file_alias'] . ']';
				}
                if (array_key_exists($row['field'], $entries)) {
                    if (strpos($entries[$row['field']], $LB_HYPHEN) !== 0) {
                        $entries[$row['field']] = $LB_HYPHEN . $entries[$row['field']];
                    }
                    $entries[$row['field']] = $entries[$row['field']] . $LB_HYPHEN . $value;
                }
                else {
                    $entries[$row['field']] = $value; 
                }
                if (strlen($row['description']) > 1) {
                    $entries[$row['field']] .= PHP_EOL . '** '. $row['description']; 
                }
            }
            $text_document .= $title_string;
            foreach ($entries as $key => $value) {
                if (strpos($key, '%') === FALSE) {
                    $item = (string)$l->t($key);
                }
                else {
                    $item = $key;
                }
                $text_document .= $item . ":: " . $value . PHP_EOL;
            }
            $text_document .= PHP_EOL;    
        }  
        $document_name = $starting_container['container_name'];

        $filepath = Util::slugify($document_name) . '_' . $time_string . ".txt";
        if (OC\Files\Filesystem::file_put_contents($filepath, $text_document)) {
            return $filepath;
        }
        else throw new Exception('Could not write to text file "'.$filepath.'"');
    }

    public static function convert_plaintext($filepath, $export_format) {      
        $local_file = OC\Files\Filesystem::getLocalFile($filepath);
        $a2x_opts = ' --dblatex-opts "-P latex.output.revhistory=0 -P doc.publisher.show=0" '; //no revision history, no dblatex logo
        $command = 'a2x -v -L -f '.$export_format. $a2x_opts .$local_file. ' 2>&1';
        exec($command, $out, $retval);
        if (!$retval) {
            $info = pathinfo($local_file);
            $basename = basename($local_file,'.'.$info['extension']); //extension not necessarily .txt
            $converted_file = $basename.'.'. self::$export_extension_map[$export_format]; //ugly, but a2x does not return the outfile directly
            return $converted_file;
        }
        else throw new Exception('a2x error: Could not convert file "'.$filepath.'" to format '.$export_format);
    }

    public static function is_absolute_path($xpath) {
         //concatenation of at least one '/foo'
         $pattern = '%^(/[\:[:alnum:]]+)+$%i';
         if (preg_match($pattern, $xpath)) {
            return true;
         }
         return false;
    }

    public static function is_relative_path($xpath) {
        //Same as absolute but starts with a dot
        $pattern = '%^\.(/[\:[:alnum:]]+)+$%i';
         if (preg_match($pattern, $xpath)) {
            return true;
         }
         return false;
    }

    public static function find_tag_in_DOMNodeList($tagname, $domnodelist) {
        for ($i = 0; $i < $domnodelist->length; $i++) {
            $node = $domnodelist->item($i);
            if (get_class($node) === 'DOMElement') {
                if ($node->tagName === $tagname) {
                    return $node;
                }
            }
        }
        return null;
    }
    
    public static function generate_wrapped_node($wrapper, $placeholder, $value, $uid, $container_uid) {
		$ret = str_replace('{', '<', $wrapper);
		$ret = str_replace('}', '>', $ret);
		$ret = str_replace($placeholder, $value, $ret);
		$ret = str_replace('%STUDY_UID%', $uid, $ret);
		$ret = str_replace('%CONTAINER_UID%', $container_uid, $ret);
		$ret = str_replace('%CHKSUM%', md5($value), $ret);
		return $ret.PHP_EOL;
	}
	
	public static function recursive_remove_empty_children(&$dom_node) {
		$children = $dom_node->childNodes;
		for ($i = $children->length-1; $i >= 0; $i--) {
			$child = $children->item($i);
			if ($child->hasChildNodes()) {
				Container_Export::recursive_remove_empty_children($child);
			}
			if ($child->nodeType === XML_TEXT_NODE) {
				//There might be whitespace fragments from the template which will form text nodes
				if (trim($child->nodeValue) == '') {
					$child->parentNode->removeChild($child);
				}
			}
			else if ($child->nodeType === XML_ELEMENT_NODE) {
				if (!$child->hasChildNodes()) {
					$child->parentNode->removeChild($child);
				}
				if ($child->childNodes->length == 1 && $child->childNodes->item(0)->nodeName === 'r:URN') {
					$child->parentNode->removeChild($child);
				}
			}
		}
	}
    
    public static function generate_ddi_mapping($template_dom) {
        $mapping = array();
        $inputs = $template_dom->getElementsByTagName('*');
        foreach ($inputs as $input) {
			if (($input->nodeName === 'input' && !$input->hasAttribute('type')) || $input->nodeName === 'textarea') {
				$input_key = $input->getAttributeNode('class')->value;
			}
			else if ($input->hasAttribute('data-checkbox-area-id')) {
				$input_key = $input->getAttributeNode('data-checkbox-area-id')->value;
			}
            if ($input->hasAttribute('data-ddi-placeholder')) {
                $mapping[$input_key]['placeholder'] = $input->getAttributeNode('data-ddi-placeholder')->value;
            }
            if ($input->hasAttribute('data-ddi-wrapper')) {
                $mapping[$input_key]['wrapper'] = $input->getAttributeNode('data-ddi-wrapper')->value;
            }
        }
        return $mapping;
    }
    
    public static function add_ddi_elements($params, $container) {
		$l = OC_L10N::get('sfb882_doc');
		$container_type = $container['container_type'];
		$container_uid = $container['uid'];
		$study_uid = $params['%STUDY_UID%'];
		if (in_array($container_type, self::$non_content_types)) {
			return $params;
		}
		$template_dom = Util::generate_template($container_type);
		$ddi_mapping = Container_Export::generate_ddi_mapping($template_dom);
		$stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_data_'.$container_type.'` WHERE `parent_uid` = ? ');
		$result = $stmt->execute(array($container['uid'])); 
		$generated_params =  array();        
		while ($row = $result->fetchRow()) {
			$field = $row['field'];
			if (strpos($row['value'], '%') === FALSE) {     
				$value = (string)$l->t($row['value']);
			}
			else {
				$value = $row['value'];
			}
			$value = htmlspecialchars($value);
			if (array_key_exists($field, $ddi_mapping) && array_key_exists('placeholder', $ddi_mapping[$field])) {
				$placeholders = explode(' ', $ddi_mapping[$field]['placeholder']);
				if (array_key_exists('wrapper', $ddi_mapping[$field])) {
					$wrappers = explode('%%%', $ddi_mapping[$field]['wrapper']);
					if (count($wrappers) != count($placeholders)) {
						$a = count($wrappers);
						$b = count($placeholders);
						throw new Exception("Error processing element $field in template $container_type: number of ddi placeholders does not match number of ddi wrappers ($a != $b)!");
					}
					for ($i = 0; $i < count($placeholders); ++$i) {
						$wrapped_node = Container_Export::generate_wrapped_node($wrappers[$i], $placeholders[$i], $value, $study_uid, $container_uid);
						if (array_key_exists($placeholders[$i], $generated_params)) {
							$generated_params[$placeholders[$i]] .= $wrapped_node;
						}
						else {
							$generated_params[$placeholders[$i]] = $wrapped_node;
						}
					}
				}
				else {
					for ($i = 0; $i < count($placeholders); ++$i) {
						if (array_key_exists($placeholders[$i], $generated_params)) {
							$generated_params[$placeholders[$i]] .= $value;
						}
						else {
							$generated_params[$placeholders[$i]] = $value;
						}
					}
				}
			}
		}
		// If there's no specific template for this container type, just return the generated parameters
		if (!array_key_exists($container_type, self::$container_special_templates)) {
			return array_merge($params, $generated_params);
		}
		else {
			$template_name = self::$container_special_templates[$container_type];
			$template_path = OC_App::getAppPath('sfb882_doc')."/templates/".$template_name.'.php';
			if (is_file($template_path)) {
				//Return those params which are not used in the template. This may occur if a template needs to refer to outside entities (i.e. Archive) 
				$template_content = file_get_contents($template_path);
				foreach ($generated_params as $key=>$value) {
					if (strpos($template_content, $key) === FALSE) {
						if (array_key_exists($key, $params)) {
							$params[$key] .= $value;
						}
						else {
							$params[$key] = $value;
						}
					}
				}
			}
			$generated_params['%STUDY_UID%'] = $study_uid;
			$generated_params['%CONTAINER_UID%'] = $container['uid'];
			$container_template = new OCP\Template('sfb882_doc', $template_name);
			$template = $container_template->inc($template_name, $generated_params);
			$template = html_entity_decode($template, ENT_QUOTES, 'UTF-8');
			//A template may be included more than one time
			if (array_key_exists($template_name, $params)) {
				$params[$template_name] .= $template;
				return $params;
			}
			return array_merge($params, array($template_name => $template));
		}
	} 

    public static function export_to_ddi_3_2($uid, $username) {
        $container_list = Container_Export::get_container_list($username);
        $starting_container = array_shift(Util::find_rows_by_field_value($container_list, 'uid', $uid));
        if ($starting_container['container_type'] != 'study') {
			throw new Exception("DDI Export is only supported for studies!");
		}
		$recursion_list = array($starting_container);
		
		//Find the general_information sibling and prepend it
		$top_level_parent = array_shift(Util::find_rows_by_field_value($container_list, 'uid', $starting_container['parent_uid']));
		$tlp_uid = $top_level_parent['uid'];
		$tlp_children = Util::find_rows_by_field_value($container_list, 'parent_uid', $tlp_uid);
		foreach($tlp_children as $child) {
			if ($child['container_type'] === 'general_information') {
				array_unshift($recursion_list, $child);
				break;
			}
		}
		
		$uid = $starting_container['uid'];
        
        $params = array('%STUDY_UID%' => $uid);
        
        while(count($recursion_list) > 0) {
            $current_container = array_shift($recursion_list);
            $children = Util::find_rows_by_field_value($container_list, 'parent_uid', $current_container['uid']);
            $recursion_list = array_merge($recursion_list, $children);
            $params = Container_Export::add_ddi_elements($params, $current_container); 
        }
        $ddi_template = new OCP\Template('sfb882_doc', 'sfb882_doc_ddi_template_main');
        $template_name = 'sfb882_doc_ddi_template_main';
        $template = $ddi_template->inc($template_name, $params);
        $template = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL. $template;
        $template = html_entity_decode($template, ENT_QUOTES, 'UTF-8');
        $ddi = new DOMDocument();   
        $ddi->loadXML($template);
        $ddi->formatOutput = true;
        Container_Export::recursive_remove_empty_children($ddi);
        $xml = $ddi->saveXML($ddi->documentElement);
        
        $document_name = $starting_container['container_name'];
        $time_string = date('d_m_y_H_i_s');
        $filepath = Util::slugify($document_name) . '_' . $time_string . ".xml";
        if (OC\Files\Filesystem::file_put_contents($filepath, $xml)) {
            return $filepath;
        }
        else throw new Exception('Could not write to file "'.$filepath.'"');
    }
}
