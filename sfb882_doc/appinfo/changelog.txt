Changelog since 27.11.13
- Kalender-Widget für Felder mit Datumseingaben hinzugefügt
- Tooltip für Container in Share und Trash
- History-Button für Container mit rekursiver Bearbeitungshistorie
- Pub-Suche für Eingaben in Publikations-Feld
- Pevz-Suche für Eingaben bei Mitarbeitern/Gruppenleitung
- Hinzufügen von eigenen Einträgen zu Checkbox-Gruppen
- Mehrfacheinträge für bestimmte Felder
- DDI-3.1-Export (Testphase)
- Erweiterte Exportformate: PDF, PS, EPUB
- Eingabefelder können jetzt wahlweise ein-/mehrzeilig sein (Inputfield vs Textarea)
- Farbe/Name von Containern können jetzt nachträglich geändert werden
- Zusätzliche Felder:
    Abschnitt "Nach der Feldphase": 
        "Berichte",
        "Evolution des Erhebungsinstruments",
        "Fallzahl Nacherhebung",
        "Gründe für Nacherhebung"
- Neuer Feldtyp: Dateiablage. Dateien werden hier per DnD persistent außerhalb des Cloud-Speichers abgelegt (Snapshot-Prinzip). Funktionweise:
    * Neue Datei ablegen: Blauen Datei-Drawer öffnen (zeigt eine Kopie des OC-Dateisystems), zur gewünschten Datei navigieren, per Drag-and-Drop
    auf das große Dateisymbol fallenlassen. Datei wird unabhängig von ihrem Original automatisch in der momentanen Version gespeichert.
    * Dateieinstellungen ändern: Unten in der Liste gewünschte Datei auswählen. Mögliche Operationen:
        ** Aktiv/Inaktiv: 'Aktive' Dateien werden als Bestandteil der Dokumentation angesehen (tauchen in der Feldbeschreibung auf),
         inaktive nicht (Hintergrund: Man kann Dateien als inaktiv vorrätig halten, ohne sie löschen zu müssen)
        ** Öffentlich/Nichtöffentlich: Aktive Dateien, die zugleich öffentlich sind, werden mitexportiert (s.u.)
        ** Download: Datei herunterladen (Wer hätte das gedacht...)
        ** Beschreibung: Kann einen beliebigen Kommentar zur Datei aufnehmen, wird mitexportiert
        ** Entfernen: Snapshot dauerhaft löschen (Irreversibel, zur Zeit noch inaktiv)
- Dateiexport: Auf Wunsch können auch Dateien mitexportiert werden, in diesem Fall wird ein ZIP-Archiv erzeugt, dass neben der Dokumentation auch
    alle 'öffentlichen' Dateien in einer beigefügten Ordnerstruktur enthält.
    
     
