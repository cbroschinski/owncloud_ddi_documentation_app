<?php
OC::$CLASSPATH['Container_Export'] = 'sfb882_doc/lib/export.php';
OC::$CLASSPATH['Util'] = 'sfb882_doc/lib/util.php';
OC::$CLASSPATH['Undo_Manager'] = 'sfb882_doc/lib/undo.php';
OC::$CLASSPATH['Container'] = 'sfb882_doc/lib/container.php';
$l = OC_L10N::get('sfb882_doc');
OCP\Util::addStyle('sfb882_doc', 'sfb882_doc_style');
OCP\Util::addStyle('sfb882_doc', 'sfb882_doc_drawer');
OCP\Util::addStyle('sfb882_doc', 'sfb882_doc_container_internal');
OCP\Util::addStyle('sfb882_doc', 'sfb882_doc_dialogs');
OCP\Util::addStyle('files', 'files');
OCP\Util::addscript('sfb882_doc', 'sfb882_doc_file_section');
OCP\Util::addscript('sfb882_doc', 'sfb882_doc_icons');
OCP\Util::addscript('sfb882_doc', 'sfb882_doc_container_internal');
OCP\Util::addscript('sfb882_doc', 'sfb882_doc');
OCP\Util::addscript('sfb882_doc', 'noty/js/noty/packaged/jquery.noty.packaged.min');
OCP\Util::addscript('sfb882_doc', 'FitText/jquery.fittext');

define('SFB882_FILE_STORAGE_PATH', '/var/sfb882_doc_files');

OCP\App::addNavigationEntry( array(
  'id' => 'sfb882_doc_index',
  'order' => 2,
  'href' => OCP\Util::linkTo( 'sfb882_doc', 'index.php' ),
  'icon' => OCP\Util::imagePath( 'sfb882_doc', 'sfb882_doc.svg' ),
  'name' => 'DDI',
  )
);
