/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
sfb882_doc_container_internal = {
    _qualitative_mandatory: ['qualitative_with_pretest', 'qualitative_without_pretest', 'mixed_with_pretest', 'mixed_without_pretest'],
    _quantiative_mandatory: ['quantitative_with_pretest', 'quantitative_without_pretest', 'mixed_with_pretest', 'mixed_without_pretest'],
    init_central_pane: function($central_pane) {
        var $trash_area = $central_pane.children(".trash_area");
        var $duplicate_area = $central_pane.children(".duplicate_area");
        var $export_area = $central_pane.children(".export_area");
        var $create_icon = $central_pane.children("#create_area").children(".subcontainer_create_icon");
        var $placeholder = $central_pane.children("#create_area").children(".subcontainer_create_icon_placeholder");
        var subcontainer_class = $central_pane.attr('data-subcontainer-class');
        var $container = $central_pane.closest(".container");	
        $create_icon.draggable({
            containment: $central_pane,
			scroll: false,
			revert: "invalid",
			delay: 5
		});
        if ($trash_area.length) {
            sfb882_doc_icons.init_trash($trash_area, subcontainer_class, $central_pane, $container.data('uid'));
        }
        if ($duplicate_area.length) {
			sfb882_doc_icons.init_duplicate($duplicate_area);
		}
		if ($export_area.length) {
			sfb882_doc_icons.init_export($export_area);
		}
        $central_pane.droppable({
            accept: ".subcontainer_create_icon, .in_central_pane",
            drop: function(event, ui) {
				//Several drop targets might be open, but only the closest is valid 
				if (!ui.draggable.closest(".central_pane").is($central_pane)) {
					return;
				}
                if (ui.draggable.hasClass("subcontainer_create_icon")) {
                    if (ui.draggable.is('[data-additional-options*="research_type"]')) {
                        $('#research_type_selection').show();
                        $('#pretest_conducted_selection').show();
                    }
					else {
						$('#research_type_selection').hide();
						$('#pretest_conducted_selection').hide();	
					}
		            $("#container_options_dialog").dialog({
					    height: 500,
					    width: 450,
                        modal: true,
                        title: t("sfb882_doc", "options_dialog_create_title"),
					    resizable: false,
					    buttons: [
					    {
						    text: t("sfb882_doc", "options_dialog_create"),
						    click: function() { 
							    var container_name = $("#container_name_input").val();
                                $("#container_name_input").val("");
							    var posx = ui.draggable.css("left");
							    var posy = ui.draggable.css("top");
							    var color = sfb882_doc.rgb_to_hex(	
								    $("#color_slider_red").slider("value"), 
								    $("#color_slider_green").slider("value"), 
								    $("#color_slider_blue").slider("value")
							    );
							    var research_type = $container.data("research_type");
							    if (ui.draggable.is('[data-additional-options*="research_type"]')) {
									research_type = $("input:radio[name=research_type_button_group]:checked").val() + "_" + $("input:radio[name=pretest_conducted_button_group]:checked").val();
								}
								sfb882_doc.create_container($container.data('uid'), container_name, research_type, "central_pane", subcontainer_class, posx, posy, color, function(result){
									sfb882_doc_container_internal.load_subcontainer(result.data, $container, 500);
									$create_icon.css({top: $placeholder.css("top"), left: $placeholder.css("left")});
								});
							    $(this).dialog("close");
						    }
					    },
					    {
						    text: t("sfb882_doc", "options_dialog_abort"),
						    click: function() {
							    $(this).dialog("close");
							    $create_icon.animate({top: $placeholder.css("top"), left: $placeholder.css("left")}, 200);
						    }
					    }]
				    });
                }
                else if (ui.draggable.hasClass("in_central_pane") && ui.draggable.hasClass("closed") && ui.draggable.parent) {
					var posx = ui.draggable.css("left");
					var posy = ui.draggable.css("top");
					var uid = ui.draggable.data("uid");
					$.ajax({
						url: OC.filePath('sfb882_doc', 'ajax', 'editUnit.php'),
						data: {'uid': uid, 'action': "MOVE", 'posx': posx, 'posy': posy},
						dataType: "json"
					});
				}
            },
        });
    },
    /*
    Initializes all internal container mechanics (Creation and DnD of sub-subcontainers, opening and closing of containers)
    */
    init: function(container) {
        /*
        Note: We cannot simply use a jquery class selector here (like '(".central_pane")'), because
        that would select the central area of _every_ existing container. We have to use DOM traversal (.children)
        in the current container.*/    
        var $outer_div = container.children(".container_outer_div");
        var $central_pane = $outer_div.children(".central_pane");
        if ($central_pane.length && $central_pane.is('[data-subcontainer-class]')) {
            sfb882_doc_container_internal.init_central_pane($central_pane);
        }
        if (container.hasClass("in_central_pane")) {
            container.draggable({
                containment: container.parent(".central_pane"),
	     	    scroll: false,
			    opacity: 0.35,
			    delay: 5,
			    zIndex: 10,
			    start: function(event, ui) {	
				    container.data({
						'start_x_pos': container.css('left'),
						'start_y_pos': container.css('top')
					});
			    },
            });
        }
        /*
        Make the container clickable (for opening) and the inner close button clickable (for closing)
        */
		container.click(function(event) {
			event.stopPropagation();
            var target = $(this);
            /* Only open a container if:
            - the container is not already open
            - there's no opening animation ongoing and 
            - no container on the same level is already open and
            - the container is not inside the trash bin recovery area
            */
			if (sfb882_doc.data.container_animation_ongoing == false && !target.hasClass("open") && target.siblings(".container.open").length == 0 && !target.hasClass("in_trash")) {
				sfb882_doc.data.container_animation_ongoing = true;
                if (target.data("uiTooltip")) {
                    target.tooltip("close");
                }
				var $outer_div = target.children(".container_outer_div");		
				var $children = $outer_div.children(".container[invisible!=true], .container_content, .container_sidebar, .container_pane, .horizontal-separator, .qualitative_box, .quantitative_box");
				$children = $children.add($outer_div.children(".container_title").children(".title_button"));
                $children = $children.add($outer_div.children(".container_pane").children(".container"));
				sfb882_doc_container_internal.grow_container(target, function() {
					sfb882_doc_container_internal.serial_fade(target, $children, 0, true);
                    $outer_div.children(".container_title").fitText(3.5, {minFontSize: "16px", maxFontSize: "24px"});
				});
			}
		});
        var close_button = container.children(".container_outer_div").children(".container_title").children(".close_button");
        close_button.click(function(event) {
            event.stopPropagation();
            var $parent_container = $(this).parent().parent().parent(".container");         
            if ($parent_container.hasClass('open') && sfb882_doc.data.container_animation_ongoing == false) {
                sfb882_doc.data.container_animation_ongoing = true;
                var $children = $parent_container.children(".container_outer_div").children(".container, .container_content, .container_sidebar, .container_pane, .horizontal-separator, .qualitative_box, .quantitative_box");
                $children = $children.add($parent_container.children(".container_outer_div").children(".container_title").children(".title_button"));
                $children = $children.add($parent_container.children(".container_outer_div").children(".container_pane").children(".container")); 
				if ($children.length > 0) {
					sfb882_doc_container_internal.serial_fade($parent_container, $children, 0, false);
				}
				else {
					sfb882_doc_container_internal.shrink_container($parent_container);
                    
				}							
            }
        });
        var history_button = container.children(".container_outer_div").children(".container_title").children(".history_button");
        history_button.click(function(event) {
            $('#history_panel').empty();
            $('#history_dialog').dialog({
                modal: true,
                width: 800
            });
            $.ajax({
                url: OC.filePath('sfb882_doc', 'ajax', 'getHistory.php'),
		        data: {'uid': container.data('uid')},
                dataType: "json",
		        success: function(result) {
                    $history_list = sfb882_doc_container_internal.generate_history_list(result.data);
                    $('#history_panel').append($history_list);
                }
            });
        });
        var options_button = container.children(".container_outer_div").children(".container_title").children(".options_button");
        options_button.click(function(event) {
            $("#container_name_input").val(container.data("container_name"));
            $("#color_slider_red").slider("value", sfb882_doc.hex_to_rgb(container.data("color")).r);
			$("#color_slider_green").slider("value", sfb882_doc.hex_to_rgb(container.data("color")).g);
			$("#color_slider_blue").slider("value", sfb882_doc.hex_to_rgb(container.data("color")).b);
            $("#example_container_title").text(container.data("container_name"));
            if (container.data('container_type') == 'study') {   
                $('#research_type_selection').show();
                $('#pretest_conducted_selection').show();
                var rt = container.data('research_type');
                $("input[name=research_type_button_group]").each(function(){
                    if (rt.indexOf($(this).attr('value')) > -1) {
                        $(this).prop('checked', true);
                        return false;
                    }
                });
                $("input[name=pretest_conducted_button_group]").each(function(){
                    if (rt.indexOf($(this).attr('value')) > -1) {
                        $(this).prop('checked', true);
                        return false;
                    }
                });
            }
            else {
                $('#research_type_selection').hide();
                $('#pretest_conducted_selection').hide();
            }
            $("#container_options_dialog").dialog({
				height: 500,
				width: 450,
                modal: true,
                title: t("sfb882_doc", "options_dialog_title"),
				resizable: false,
				buttons: [
				{
					text: t("sfb882_doc", "options_dialog_change"),
					click: function() {
						var unitname = $("#container_name_input").val();
                        if (unitname != container.data("container_name")) {
                            $.ajax({
                                url: OC.filePath('sfb882_doc', 'ajax', 'editUnit.php'),
	                            data: {'uid': container.data('uid'), 'action': 'RENAME', 'name': unitname},
                                dataType: "json",
	                            success: function(result) {
                                    var $title = container.children(".container_outer_div").children(".container_title");
                                    $title_elements = $title.children().clone(true);
                                    $title.text(unitname);
                                    $title.append($title_elements);
                                    container.data("container_name", unitname);
                                }
                            });
                        }
                        $("#container_name_input").val("");
						var color = sfb882_doc.rgb_to_hex(	
							$("#color_slider_red").slider("value"), 
							$("#color_slider_green").slider("value"), 
							$("#color_slider_blue").slider("value")
						);
                        if (color != container.data("color")) {
						    $.ajax({
                                url: OC.filePath('sfb882_doc', 'ajax', 'editUnit.php'),
	                            data: {'uid': container.data('uid'), 'action': 'RECOLOR', 'color': color},
                                dataType: "json",
	                            success: function(result) {
                                    container.animate({"background-color": color}, 1000);
                                    container.data("color", color);
                                }
                            });
                        }
                        if (container.data('container_type') == 'study') {
                            var research_type = $("input:radio[name=research_type_button_group]:checked").val()+ "_" + $("input:radio[name=pretest_conducted_button_group]:checked").val();
                            if (research_type != container.data('research_type')) {
                                $.ajax({
                                    url: OC.filePath('sfb882_doc', 'ajax', 'editUnit.php'),
                                    data: {'uid': container.data('uid'), 'action': 'SET_RESEARCH_TYPE_RECURSIVE', 'research_type': research_type},
                                    dataType: "json",
                                    success: function(result) {
                                        container.data('research_type', research_type);
                                        sfb882_doc_container_internal.set_research_title_icons(container);
                                        sfb882_doc_container_internal.update_children_by_research_type(container, result.data);
                                    }
                                });
                            }
                        }
						$(this).dialog("close");
					}
				},
				{
					text: t("sfb882_doc", "options_dialog_abort"),
					click: function() {
						$(this).dialog("close");
						var dummy = $("#icon_bar_pos_1_placeholder");
						$(".create_icon").animate({top: dummy.css("top"), left: dummy.css("left")}, 200);
					}
				}]
			});
        });

        //Init all static checkboxes in form containers
        container.children(".container_outer_div").children(".container_content").children(".content_input_checkbox_area").children(".checkbox_unit:not(.custom)").children("input").change(function(event) {
            var $field = $(this);
            var value = $field.attr("data-checkbox-id");
            if ($field.is(":checked")) {
                var action = "ADD";
            }
            else {
                var action = "DELETE";
            }
            var field_id = $(this).parent().parent(".content_input_checkbox_area").attr("data-checkbox-area-id");
            $.ajax({
                type: "POST",
                url: OC.filePath('sfb882_doc', 'ajax', 'saveField.php'),
		        data: {'field_id': field_id, 'uid': container.data("uid"), 'value': value, 'action': action},
                dataType: "json",
		        success: function(result) {
                    if (result.status == "error") {
                        sfb882_doc.notify(result.message, 10000);
                        return;
                    }
                    var $checkbox_area = $field.parent().parent(".content_input_checkbox_area");
                    var $title = $checkbox_area.prev(".content_title");
                    var title_text = sfb882_doc_container_internal.create_checkbox_title($checkbox_area);
                    sfb882_doc_container_internal.set_field_title($title, title_text);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    sfb882_doc.notify(errorThrown, 10000);
                }
            });
        });
        container.children(".container_outer_div").children(".progressbar_container").children(".progressbar").progressbar({
            max: 100
        });
        container.children(".container_outer_div").children(".container_title").fitText(1, {minFontSize: "16px", maxFontSize: "24px"});
        //Init datepicker widgets
        container.children(".container_outer_div").children(".container_content").children(".content_input.date").children("input").datepicker({
			dateFormat: "yy-mm-dd",
			constrainInput: true, 
            onClose: function(dateText, inst) {
                if (dateText.length > 0) {
                    $(this).siblings(".save").click();
                }
            }
        });
        //Init pub search
        var $pub_searchables = container.children(".container_outer_div").children(".container_content").children(".pub_search").find("input");
        $pub_searchables.autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: OC.filePath('sfb882_doc', 'ajax', 'pub_search.php'),
                    data: {
                        'query': request.term
                    },
                    dataType: "json",
                }).success(function(result){
                    if (result.data.length) {           
                        var obj = eval("(" + result.data + ")"); 
                        var results = $.map(obj, function(entry) {
                            return entry.citation;
                        });
                        response(results);   
                    }             
                });   
            },
            minLength: 3
        });
        //Init data search
        var $data_searchables = container.children(".container_outer_div").children(".container_content").children(".data_search").find("input");
        $data_searchables.autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: OC.filePath('sfb882_doc', 'ajax', 'data_search.php'),
                    data: {
                        'query': request.term
                    },
                    dataType: "json",
                }).success(function(result){
                    if (result.data.length) {           
                        var obj = eval("(" + result.data + ")"); 
                        var results = $.map(obj, function(entry) {
                            return entry.citation;
                        });
                        response(results);   
                    }             
                });   
            },
            minLength: 3
        });
        //Init pevz search
        var $pevz_searchables = container.children(".container_outer_div").children(".container_content").children(".pevz_search").find("input");
        $pevz_searchables.autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: OC.filePath('sfb882_doc', 'ajax', 'pevz_search.php'),
                    data: {
                        'org_id': '26155953',
                        'query': request.term
                    },
                    dataType: "json"
                }).success(function(result){
                    if (result.data.length) {              
                        response(result.data);
                    }             
                });   
            },
        });
        //Finally, mark the container as hidden if the research type is restricted and does not match
        sfb882_doc_container_internal.update_container_research_visibility(container, false);
    },
    update_container_research_visibility: function($container, apply_now) {
        if ($container.is('[data-research_types_only]')) {
            var rt = $container.data('research_type');
            if ($container.is('[data-research_types_only~="'+ rt +'"]')) {     
                $container.attr("invisible", "false");  //We set an attribute for easier filtering during open/close
                if (apply_now) {
                    $container.show();
                }      
            }
            else {
                $container.attr("invisible", "true"); 
                if (apply_now) {
                    $container.hide();
                }       
            }
        }                     
    },
    update_children_by_research_type: function($container, type_updates) {
        var $container_children = $container.find(".container");
        $container_children.each(function() {
            var uid = $(this).data('uid');
            if (uid in type_updates) {
                $(this).data('research_type', type_updates[uid]);
            }  
            sfb882_doc_container_internal.update_container_research_visibility($(this), true);
            sfb882_doc_container_internal.update_progressbar($(this));
        });
    },
    update_progressbar: function($container) {
        var $progressbar = $container.children(".container_outer_div").children(".progressbar_container").children(".progressbar");
        //Get the percentage from the ratio of filled entries.
        var total = 0;
        var filled = 0;
        $container.children(".container_outer_div").children(".container_content").children(".content_title").each(function(){
            $content = $(this).next(".content_input,.content_input_checkbox_area,.content_input_file_field");   
            $mand_quali = $content.filter("[data-mandatory*='*']").add($content.find($("[data-mandatory*='*']")));
            $mand_quanti = $content.filter("[data-mandatory*='+']").add($content.find($("[data-mandatory*='+']")));
            var rt = $container.data('research_type');
            if ($mand_quanti.length > 0 && sfb882_doc_container_internal._quantiative_mandatory.indexOf(rt) > -1) {
                total++;
                if ($(this).data("filled") == true) {
                    filled++;
                }
            }
            else if ($mand_quali.length > 0 && sfb882_doc_container_internal._qualitative_mandatory.indexOf(rt) > -1) {
                total++;
                if ($(this).data("filled") == true) {
                    filled++;
                }
            }
        });
        if (total > 0) {
            var ratio = Math.min(filled/total, 1);
        }
        //If there are no entries, search for 'progress-bared' subcontainers instead
        else {
            //That's some hilarious DOM traversal...
            //We select the class '.ui-progressbar' instead of 'progressbar' to get only the ones which have been initialized
            var $subcontainer_bars = $container.children(".container_outer_div").children(".container[invisible!=true]").children(".container_outer_div").children(".progressbar_container").children(".ui-progressbar");
            $subcontainer_bars = $subcontainer_bars.add($container.children(".container_outer_div").children(".container_pane").children(".container[invisible!=true]").children(".container_outer_div").children(".progressbar_container").children(".ui-progressbar"));
            if ($subcontainer_bars.length == 0) {
                return;
            }
            var total_value = 0;            
            $subcontainer_bars.each(function(){
                var val = $(this).progressbar("option", "value");
                total_value += val;
            });
            var average = total_value/$subcontainer_bars.length;
            var ratio = average / 100;
        } 
        var percentage = Math.round(ratio * 100);
        var red = Math.round((1-ratio) * 255);
        var green = Math.round(ratio * 255);
        $progressbar.progressbar({
            'max': 100,
            'value': percentage
        });
        $progressbar.children(".ui-progressbar-value").css({
            "background" : "rgb(" + red + ", " + green + ", 0)"
        });
        $progressbar.children(".progress_label").text(percentage + "%");
        $parent = sfb882_doc_container_internal.find_parent_container($container);
        if ($parent.length > 0) {
            sfb882_doc_container_internal.update_progressbar($parent);
        }
    },
    generate_history_list: function(result) {
        var $list = $('<ul></ul>');
        for (var i = 0; i < result.length; i++ ) {
            var entry = result[i];
            var item = "---";
            if (entry['lm_type'] == "INSERT") {
                item = t("sfb882_doc", "history_item_create");         
            }
            else if (entry['lm_type'] == "UPDATE") {
                item = t("sfb882_doc", "history_item_update");       
            }
            item = item.replace("%LM_DATE", entry['lm_date']);
            item = item.replace("%FIELD", entry['field']);
            item = item.replace("%VALUE", entry['value']);
            item = item.replace("%CONTAINER_NAME", entry['container_name']);
            item = item.replace("%LM_USER", entry['lm_user']);
            $list.append($("<li>" + item + "</li>"))
        }
        return $list
    },
    //Takes a checkbox area and constructs the correct title string, depending on the selected cbs in its group
    create_checkbox_title: function($checkbox_area) {
        var title_delimiter = '; '
        if ($checkbox_area.hasClass("title_line_break")) {
            title_delimiter = '<br />';
        }
        var $all_cb_units = $checkbox_area.find(".checkbox_unit");
        var combined_title = "";
        $all_cb_units.each(function(){
            if ($(this).children("input").is(":checked")) {
                combined_title += $(this).text() + title_delimiter;
            }
        });
        //Remove the last delimiter
        if (combined_title.length > 2) {
            if (title_delimiter == '<br />') {
                combined_title = combined_title.slice(0, -6);
            }
            else if (title_delimiter == '; ') {
                combined_title = combined_title.slice(0, -2);
            }
        }
        return combined_title;
    },
    load_all_subcontainers: function(container) {
        var parent_uid = container.data("uid");
        $.ajax({
            url: OC.filePath('sfb882_doc', 'ajax', 'loadUnits.php'),
			dataType: "json",
            data: {'location': 'internal', 'parent_uid': parent_uid},
			success: function(result) {
				for (var i = 0; i < result.data.length; i++) {
					var subcontainer_data = result.data[i];
					sfb882_doc_container_internal.load_subcontainer(subcontainer_data, container);
				}
			}
        });
    },
    load_subcontainer: function(subcontainer_data, parent_container, animation_time) {
        animation_time = (typeof animation_time === "undefined") ? 0 : animation_time;
        $.ajax({
            url: OC.filePath('sfb882_doc', 'ajax', 'generateContainer.php'),
            data: {'uid': subcontainer_data.uid},
            dataType: "json",
            success: function(result) {
                var $new_subcontainer = $(result.html_data); 
                $new_subcontainer.data({
                    'uid': subcontainer_data.uid,
                    'color': subcontainer_data.color,
                    'container_name': subcontainer_data.container_name,
                    'container_type': subcontainer_data.container_type,
                    'research_type': subcontainer_data.research_type
                });
                if (subcontainer_data.container_type == 'study') {
                    sfb882_doc_container_internal.set_research_title_icons($new_subcontainer);
                }
	            if (subcontainer_data.location == "central_pane") {
                    parent_container.children(".container_outer_div").children(".central_pane").append($new_subcontainer);
		            $new_subcontainer.css({'left': subcontainer_data.posx, 'top': subcontainer_data.posy});
                    if (animation_time != 0) {
                        $new_subcontainer.show("fold", animation_time, function() {        
					        $new_subcontainer.animate({"background-color": subcontainer_data.color}, animation_time, function() {
							    $new_subcontainer.children(".container_outer_div").children(".container_title").show("slide", animation_time, function() {
									sfb882_doc_container_internal.prepare_init($new_subcontainer);
								});
                            });
                        });
						return;
                    }
                    /*
                    This else clause seems to be unnecessary because at first glance an animation time of 0 would make
                    the upper code execute instantely. However, that does not seem to happen in some cases (Race condition?)
                    which makes some jquery selectors fail during internal initialization (".container_title").
                    */
                    else {
                        $new_subcontainer.show();
                        $new_subcontainer.css({"background-color": subcontainer_data.color});
                        $new_subcontainer.children(".container_outer_div").children(".container_title").show();
                    }    
	            }
                else if (subcontainer_data.location == "sidebar") {     
                    parent_container.children(".container_outer_div").children(".left_pane").append($new_subcontainer); 
                }
                else {
                    parent_container.children(".container_outer_div").append($new_subcontainer);
                }
                sfb882_doc_container_internal.prepare_init($new_subcontainer);
            }
        });
    },
    /*
     * Check if the container is a content container (leaf) or has subcontainers (branch) and
     * choose the according init methods
     */
    prepare_init: function($container) {
		if ($container.children(".container_outer_div").children(".container_content").length > 0) {
			sfb882_doc_container_internal.load_data($container, function() { 
				sfb882_doc_container_internal.init_input_content($container);
				sfb882_doc_container_internal.init($container);
			});
		}   
		else {
			sfb882_doc_container_internal.init($container);
			sfb882_doc_container_internal.load_all_subcontainers($container);
		}
	},
    set_research_title_icons: function($container) {
        var $title = $container.children('.container_outer_div').children('.container_title');
        $title.children('.mand_title').hide();
        if ($container.data('research_type').indexOf('qualitative') > -1) {
            $title.children('.mand_mult').css('display', 'inline-block');
        }
        else if ($container.data('research_type').indexOf('quantitative') > -1) {
            $title.children('.mand_plus').css('display', 'inline-block');
        }
        else if ($container.data('research_type').indexOf('mixed') > -1) {
            $title.children('.mand_mult').css('display', 'inline-block');
            $title.children('.mand_plus').css('display', 'inline-block');
        }
    },
    accordionize: function($container) {
        $container.find(".container_content").accordion({
            header: ".content_title",
            heightStyle: "content",
            collapsible: true,
            active: false,
            activate: function(event, ui) {
                if (ui.newPanel.hasClass("content_input_file_field")) {
                    $("#files").css({'z-index': 20});
                }
                else {
                    $("#files").css({'z-index': 10});
                }
            }
        });      
    },
    init_input_content: function($container) {
        sfb882_doc_container_internal.add_logos($container);
        sfb882_doc_container_internal.add_buttons($container);
        sfb882_doc_container_internal.accordionize($container);
        sfb882_doc_file_section.create_file_sections($container);
    },
    load_data: function($container, callback) {
        $.ajax({
            url: OC.filePath('sfb882_doc', 'ajax', 'loadData.php'),
			data: {'uid': $container.data('uid')},
            dataType: "json",
			success: function(result) {
                if (result.data) {
                    for (var i = 0; i < result.data.length; i++) {
                        var field = result.data[i].field;
                        var $element = $container.find("[data-text-input-id='" + field + "']").add($container.find("[data-checkbox-area-id='"+ field +"']"));
                        var value = result.data[i].value;
                        if ($element.is("input,textarea")) {
                            $element.val(value);
                            var title_text = value;
                            var $title = $element.parent().prev(".content_title");
                                                            
                        }
                        else if ($element.is("div.content_input_checkbox_area")) {
							/*
							 * var $static_cb = $element.find("[data-checkbox-id='"+ value +"']");
							 * This would be easier but will get you into trouble when people start using
							 * characters like "'" in their custom checkboxes... 
							 */
                            var $static_cbs = $element.find("[data-checkbox-id]");
                            var add_custom = true;
                            $static_cbs.each(function() {
								if ($(this).attr("data-checkbox-id") === value) {
									$(this).prop('checked', true);
									add_custom = false;
									return false;
								}
							});
                           if (add_custom) {
                                sfb882_doc_container_internal.addCustomCheckbox($element, value);
                            }
                            var title_text = sfb882_doc_container_internal.create_checkbox_title($element); 
                            var $title = $element.prev(".content_title");       
                        }
                        sfb882_doc_container_internal.set_field_title($title, title_text);
                    }
                    sfb882_doc_container_internal.update_progressbar($container);
                }
                if (callback) {
                    callback.call();
                }
            }
        });        
    },
    find_parent_container: function($container) {
        var $parent = $container.parent();
        while(!$parent.hasClass("container")) {
            if ($parent.is("#sfb882_doc_content")) {
                return $([]);
            }
            $parent = $parent.parent();
        }
        return $parent;
    },
    set_field_title: function($title, value) {
        if (!$title) {
            console.log("Warning: title element for text '" + value + "' not found");
            return;
        }
        if (!$title.data('orig')) {
            $title.data('orig', $title.children(".title_string").html());
        }
        if (value.length > 0) {
            $title.children(".title_string").html($title.data('orig') + ":  " + value);
            $title.data('filled', true);
            $title.css({"background": "rgba(141, 250, 84, 200)"});
        }
        else {
            $title.children(".title_string").html($title.data('orig'));
            $title.data('filled', false);
            $title.removeAttr('style');
        }
    },
    add_buttons: function($container) {
        //Add save, undo and redo buttons to text input fields
        var $input_fields = $container.find("[data-text-input-id]");
        if ($input_fields.length == 0) {
            return;
        }
        var $redo_button = $('<div class="redo input_button"></div>');
        var $undo_button = $('<div class="undo input_button"></div>');
        var $save_button = $('<div class="save input_button"></div>');   
        $input_fields.after($save_button).after($redo_button).after($undo_button);
        //Implement save button mechanics
        $save_buttons = $container.find(".content_input > .save.input_button");
        $save_buttons.click(function(event) {
            var $field = $(this).siblings("textarea,input");
            var value = $field.val().trim();
            var field_id = $field.data("text-input-id");
            var uid = $container.data("uid");
            $.ajax({
                type: "POST",
                url: OC.filePath('sfb882_doc', 'ajax', 'saveField.php'),
		        data: {'field_id': field_id, 'uid': uid, 'value': value, 'action': 'UPDATE'},
                dataType: "json",
		        success: function(result) {
                    if (result.status == "success") {
                        $title = $field.parent().prev(".content_title");
                        if (value.length == 0) {
                            $field.val("");
                        }
                        sfb882_doc_container_internal.set_field_title($title, value);
                    }
                },
                error: function(result) {
                    sfb882_doc.notify(result.responseText, 10000);
                }
            });        
        });
        //Implements undo/redo button mechanics
        $undo_redo_buttons = $container.find(".content_input > .redo.input_button, .content_input > .undo.input_button");
        $undo_redo_buttons.click(function(event) {
            var $field = $(this).siblings("textarea,input");
            var value = $field.val().trim();   
            var field_id = $field.data("text-input-id");
            var uid = $container.data("uid");
            if ($(this).hasClass("undo")) {
                var method = "undo";
            }
            else if ($(this).hasClass("redo")) {
                var method = "redo";
            }
            $.ajax({
                type: "POST",
                url: OC.filePath('sfb882_doc', 'ajax', 'undoRedo.php'),
		        data: {'field_id': field_id, 'uid': uid, 'value': value, 'method': method},
                dataType: "json",
		        success: function(result) {
                    if (result.data.length > 0) {
                        $title = $field.parent().prev(".content_title");
                        $title.css({
                            "background": "rgba(141, 250, 84, 200)"
                        });
                        $field.val(result['data']);
                    }
                },
                error: function(result) {
                    sfb882_doc.notify(result.responseText, 10000);
                }
            });        
        });

        //Add plus buttons to extensible checkbox areas
        var $checkbox_areas =  $container.find(".content_input_checkbox_area[data-checkbox-extensible=true]");
        $checkbox_areas.each(function() {
            var id_attr = $(this).attr("data-checkbox-area-id");
            /*
            Emulates a 'hasAttr()'-like function, see
            http://stackoverflow.com/questions/1318076/jquery-hasattr-checking-to-see-if-there-is-an-attribute-on-an-element
            for possible browser-related pitfalls
            */
            if (typeof id_attr !== 'undefined' && id_attr !== false) {
                var $add_input = $('<div class="adder_area"><input class="checkbox_adder" placeholder="Additional entries..."/><div class="plus input_button"></div></div>'); 
                $add_input.appendTo($(this));
            }
        });
        $adder_buttons = $container.find(".adder_area>.plus.input_button");
        $adder_buttons.click(function(event) {
            var $field = $(this).siblings(".checkbox_adder");
            var value = $field.val().trim();
            var $checkbox_area = $field.parent().parent(".content_input_checkbox_area");
            var cba_id = $checkbox_area.attr("data-checkbox-area-id");
            var uid = $container.data("uid");
            if (value.length > 0) {
                $field.val("");
                $.ajax({
                    type: "POST",
                    url: OC.filePath('sfb882_doc', 'ajax', 'saveField.php'),
		            data: {'uid': uid, 'field_id': cba_id, 'value': value, 'action': 'ADD'},
                    dataType: "json",
		            success: function(result) {
                        if (result.status == "success") { 
                            sfb882_doc_container_internal.addCustomCheckbox($checkbox_area, value);
                            var title_text = sfb882_doc_container_internal.create_checkbox_title($checkbox_area);
                            var $title = $checkbox_area.prev(".content_title");
                            sfb882_doc_container_internal.set_field_title($title, title_text);
                        }
                    }
                });
            } 
        });
    },
    add_logos: function($container) {
        //Append indicators for mandatory fields
        var $mandatory_plus_fields = $container.find("[data-mandatory*='+']");
        var $mandatory_mult_fields = $container.find("[data-mandatory*='*']");
        $mandatory_plus_fields.closest(".content_input,.content_input_checkbox_area,.content_input_file_field").prev(".content_title").append('<div class="logo mand_plus" title="' + t("sfb882_doc", "logo_mand_plus_desc") + '"></div>');
        $mandatory_mult_fields.closest(".content_input,.content_input_checkbox_area,.content_input_file_field").prev(".content_title").append('<div class="logo mand_mult" title="' + t("sfb882_doc", "logo_mand_mult_desc") + '"></div>');
        //Append DDI logo
        var $ddi_inputs = $container.find("[data-ddi-placeholder]");
        $ddi_inputs.closest(".content_input,.content_input_checkbox_area,.content_input_file_field").prev(".content_title").append('<div class="logo ddi" title="' + t("sfb882_doc", "logo_ddi_desc") + '"></div>');
        //Append PUB logo
        var $pub_inputs = $container.find(".pub_search,.data_search");
        $pub_inputs.prev(".content_title").append('<div class="logo pub" title="' + t("sfb882_doc", "logo_pub_desc") + '"></div>');
        //Append PEVZ logo
        var $pevz_inputs = $container.find(".pevz_search");
        $pevz_inputs.prev(".content_title").append('<div class="logo pevz" title="' + t("sfb882_doc", "logo_pevz_desc") + '"></div>');
        //Append file logo
        var $file_inputs = $container.find(".content_input_file_field");
        $file_inputs.prev(".content_title").append('<div class="logo file" title="' + t("sfb882_doc", "logo_file_desc") + '"></div>');
    },
    addCustomCheckbox: function($element, value) {
        var $cb_unit = $('<div class="checkbox_unit custom"><input type="checkbox" />' + value + '</div>');
        var $last_cb = $element.children("div.checkbox_unit:last");
        if ($last_cb.length > 0) {
            $last_cb.after($cb_unit);
        }
        else {
            $element.prepend($cb_unit);
        }
        var $checkbox = $cb_unit.children("input");
        $checkbox.prop('checked', true);
        $checkbox.change(function() {
            if ($checkbox.prop('checked') == false) {
                var uid = $element.parents(".container:first").data("uid");
                var cba_id = $element.attr("data-checkbox-area-id");
                $.ajax({
                    type: "POST",
                    url: OC.filePath('sfb882_doc', 'ajax', 'saveField.php'),
		            data: {'uid': uid, 'field_id': cba_id, 'value': value, 'action': 'DELETE'},
                    dataType: "json",
		            success: function(result) {
                        if (result.status == "success") {
                            $cb_unit.remove();
                            var title_text = sfb882_doc_container_internal.create_checkbox_title($element); 
                            var $title = $element.prev(".content_title");       
                            sfb882_doc_container_internal.set_field_title($title, title_text);
                        }
                    }
                });
            }
        });
    },
    get_minimized_class: function(element) {
        if (element.hasClass("in_share")) {
            return "stored";
        }
        return "closed";
    },
    get_show_effect:function(element) {
		if (element.hasClass("container")) {
			return "fade";
		}
		if (element.hasClass("container_content")) {
			return "slide";		
		}
        if (element.hasClass("close_button")) {
			return "drop";		
		}
        if (element.hasClass("close_button")) {
			return "drop";		
		}
        if (element.hasClass("container_pane")) {
			return "slide";		
		}
		return "fade";
	},
	get_hide_effect:function(element) {
		if (element.hasClass("container")) {
			return "fade";
		}
		if (element.hasClass("container_content")) {
			return "slide";		
		}
        if (element.hasClass("close_button")) {
			return "drop";		
		}
        if (element.hasClass("main_container_left_area")) {
			return "slide";		
		}
		return "fade";
	},
    animation_constants: {
		time_factor: 0.5,
        opening_time: 500,
        closing_time: 500,
        fade_in_time: 100,
        fade_out_time: 100,
    },
	shrink_container:function(container) {
        var closed_class = sfb882_doc_container_internal.get_minimized_class(container);
        var closing_time = sfb882_doc_container_internal.animation_constants.closing_time * sfb882_doc_container_internal.animation_constants.time_factor;
        if (container.hasClass("in_workspace") || container.hasClass("in_share")) {
            container.css({width : ""});
        }
		container.switchClass("open", closed_class, closing_time, "swing", function() {
			sfb882_doc.data.container_animation_ongoing = false;
			container.css("z-index", "1");
	        if (container.hasClass("in_workspace")) {
		        var orig_position = container.data('orig_position');
		        container.animate({left: orig_position.left, top: orig_position.top}, closing_time, function() {
			        container.draggable("enable");
		        });
	        }
            else if (container.hasClass("in_share")) {
                var orig_position = container.data('orig_position');
                container.detach();
                $("#share").append(container);
                container.children(".container_outer_div").children(".container_title").hide("fade", 200);
                container.animate({left: orig_position.left, top: orig_position.top}, closing_time, function() {
			        container.draggable("enable");
                });   
            }
            else if (container.hasClass("in_central_pane")) {
                var orig_position = container.data('orig_position');
		        container.animate({left: orig_position.left, top: orig_position.top}, closing_time, function() {
			        container.draggable("enable");
		        });
            }
	        else if (container.hasClass("middle_one_orig")) {
		        container.switchClass("base_corner", "middle_one", closing_time);
	        }
	        else if (container.hasClass("in_sidebar_orig")) {
		        container.switchClass("base_corner", "in_sidebar", closing_time);
	        }
	        else if (container.hasClass("upper_two_first_orig")) {
		        container.switchClass("base_corner", "upper_two_first", closing_time);
	        }
            else if (container.hasClass("upper_two_second_orig")) {
		        container.switchClass("base_corner", "upper_two_second", closing_time);
	        }
	        else if (container.hasClass("lower_two_orig")) {
		        container.switchClass("base_corner", "lower_two", closing_time);
	        }
            else if (container.hasClass("middle_three_orig")) {
		        container.switchClass("base_corner", "middle_three", closing_time);
	        }
            else if (container.hasClass("upper_four_orig")) {
		        container.switchClass("base_corner", "upper_four", closing_time);
	        }
	        else if (container.hasClass("lower_four_orig")) {
		        container.switchClass("base_corner", "lower_four", closing_time);
	        }
            else if (container.hasClass("upper_five_orig")) {
		        container.switchClass("base_corner", "upper_five", closing_time);
	        }
	        else if (container.hasClass("lower_five_orig")) {
		        container.switchClass("base_corner", "lower_five", closing_time);
	        }
            var $outer_div = container.children(".container_outer_div");
            if (!container.hasClass("in_share")) { 
                $outer_div.children(".progressbar_container").show();  
                sfb882_doc_container_internal.update_progressbar(container);  
            }
            $outer_div.children(".container_title").fitText(3.5, {minFontSize: "16px", maxFontSize: "24px"});
            $("#files").css({'z-index': 10});
        });
	},
	grow_container:function(container, callback) {
        container.children(".container_outer_div").children(".progressbar_container").hide();
        var opening_time = sfb882_doc_container_internal.animation_constants.opening_time * sfb882_doc_container_internal.animation_constants.time_factor;
		container.css("z-index", "10");
		if (container.hasClass("in_workspace")) {
            container.draggable("disable");
            /*
            Workaround: Opening a container without moving it first would make it semi-opaque, thus we have to
            manually set the opacity.
            */
            container.css("opacity", "1");
			container.data('orig_position', {top: container.css("top"), left: container.css("left")});
			container.animate({left: "4em", top: "-0.5em"}, opening_time);
		}
        else if (container.hasClass("in_share")) {
            container.css("opacity", "1");
			container.data('orig_position', {top: container.css("top"), left: container.css("left")});
            container.draggable("disable");
            container.detach();
            $("#sfb882_doc_content").append(container);
			container.animate({left: "4em", top: "0.5em"}, opening_time, function() {
                container.children(".container_outer_div").children(".container_title").show("fade", 200);
            });
        }
        else if (container.hasClass("in_central_pane")) {
            container.draggable("disable");
            container.css("opacity", "1");  
			container.data('orig_position', {top: container.css("top"), left: container.css("left")});
			container.animate({left: "0", top: "3em"}, opening_time); //base corner
        }
        else if (container.hasClass("in_sidebar")) {
            container.switchClass("in_sidebar", "base_corner", opening_time);
        }
		else if (container.hasClass("middle_one")) {
			container.switchClass("middle_one", "base_corner", opening_time);
		}
		else if (container.hasClass("upper_two_first")) {
			container.switchClass("upper_two_first", "base_corner", opening_time);
		}
        else if (container.hasClass("upper_two_second")) {
			container.switchClass("upper_two_second", "base_corner", opening_time);
		}
		else if (container.hasClass("lower_two")) {
			container.switchClass("lower_two", "base_corner", opening_time);
		}
        else if (container.hasClass("middle_three")) {
			container.switchClass("middle_three", "base_corner", opening_time);
		}
        else if (container.hasClass("upper_four")) {
			container.switchClass("upper_four", "base_corner", opening_time);
		}
		else if (container.hasClass("lower_four")) {
			container.switchClass("lower_four", "base_corner", opening_time);
		}
        else if (container.hasClass("upper_five")) {
			container.switchClass("upper_five", "base_corner", opening_time);
		}
		else if (container.hasClass("lower_five")) {
			container.switchClass("lower_five", "base_corner", opening_time);
		}
        var closed_class = sfb882_doc_container_internal.get_minimized_class(container);
		container.switchClass(closed_class, "open", opening_time, "swing", function() {
            if (container.hasClass("in_workspace") || container.hasClass("in_share")) {
                //Make top-level containers somewhat smaller
                container.css({width : "90%"});
            }
			if (callback) {
				callback.call();
			}
		});
	},
	serial_fade:function(owning_container, element_array, cur_index, fadeIn) {
		if (fadeIn) {
			if (cur_index < element_array.length) {
				var element = element_array.eq(cur_index);
				element.show({
					effect: sfb882_doc_container_internal.get_show_effect(element),
					complete: function() {
						sfb882_doc_container_internal.serial_fade(owning_container, element_array, cur_index + 1, true);
					},
					duration: sfb882_doc_container_internal.animation_constants.fade_in_time * sfb882_doc_container_internal.animation_constants.time_factor
				});
			}
			else {
				sfb882_doc.data.container_animation_ongoing = false;
			}
		}
		else {
			if (cur_index < element_array.length) {
				var element = element_array.eq(cur_index);
				element.hide({
					effect: sfb882_doc_container_internal.get_hide_effect(element),
					complete: function() {
						sfb882_doc_container_internal.serial_fade(owning_container, element_array, cur_index + 1, false);
					},
					duration: sfb882_doc_container_internal.animation_constants.fade_out_time * sfb882_doc_container_internal.animation_constants.time_factor
				});
			}
			else {
				sfb882_doc_container_internal.shrink_container(owning_container);
			}
		}
	}
}
