/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
sfb882_doc_icons = {
    location_class_mapping: {
        'workbench': 'in_workspace',
        'central_pane': 'in_central_pane'
    },
    trash_bin_open: false,
    /*
    * Determines if an event takes place inside an element
    * @param event A jQuery event object
    * @param $element a jQuery object
    * @return string
    */
    _event_inside: function(event, $element) {
        if ($element.offset().left < event.pageX && 
			event.pageX <= $element.offset().left + $element.width() &&
			$element.offset().top <= event.pageY && 
			event.pageY <= $element.offset().top + $element.height()) 
		{
			return true;
		}
		else return false;
    },
    /*
    * Initializes the 'trash can' elements by making them droppable targets (for deletion) and
    * double-clickable for restoring of trashed containers
    * @param $trash_area A jQuery object containing the trash area
    * @param accept_class A CSS class dropped containers must have to be accepted by the trash area
    * @param $restore_target A jQuery object containing the object restored containers may be dropped onto
    * for restoration, usually some kind of work pane or canvas
    * @param owner_uid uid of the container the trash area is located in
    */
    init_trash: function($trash_area, accept_class, $restore_target, owner_uid) {
	    $trash_area.droppable({
	        hoverClass: "trash_area_hover",
	        tolerance: "pointer",
	        accept: function(element) {
		        if (element.hasClass(accept_class) && element.hasClass("closed")) {
			        return true;
		        }
		        return false;
	        },
	        drop: function(event, ui) {
                trash_area_offset_x = $trash_area.offset().left;
                trash_area_offset_y = $trash_area.offset().top;
                var drop_left = trash_area_offset_x - 31;
                var drop_top = trash_area_offset_y - 62;
		        var elem = ui.draggable;
		        var title = elem.children(".container_outer_div").children(".container_title");
                var progressbar = elem.children(".container_outer_div").children(".progressbar_container");
		        progressbar.hide("slide", {"direction" : "right"}, 500, function() {
                    title.hide("slide", {"direction" : "left"}, 500, function() { 
			            elem.switchClass("in_workspace", "standard", 500, "swing", function() {
				            elem.switchClass("closed", "create_icon", 500, "swing", function() {
					            elem.animate({left:drop_left, top: drop_top}, 500, "swing", function() {
						            elem.hide("slide", {direction: "down"}, 500, function() {
							            var elem_id = elem.data("uid");
							            $.ajax({
								            url: OC.filePath('sfb882_doc', 'ajax', 'trashUnit.php'),
								            data: {"uid": elem_id},
								            dataType: "json",
								            success: function() {							
									            elem.remove();
								            }
							            });
						            });							
					            });
				            });
			            });
                    });
		        });
	        }
        });
        $trash_area.dblclick(function() {
            if (sfb882_doc_icons.trash_bin_open) {
                return;
            }
            else {
                sfb882_doc.data.trash_bin_open = true;
            }
            sfb882_doc.notify(t("sfb882_doc", "restoring_studies"));
            $.ajax({
			    url: OC.filePath('sfb882_doc', 'ajax', 'loadUnits.php'),
                data: {'location': 'trash', 'parent_uid': owner_uid},
			    dataType: "json",
			    success: function(result) {
                    if (result.data.length < 1) {
                        sfb882_doc.notify(t("sfb882_doc", "no_studies_in_trash"));
                        sfb882_doc.data.trash_bin_open = false;
                        return;
                    }
                    var index = result.data.length - 1;
                    arrange_containers:
			        for (var top = 5; top <= 185; top += 60) {
                        for (var left = 85; left <= 445; left += 90) {
                            var unit = result.data[index];
                            unit.posy = top + "px";
                            unit.posx = left + "px";
                            index--;
                            sfb882_doc_icons.restore_skeleton(unit, $restore_target);
                            if (index < 0) {
                                break arrange_containers;
                            }
                        }
                    }
                    setTimeout(function(){
                        var $bin = $('#bin');
                        $bin.show("highlight", 500, function(){
                            $bin.switchClass("minimized", "maximized", 500, function() {
                                $('#bin_text').show("fade", 100);
                            });
                        })
                    }, 1500);
			    }
		    });         
        });
    },
    restore_skeleton: function(unit, $restore_target) {
        var $new_container = $('<div class="container in_trash stored" style="display: none"></div>');
        $new_container.addClass(unit.container_type);
        $("#bin").append($new_container);
		$new_container.css({'left': unit.posx, 'top': unit.posy, 'background-color': unit.color});
		$new_container.data({
            "uid": unit.uid,
            "foreign": false,
            "color": unit.color,
            "container_name": unit.container_name,
            "container_type": unit.container_type,
            "research_type": unit.research_type
        });
        $.ajax({
            url: OC.filePath('sfb882_doc', 'ajax', 'getMeta.php'),
            data: {'uid': unit.uid},
            dataType: "json",
            success: function(result) {
                if (result.data) {
                    sfb882_doc.generate_tooltip($new_container, result.data);
                }
            }
        });
        var $target = $restore_target;
		$new_container.show();
        $new_container.draggable({ 
		    containment: "#sfb882_doc_content",
     		scroll: false,
		    opacity: 0.35,
		    delay: 5,
		    zIndex: 20,
            start: function(event, ui) {
                sfb882_doc_icons.startX = ui.helper.css('left');
                sfb882_doc_icons.startY = ui.helper.css('top');
                sfb882_doc.update_drag_constraints();
			    var $helper = ui.helper.detach();
                $helper.tooltip({disabled: true});
			    $("#sfb882_doc_content").append($helper);
            },
            drag: function(event, ui) {
                sfb882_doc._update_container_position(event, ui);
                if (sfb882_doc_icons._event_inside(event, $('#bin')) || sfb882_doc_icons._event_inside(event, $target)) {    
                    ui.helper.removeClass("invalid");
                }    
                else {
                    ui.helper.addClass("invalid");
                }
            },
            stop: function(event, ui) {
                sfb882_doc._update_container_position(event, ui);
                if (sfb882_doc_icons._event_inside(event, $target)) {    
                    var posx = ui.helper.offset().left;
	                var posy = ui.helper.offset().top; 
                    posx = posx + "px";
                    posy = posy + "px";
                    $.ajax({
		                url: OC.filePath('sfb882_doc', 'ajax', 'untrashUnit.php'),
		                data: {'uid': unit.uid, 'posx': posx, 'posy': posy},
		                dataType: "json",
		                success: function(result) {
                            $.ajax({
			                    url: OC.filePath('sfb882_doc', 'ajax', 'generateContainer.php'),
			                    data: {'uid': unit.uid},
			                    dataType: "json",
			                    success: function(result) {
                                    $content = $(result.html_data);       
                                    if ($content.is('.container')) {
                                        ui.helper.append($content.children());
                                    }
                                    else {
                                        ui.helper.append($content);
                                    }
                                    ui.helper.switchClass("in_trash", sfb882_doc_icons.location_class_mapping[unit.location]); 
	                                $target.append(ui.helper);
                                    ui.helper.draggable("destroy");
                                    ui.helper.css({"top": posy, "left": posx});
                                    ui.helper.switchClass("stored", "closed", 100, "swing", function() {
							            ui.helper.children(".container_outer_div").children(".container_title").show();
                                        ui.helper.children(".container_outer_div").children(".progressbar_container").show();
                                        if (ui.helper.data('container_type') == 'study') {
                                            sfb882_doc_container_internal.set_research_title_icons(ui.helper);
                                        }   
                                        if (ui.helper.children(".container_outer_div").children(".container_content").length > 0) {
                                            sfb882_doc_container_internal.load_data(ui.helper, function() {
                                                sfb882_doc_container_internal.init_input_content(ui.helper);
                                                sfb882_doc_container_internal.init(ui.helper); 
                                            });    
                                        }
                                        else {
                                            sfb882_doc.init_container(ui.helper);      
                                        }
						            });		
                                }
                            });
		                }
	                });  
                }    
                else {
                    ui.helper.animate({
                        left: sfb882_doc_icons.startX,
                        top: sfb882_doc_icons.startY
                        }, 0, 
                        function() {
                           $("#bin").append(ui.helper);
                           ui.helper.removeClass("invalid");
                        }
                    );
                }
            }
        });  
    },
    init_export: function($export) {
		$export.droppable({
			hoverClass: "trash_area_hover",
			tolerance: "pointer",
			greedy: true,
			accept: function(element) {
				if (element.hasClass("container") && element.hasClass("closed")) {
					return true;
				}
				return false;
			},
			drop: function(event, ui) {
				var container = ui.draggable;
                var uid = container.data("uid");
                $("input:radio[value='plaintext']").prop('checked', true);
                if (container.data('container_type') == 'study') { //Only studies can be exported to DDI
					$("input:radio[value='ddi_3_2']").prop('disabled', false);
				}
				else {
					$("input:radio[value='ddi_3_2']").prop('disabled', true);
				}
                $("#export_dialog").dialog({
					height: 500,
					width: 500,
					resizable: false,
					buttons: [
					{
						text: t("sfb882_doc", "export_button_ok_desc"),
						click: function() {
                            var export_type = $("input:radio[name=export_type_button_group]:checked").val();
                            var attach_files = $("#attachfiles_cb").prop('checked');
                            var export_running_noty = noty({
                                text: 'Dateiexport...',
                                modal: true,
                                type: 'information',
                                layout: 'center'
                            });
                            $.ajax({
                                url: OC.filePath('sfb882_doc', 'ajax', 'exportContainer.php'),
								data: {
                                    "uid": uid,
                                    "export_type": export_type,
                                    "export_scope": "all",
                                    "attach_files": attach_files
                                },
								dataType: "json",
								success: function(result) {
                                    export_running_noty.close();
                                    if (result.status == 'success') {   
                                        noty({
                                            text: 'Export erfolgreich, erstellte Datei:<br />' + result.path,
                                            modal: true,
                                            type: 'success',
                                            layout: 'center',
                                            timeout: 5000
                                        });
                                    }
                                    else {
                                        noty({
                                            text: 'Export fehlgeschlagen, der folgende Fehler ist aufgetreten: ' + result.message,
                                            modal: true,
                                            type: 'error',
                                            layout: 'center',
                                            timeout: 7000
                                        });
                                    }
								}
                            });
							$(this).dialog("close");
							if (container.data('start_x_pos') && container.data('start_y_pos')) {
								container.animate({
									'left': container.data('start_x_pos'),
									'top': container.data('start_y_pos')
								}, 500);
							}
						}
					},
					{
						text: t("sfb882_doc", "export_button_abort_desc"),
						click: function() {
							$(this).dialog("close");
							if (container.data('start_x_pos') && container.data('start_y_pos')) {
								container.animate({
									'left': container.data('start_x_pos'),
									'top': container.data('start_y_pos')
								}, 500);
							}
						}
					}]
				});
			}
		});
	},
    init_duplicate: function($duplicate) {
		$duplicate.droppable({
	        tolerance: "pointer",
	        greedy: true,
	        accept: ".container",
	        drop: function(event, ui) {
				var container = ui.draggable;
				$('#duplicate_name_input').val(container.data('container_name'));
				$('#duplicate_dialog').dialog({
					height: 200,
					width: 300,
					modal: true,
					resizable: false,
					buttons: [
					{
						text: t("sfb882_doc", "duplicate_button_ok_desc"),
						click: function() {
							var uid = container.data("uid");
							var new_name = $('#duplicate_name_input').val();
							$.ajax({
								url: OC.filePath('sfb882_doc', 'ajax', 'duplicateContainer.php'),
								data: {'uid': uid, 'container_name': new_name},
								dataType: "json",
								success: function(result) {							
									var $parent_container = sfb882_doc_container_internal.find_parent_container($duplicate);
									sfb882_doc_container_internal.load_subcontainer(result.data, $parent_container, 500);
								}
							});
							$(this).dialog("close");
						}
					},
					{
						text: t("sfb882_doc", "duplicate_button_abort_desc"),
						click: function() {
							$(this).dialog("close");
							if (container.data('start_x_pos') && container.data('start_y_pos')) {
								container.animate({
									'left': container.data('start_x_pos'),
									'top': container.data('start_y_pos')
								}, 500);
							}
						}
					}
					]
				});
	        }
        });
	}
}
