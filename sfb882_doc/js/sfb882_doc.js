/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
sfb882_doc = {
	data: {
		container_animation_ongoing : false,
	},
    notify:function (message, time) {
        time = typeof time !== 'undefined' ? time : 3000;
        if ($('#notification').data("showing") && $('#notification').data("showing") == true) {
            $('#notification').hide();    
        }
		$('#notification').text(t('sfb882_doc', message));
        $('#notification').data("showing", true);
		$('#notification').fadeIn(500, function() {
            window.setTimeout(function() {
                $('#notification').fadeOut(500, function() {
                    $('#notification').data("showing", false);
                });
            }, time);
        });
	},
    _inside_share: function(event) {
		if (sfb882_doc.data.share_x1 < event.pageX 	&& 
			event.pageX <= sfb882_doc.data.share_x2 && 
			sfb882_doc.data.share_y1 <= event.pageY && 
			event.pageY <= sfb882_doc.data.share_y2) 
		{
			return true;
		}
		else return false;
	},
    _inside_bin: function(event) {
		if (sfb882_doc.data.bin_x1 < event.pageX 	&& 
			event.pageX <= sfb882_doc.data.bin_x2 && 
			sfb882_doc.data.bin_y1 <= event.pageY && 
			event.pageY <= sfb882_doc.data.bin_y2) 
		{
			return true;
		}
		else return false;
	},
	update_drag_constraints: function() {
        var $share = $("#share");
		sfb882_doc.data.share_x1 = $share.offset().left;
		sfb882_doc.data.share_y1 = $share.offset().top;
		sfb882_doc.data.share_x2 = sfb882_doc.data.share_x1 + $share.width();
		sfb882_doc.data.share_y2 = sfb882_doc.data.share_y1 + $share.height();
        var $bin = $("#bin");
		sfb882_doc.data.bin_x1 = $bin.offset().left;
		sfb882_doc.data.bin_y1 = $bin.offset().top;
		sfb882_doc.data.bin_x2 = sfb882_doc.data.bin_x1 + $bin.width();
		sfb882_doc.data.bin_y2 = sfb882_doc.data.bin_y1 + $bin.height();
		var $content = $("#sfb882_doc_content");
		sfb882_doc.data.content_offset_x = $content.offset().left;
		sfb882_doc.data.content_offset_y = $content.offset().top;
		sfb882_doc.data.content_width = $content.width();
		sfb882_doc.data.content_height = $content.height();
	},
	/*	
	Shouldn't be called directly as it depends on correctly set constraint variables		
	*/
	_update_container_position: function(event, ui) {
		var posx = event.pageX - sfb882_doc.data.content_offset_x - ui.helper.width()/2;
		var posy = event.pageY - sfb882_doc.data.content_offset_y - ui.helper.height()/2;
		var maxx = sfb882_doc.data.content_width - ui.helper.outerWidth();
		var maxy = sfb882_doc.data.content_height - ui.helper.outerHeight();
		ui.position.left = Math.max(Math.min(posx, maxx), 0);
		ui.position.top = Math.max(Math.min(posy, maxy), 0);
	},
	
	/*
     * Inits a "Unit" container:
	 * - make the container dragggable 
     * - accordionize all inner input fields
     * - init container inner logic
	*/
	init_container:function(container) {
        if (container.hasClass("in_workspace") || container.hasClass("in_trash") || container.hasClass("in_share") && container.data("foreign") == false) {
		    container.draggable({ 
			    containment: "#sfb882_doc_content",
	     		scroll: false,
			    opacity: 0.35,
			    delay: 5,
			    zIndex: 10,
			    start: function(event, ui) {
				    //Make sure that all variables acting as constraints (lengths, widths, x/y-positions...) are up to date
				    sfb882_doc.update_drag_constraints();
				    var $helper = ui.helper.detach();
				    $helper.data({
						'start_x_pos': $helper.css('left'),
						'start_y_pos': $helper.css('top')
					});
				    $("#sfb882_doc_content").append($helper);
			    },
			    drag: function(event, ui) {
				    sfb882_doc._update_container_position(event, ui);
				    if (sfb882_doc._inside_share(event)) {		
					    if (ui.helper.hasClass("closed") && $('#share').hasClass("maximized")) {
						    ui.helper.children(".container_outer_div").children(".container_title").hide();
                            ui.helper.children(".container_outer_div").children(".progressbar_container").hide();
						    ui.helper.switchClass("closed", "stored", 100, "swing");						
					    }
				    }
                    else if (sfb882_doc._inside_bin(event) && ui.helper.hasClass("in_trash")) {
                        if (ui.helper.hasClass("closed") && $('#bin').hasClass("maximized")) {
						    ui.helper.children(".container_outer_div").children(".container_title").hide();
                            ui.helper.children(".container_outer_div").children(".progressbar_container").hide();
						    ui.helper.switchClass("closed", "stored", 100, "swing");						
					    }
                    }
				    else {
					    //it is outside the share area
					    if (ui.helper.hasClass("stored")) {	
						    ui.helper.switchClass("stored", "closed", 100, "swing", function() {
							    ui.helper.children(".container_outer_div").children(".container_title").show();
                                ui.helper.children(".container_outer_div").children(".progressbar_container").show();
						    });		
					    }
				    }
			    },
			    stop: function(event, ui) {
				    if (sfb882_doc._inside_share(event)) {
                        var uid = ui.helper.data("uid");
                        var posx = ui.helper.offset().left;
			            var posy = ui.helper.offset().top;
                        var share_x = Math.round(posx - sfb882_doc.data.share_x1);
                        var share_y = Math.round(posy - sfb882_doc.data.share_y1);
                        share_x = share_x + "px";
                        share_y = share_y + "px";
					    if (ui.helper.hasClass("in_share")) {
						    $.ajax({
                                url: OC.filePath('sfb882_doc', 'ajax', 'editUnit.php'),
							    data: {'uid': uid, 'action': "MOVE", 'posx': share_x, 'posy': share_y},
							    dataType: "json",
                                success: function() {
                                    $("#share").append(ui.helper);
                                    ui.helper.css({"top": share_y, "left": share_x});
                                }
                            });
					    }
					    else if (ui.helper.hasClass("in_workspace") || ui.helper.hasClass("in_trash")) {         
                            $.ajax({
							    url: OC.filePath('sfb882_doc', 'ajax', 'shareUnit.php'),
							    data: {'uid': uid, 'posx': share_x, 'posy': share_y},
							    dataType: "json",
							    success: function(result) {
								    ui.helper.switchClass("in_workspace", "in_share");
						            $("#share").append(ui.helper);
                                    ui.helper.css({"top": share_y, "left": share_x});
							    }
						    });
					    }
				    }
                    else if (sfb882_doc._inside_bin(event)) {
                        if (ui.helper.hasClass("in_trash")) {
                            var posx = ui.helper.offset().left;
		                    var posy = ui.helper.offset().top;
                            var bin_x = Math.round(posx - sfb882_doc.data.bin_x1);
                            var bin_y = Math.round(posy - sfb882_doc.data.bin_y1);
                            bin_x = bin_x + "px";
                            bin_y = bin_y + "px";
                            $("#bin").append(ui.helper);
                            ui.helper.css({"top": bin_y, "left": bin_x});
                        }
                    }
				    //event took place outside of share or trash area
				    else {
                        var uid = ui.helper.data("uid");
                        var posx = ui.helper.offset().left;
		                var posy = ui.helper.offset().top; 
                        posx = posx + "px";
                        posy = posy + "px";
					    //remove the unit from share and put in back on the workbench
					    if (ui.helper.hasClass("in_share")) {
						    $.ajax({
					            url: OC.filePath('sfb882_doc', 'ajax', 'unshareUnit.php'),
					            data: {'uid': uid, 'posx': posx, 'posy': posy},
					            dataType: "json",
					            success: function(result) {
						            ui.helper.switchClass("in_share", "in_workspace");
				                    $("#sfb882_doc_content").append(ui.helper);
                                    ui.helper.css({"top": posy, "left": posx});
					            }
				            });                          
					    }
					    if (ui.helper.hasClass("in_trash")) {
                            //remove the unit from trash and put in back on the workbench
					        $.ajax({
				                url: OC.filePath('sfb882_doc', 'ajax', 'untrashUnit.php'),
				                data: {'uid': uid, 'posx': posx, 'posy': posy},
				                dataType: "json",
				                success: function(result) {
					                ui.helper.switchClass("in_trash", "in_workspace");
			                        $("#sfb882_doc_content").append(ui.helper);
                                    ui.helper.css({"top": posy, "left": posx});
				                }
			                });  
					    }
				    }
			    }
		    });   
        } 
        else if (container.hasClass("in_share") && container.data("foreign") == true) {
            container.draggable({ 
			    containment: "#share",
	     		scroll: false,
			    opacity: 0.35,
			    delay: 5,
			    zIndex: 10,
                start: function(event, ui) {
				    sfb882_doc.update_drag_constraints();
			    },
			    stop: function(event, ui) {
                    var uid = ui.helper.data("uid");
                    var posx = ui.helper.offset().left;
		            var posy = ui.helper.offset().top;
                    var share_x = Math.round(posx - sfb882_doc.data.share_x1);
                    var share_y = Math.round(posy - sfb882_doc.data.share_y1);
                    share_x = share_x + "px";
                    share_y = share_y + "px";    
				    $.ajax({
                        url: OC.filePath('sfb882_doc', 'ajax', 'editUnit.php'),
					    data: {'uid': uid, 'action': "MOVE", 'posx': share_x, 'posy': share_y},
					    dataType: "json",
                        success: function() {
                            $("#share").append(ui.helper);
                            ui.helper.css({"top": share_y, "left": share_x});
                        }
                    });
			    }
		    });   
        }
        sfb882_doc_container_internal.load_all_subcontainers(container);
        sfb882_doc_container_internal.init(container);
	},
	init_icons:function() {
		$(".create_icon").draggable({
			containment: "#sfb882_doc_content",
			scroll: false,
			revert: "invalid",
			delay: 5
		});
        $trash_area = $('#sfb882_doc_content').children('.icon_bar').children('.trash_area');
        $export = $('#sfb882_doc_content').children('.icon_bar').children('.export_area');
        sfb882_doc_icons.init_trash($trash_area, 'in_workspace', $('#sfb882_doc_content'), -1);
        sfb882_doc_icons.init_export($export);
        $('#share_drawer').click(function() {
            var $share = $('#share');
            if ($share.hasClass("minimized")) {
                $('#share').switchClass("minimized", "maximized", 400, function() {
                    $('#share_text').show("fade", 100);
                });
            }
            else {
                $('#share_text').hide("fade", 100, function() {
                    $share.switchClass("maximized", "minimized", 400);
                });
            }
        });
        $('#options_drawer').click(function() {
			var $options = $('#options');
			if ($options.hasClass("minimized")) {
                $('#options').switchClass("minimized", "maximized", 400, function() {
					$('#options_pane').show("fade", 100);
				});
            }
            else {
				$('#options_pane').hide("fade", 100, function() {
					$('#options').switchClass("maximized", "minimized", 400);
				});
            }
		});
        $('#bin_drawer').click(function() {
            var $bin = $('#bin');
            $('#bin_text').hide("fade", 100, function() {
                $bin.switchClass("maximized", "minimized", 400, function() {
                    $bin.hide("fade", 250, function() {
                        $bin.children(".container").remove();
                        sfb882_doc.data.trash_bin_open = false;
                    });
                });
            });
        });
        $('#files_drawer').click(function() {
            var $files = $('#files');
            if ($files.hasClass("minimized")) {
                $('#files').switchClass("minimized", "ultramized", 400);
            }
            else {
                $files.switchClass("ultramized", "minimized", 400);
            }
        }); 
        $("#share").draggable({
			containment: "#sfb882_doc_content",
			scroll: false,
			delay: 5,
            handle: $("#share_icon_area")
		});
        $("#files").draggable({
			containment: "#sfb882_doc_content",
			scroll: false,
			delay: 5,
            handle: $("#files_icon_area")
		});
		$("#options").draggable({
			containment: "#sfb882_doc_content",
			scroll: false,
			delay: 5,
            handle: $("#options_icon_area")
		});
	},
    init_layout: function() {
		$("#share").show();
        $("#files").show();
        $("#options").show();
        $("#bin").position({
            my: "left top",
            at: "center",
            of: $(".trash_area")
        });
        $("#share").position({
            my: "left top",
            at: "left bottom",
            of: $("#create_area")
        });
        $("#files").position({
            my: "left top",
            at: "left bottom",
            of: $("#share")
        });
        $("#options").position({
            my: "left top",
            at: "left bottom",
            of: $("#files")
        });
    },
	/*
	* Fetch all units owned by the user from the db and display them.
	*/
	load_all_units: function() {
		$.ajax({
			url: OC.filePath('sfb882_doc', 'ajax', 'loadUnits.php'),
			dataType: "json",
            data: {'location': 'top_level'},
			success: function(result) {
				for (var i = 0; i < result.data.length; i++) {
					unit = result.data[i];
					sfb882_doc.load_toplevel_unit(unit, true);							
				}  
			}
		});
        $.ajax({
            url: OC.filePath('sfb882_doc', 'ajax', 'getShares.php'),
			dataType: "json",
			success: function(result) {
				for (var i = 0; i < result.data.length; i++) {
					sfb882_doc.load_toplevel_unit(result.data[i], false);
				}
			}
        });
	},
	get_unit_by_uid: function(unit_list, uid_to_find) {
		for (var i = 0; i < unit_list.length; i++) {
			if (unit_list[i].uid == uid_to_find) {
				return unit_list[i];
			}
		}
	},
	align_container_to_share: function($container, $parent) {
		$dummy = $('<div class="maximized"></div>');

		var left = parseInt($container.css('left'), 10);
		var top = parseInt($container.css('top'), 10);
		var max_left = $dummy.width() - $container.width();
		var max_top = $dummy.height() - $container.height();
		
		var handle_min_left = $("#share_icon_area").width() + $("#share_drawer").width();
		var handle_min_top = $("#share_icon_area").height();
		
		left = (left < 0) ? 0 : left;
		left = (left > max_left) ? max_left : left;
		top = (top < 0) ? 0 : top;
		top = (top > max_top) ? max_top : top;
		left = (left < handle_min_left && top < handle_min_top) ? handle_min_left : left;
		 
		$container.css({'left': left + 'px', 'top': top + 'px'});
	},
	load_toplevel_unit: function(unit, self_owned) {
		$.ajax({
			url: OC.filePath('sfb882_doc', 'ajax', 'generateContainer.php'),
			data: {'uid': unit.uid},
			dataType: "json",
			success: function(result) {
				if (unit.location == "workbench") {
					$new_container = $(result.html_data);
					$new_container.addClass("in_workspace");
					$new_container.addClass("closed");
					$("#sfb882_doc_content").append($new_container);
					$new_container.css({'left': unit.posx, 'top': unit.posy, 'background-color': unit.color});
					$new_container.find($(".container_title")).show();
					$new_container.data({
                        "uid": unit.uid,
                        "foreign": false,
                        "color": unit.color,
                        "container_name": unit.container_name
                    });
					$new_container.show("highlight", "200");
					sfb882_doc.init_container($new_container);
				}
                else if (unit.location == "share") {             
					$new_container = $(result.html_data);
					$new_container.addClass("in_share");
					$new_container.addClass("stored");
					$("#share").append($new_container);
					$new_container.css({'left': unit.posx, 'top': unit.posy, 'background-color': unit.color});		
					sfb882_doc.align_container_to_share($new_container, $("#share"));
					$new_container.data({
                        "uid": unit.uid,
                        "foreign": false,
                        "color": unit.color,
                        "container_name": unit.container_name
                    });
					$new_container.show("highlight", "200");
                    $new_container.children(".container_outer_div").children(".progressbar_container").hide();
                    if (self_owned == false) {
                        $new_container.data("foreign", true);
                        $share_icon = $('<div class="share_icon"></div>');
                        $new_container.children(".container_outer_div").append($share_icon);
                        $.ajax({
                            url: OC.filePath('sfb882_doc', 'ajax', 'getMeta.php'),
                            data: {'uid': unit.uid},
                            dataType: "json",
                            success: function(result) {
                                if (result.data) {
                                    sfb882_doc.generate_tooltip($new_container, result.data);
                                }
                            }
                        });
                    }
                    sfb882_doc.init_container($new_container);
                }
			}
		});
	},
    build_new_container: function(uid) {
		$.ajax({
			url: OC.filePath('sfb882_doc', 'ajax', 'generateContainer.php'),
			data: {'uid': uid},
			dataType: "json",
			success: function(result) {
				var $container = $(".create_icon");
				var color = result.color;
				$container.removeClass("icon_bar_pos_1");
				$container.addClass("in_workspace");
				$container.append($(result.html_data).children());
				$container.switchClass("create_icon", "closed", 500, "swing", function() {
					$container.animate({"background-color": color}, 500, function() {
						$container.children(".container_outer_div").children(".container_title").show("slide", 500, function() {
                            $container.detach();
				            $("#sfb882_doc_content").append($container);
				            //Attach the uid directly to the container
				            $container.data({
                                "uid": uid,
                                "color": color,
                                "foreign": false
                            });                            						
				            sfb882_doc.init_container($container);
				            var new_icon = '<div class="container create_icon icon_bar_pos_1"></div>';
				            $("#create_area").append(new_icon);
				            sfb882_doc.init_icons();
                        });
					});
				});
			},
			error: function(jqXHR, textstatus, error) {
				sfb882_doc.notify("Something went wrong! Status: " + jqXHR + "|||" + textstatus + ", Error: " + error);
			}
        });
    },
    /*
    Creates a new container in the DB and returns the generated uid as a callback parameter
    */
	create_container: function(parent_uid, container_name, research_type, location, container_type, posx, posy, color, callback) {
		$.ajax({
			url: OC.filePath('sfb882_doc', 'ajax', 'newContainer.php'),
			data: {'parent_uid': parent_uid, 'container_name': container_name, 'research_type': research_type, 'location': location, 'container_type': container_type, 'posx': posx, 'posy': posy, 'color': color},
			success: function(result) {
                if (callback) {
                    callback(result);
                }
			}
		});
	},
    generate_tooltip: function($container, metadata) {
		//name
		var tooltip_text = t("sfb882_doc", "container_name") + metadata.container_name;
        //owner
        tooltip_text += "<br />" + t("sfb882_doc", "owner_title") + metadata.displayname;
        //creation_date
        tooltip_text += "<br />" + t("sfb882_doc", "creation_date_title");
        if (metadata.creation_date === null) {
            tooltip_text += t("sfb882_doc", "unknown_creation_date");
        }
        else {
            var text = t("sfb882_doc", "creation_date");
            text = text.replace("%CREATION_DATE", metadata.creation_date);
            tooltip_text += text;
        }
        //last_modification
        tooltip_text += "<br />" + t("sfb882_doc", "last_mod_title");
        if (metadata.lm_type === null || metadata.lm_date === null || metadata.lm_user === null) {
            tooltip_text += t("sfb882_doc", "unknown_last_mod");
        }
        else {
            var text = t("sfb882_doc", "last_mod");
            text = text.replace("%LM_DATE", metadata.lm_date);
            text = text.replace("%LM_USER", metadata.lm_user);
            text = text.replace("%LM_TYPE", t("sfb882_doc", "%" + metadata.lm_type));
            tooltip_text += text;
        }
        //the tooltip won't show if no title attrib is set, although it can be empty. Strange.
        $container.attr("title", "");  
        $container.tooltip({
            tooltipClass: "share_tooltip",
            content: tooltip_text,
            track: true
        });
    },
	/*
	* Useful code snippet from Jquery UI demos (http://jqueryui.com/slider/#colorpicker)
	*/
	rgb_to_hex: function (r, g, b) {
		var hex = [
			r.toString( 16 ),
			g.toString( 16 ),
			b.toString( 16 )
		];
		$.each( hex, function( nr, val ) {
			if ( val.length === 1 ) {
				hex[ nr ] = "0" + val;
			}
		});
		return "#" + hex.join( "" ).toUpperCase();
	},
    /*
    * ...and the other way, this time from sof (https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb)
    */
    hex_to_rgb: function(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    },
	/*
	* Set the color of the example container according to the slider values
	*/
	change_example_color: function() {
		var red = $("#color_slider_red").slider("value");
		var green = $("#color_slider_green").slider("value");
		var blue = $("#color_slider_blue").slider("value");
		var hexvalue = sfb882_doc.rgb_to_hex(red, green, blue);
		$("#example_container").css({"background": hexvalue});
	},
	init_dialogs: function() {
		$("#color_slider_red, #color_slider_green, #color_slider_blue").slider({
		    orientation: "horizontal",
		    range: "min",
		    max: 255,
		    slide: sfb882_doc.change_example_color,
		    change:sfb882_doc.change_example_color
	    });
	    $("#color_slider_red").slider("value", 153);
	    $("#color_slider_green").slider("value", 204);
	    $("#color_slider_blue").slider("value", 0);
        $('#container_name_input').on('input', function() {
            $("#example_container_title").text($("#container_name_input").val());
        });           
	},
    init_file_section: function(dir) {
        $.ajax({
			url: OC.filePath('sfb882_doc', 'ajax', 'getFileSection.php'),
			dataType: "json",
            data: {'dir': dir},
			success: function(result) {
                $("#testarea").empty();
				$("#testarea").append(result.data);
                $(".name").click(function() {
                    var $parent_tr = $(this).parent("td").parent("tr");
                    if ($parent_tr.attr("data-type") == "dir") {
                        sfb882_doc.init_file_section(decodeURI($(this).attr("data-filepath")));
                    }
                });
                $(".crumb").click(function() {
                    sfb882_doc.init_file_section(decodeURI($(this).attr("data-dir")));
                });
                var $files = $("td.filename");
                $files.each(function() {         
                    if ($(this).parent().attr("data-type") == "file") {
                        $(this).draggable({
                            containment: "#sfb882_doc_content",
                            revert: "invalid",
	                 		scroll: false,
			                opacity: 0.7,
                            zIndex: 100,
                            cursor: "move",
                            appendTo: "#sfb882_doc_content",
                            helper: createDragShadow
                        });
                    }
                })
			},
        });
    },
    init_options_pane: function() {
		$("#animation_time_slider").slider({
		    orientation: "horizontal",
		    value: 50,
		    max: 100,
		    slide: function() {
				sfb882_doc_container_internal.animation_constants.time_factor = $("#animation_time_slider").slider("value") / 100;
			},
		    change: function() {
				sfb882_doc_container_internal.animation_constants.time_factor = $("#animation_time_slider").slider("value") / 100;
			}
	    });
	}
}

/*dragshadow*/
var createDragShadow = function(event){
    $file = $(event.target).parents('tr');
	var dragshadow = $('<table class="dragshadow"></table>');
	var tbody = $('<tbody></tbody>');
	dragshadow.append(tbody);
    tbody.append($file.clone());
	return dragshadow;
}


$(document).ready(function(){
	if (!$("#sfb882_doc_content").length) {
		return;
	}
    sfb882_doc.notify("Initializing...");
	sfb882_doc.load_all_units();
	sfb882_doc.init_icons();
    sfb882_doc.init_layout();
    sfb882_doc.init_dialogs();
    sfb882_doc.init_options_pane();
    sfb882_doc.init_file_section("");
	$("#sfb882_doc_content").droppable({
		accept: ".create_icon, .in_workspace",
		drop: function(event, ui) {
			if (ui.draggable.hasClass("create_icon")) {
                $('#research_type_selection').hide();
                $('#pretest_conducted_selection').hide();
				$("#container_options_dialog").dialog({
					height: 400,
					width: 400,
                    modal: true,
                    title: t("sfb882_doc", "options_dialog_create_title"),
                    dialogClass: "container_create_dialog",
					resizable: false,
					buttons: [
					{
						text: t("sfb882_doc", "options_dialog_create"),
						click: function() {
							var unitname = $("#container_name_input").val();
                            $("#container_name_input").val("");
							var posx = ui.draggable.css("left");   
							var posy = ui.draggable.css("top");      
							var color = sfb882_doc.rgb_to_hex(	
								$("#color_slider_red").slider("value"), 
								$("#color_slider_green").slider("value"), 
								$("#color_slider_blue").slider("value")
							);
							sfb882_doc.create_container(-1, unitname, 'mixed_with_pretest', 'workbench', 'top_level', posx, posy, color, function(result){
                                var tl_uid = result.data.uid;
                                sfb882_doc.build_new_container(tl_uid);
                                /*sfb882_doc.create_container(tl_uid, 'Allgemeine Angaben', 'mixed_with_pretest', 'sidebar', 'general_information', 0, 0, color, function(){
                                    sfb882_doc.build_new_container(tl_uid);
                                });*/
                            });
							$(this).dialog("close");
						}
					},
					{
						text: t("sfb882_doc", "options_dialog_abort"),
						click: function() {
							$(this).dialog("close");
							var dummy = $("#icon_bar_pos_1_placeholder");
							$(".create_icon").animate({top: dummy.css("top"), left: dummy.css("left")}, 200);
						}
					}]
				});
			}
			else if (ui.draggable.hasClass("in_workspace") && ui.draggable.hasClass("closed")) {
				var posx = ui.draggable.css("left");
				var posy = ui.draggable.css("top");
				var uid = ui.draggable.data("uid");
				$.ajax({
					url: OC.filePath('sfb882_doc', 'ajax', 'editUnit.php'),
					data: {'uid': uid, 'action': "MOVE", 'posx': posx, 'posy': posy},
					dataType: "json"
				});
			}
		}
	});
});
