/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
sfb882_doc_file_section = {
    /*
    Note: Would be nice to simply model the templates bottom-up, but self-reference in object literals is tricky.
    We have to use getters when including templates, see
    https://stackoverflow.com/questions/4616202/self-references-in-object-literal-declarations
    */
    tmpl: {
        file_selected_area : '<div class="file_selected_area" />',
        file_dropper : '<div class="file_dropper" title="' + t("sfb882_doc", "file_dropper_desc") + '"></div',
        file_list : '<div class="file_list" />',
        fisf_name : t("sfb882_doc", "fs_file_name") + '<input type="text" class="fis_filename" readonly="readonly" /><br />',
        fisf_mime : t("sfb882_doc", "fs_mime_type") + '<input type="text" class="fis_mimetype" readonly="readonly" /><br />',
        fisf_chksum : t("sfb882_doc", "fs_chksum") + '<input type="text" class="fis_checksum" readonly="readonly" /><br />',
        fisf_size : t("sfb882_doc", "fs_file_size") + '<input type="text" class="fis_filesize" readonly="readonly" /><br />',
        fisf_origin : t("sfb882_doc", "fs_origin") + '<input type="text" class="fis_origin" readonly="readonly" /><br />',
        fisf_created : t("sfb882_doc", "fs_created") + '<input type="text" class="fis_creation" readonly="readonly" /><br />',
        get fis_fieldset_general() { 
            return '<fieldset class="file_info_section_fieldset"><legend>' + t("sfb882_doc", "fs_title_general") + '</legend>'+this.fisf_name+this.fisf_mime+this.fisf_chksum+this.fisf_size+this.fisf_origin+this.fisf_created+'</fieldset>';
        },
        fisf_active : t("sfb882_doc", "fs_active_cb") + '<input type="checkbox" class="fis_active_cb" /><br />',
        fisf_public : t("sfb882_doc", "fs_exportable_cb") + '<input type="checkbox" class="fis_public_cb" /><br />',
        //fisf_rm_btn : '<input type="button" value="Datei entfernen" class="fis_remove_button" />', //TODO: Implement file removal!
        fisf_dl_btn : '<input type="button" value="Download" class="fis_download_button" />',
        get fis_fieldset_options() { 
            return '<fieldset class="file_info_section_fieldset"><legend>' + t("sfb882_doc", "fs_title_options") + '</legend>'+this.fisf_active+this.fisf_public/*+this.fisf_rm_btn*/+this.fisf_dl_btn+'</fieldset>';
        },
        fisf_comments : '<textarea class="fis_comments" /><div class="save input_button" />',
        get fis_fieldset_desc() { 
            return '<fieldset class="file_info_section_fieldset"><legend>' + t("sfb882_doc", "fs_title_desc") + '</legend>'+this.fisf_comments+'</fieldset>';
        },
        get file_info_section() {
            return '<div class="file_info_section"><form>' + this.fis_fieldset_general + this.fis_fieldset_options + this.fis_fieldset_desc + ' </form></div>';
        }
    },
    set_file_section_checkboxes: function($file_info_section, active, public) {
        var $fis_active_cb = $file_info_section.find(".fis_active_cb");
        var $fis_public_cb = $file_info_section.find(".fis_public_cb");
        if (active == 1) {
            $fis_active_cb.prop('checked', true);
            $fis_public_cb.prop('disabled', false);
            if (public == 1) {
                $fis_public_cb.prop('checked', true);
            }
            else {
                $fis_public_cb.prop('checked', false);
            }
        }
        else {
            $fis_active_cb.prop('checked', false);
            $fis_public_cb.prop('checked', false);
            $fis_public_cb.prop('disabled', true);
        }
    },
    connect_file_icon: function($file_icon, $file_info_section, $file_selected_area, uid, field_id) {
        $file_icon.click(function(event){
            $file_selected_area.empty();
            $file_icon.removeClass("file_icon_selected");
            var $cloned_icon = $file_icon.clone(true);
            $file_selected_area.append($cloned_icon);
            $file_info_section.find(".fis_filename").val($file_icon.data().file_alias);
            $file_info_section.find(".fis_mimetype").val($file_icon.data().mime_type);
            $file_info_section.find(".fis_checksum").val($file_icon.data().chksum);
            $file_info_section.find(".fis_filesize").val($file_icon.data().file_size);
            $file_info_section.find(".fis_origin").val($file_icon.data().file_origin);
            $file_info_section.find(".fis_creation").val($file_icon.data().creation_date);
            $file_info_section.find(".fis_comments").val($file_icon.data().description);
            sfb882_doc_file_section.set_file_section_checkboxes($file_info_section, $file_icon.data().active, $file_icon.data().public);    
            $file_info_section.find(".fis_download_button").click(function(event){
                window.location=OC.filePath('sfb882_doc', 'ajax', 'downloadFile.php') + '?'+ $.param({'uid': uid, 'field_id': field_id, 'chksum': $file_icon.data().chksum});
            });
            $file_icon.siblings(".file_icon_selected").removeClass("file_icon_selected");   
            $file_icon.addClass("file_icon_selected");
        });
    },
    update_file_field_title: function($file_field) {
        var $title = $file_field.prev(".content_title");
        var $file_icons = $file_field.find('.file_list').find(".file_icon");
        var title_string = "";
        $file_icons.each(function(){
            if ($(this).data('active') == 1) {
                title_string += $(this).data('file_alias') + ', ';
            }
        });
        if (title_string.length > 1) {
            title_string = title_string.slice(0, -2); //remove the last ', '
        }
        sfb882_doc_container_internal.set_field_title($title, title_string);
    }, 
    create_file_icon: function(data_array) {
        var active_string = '';
        if (data_array.active != 0) {
            active_string = ' file_active';
        }
        var public_string = '';
        if (data_array.public != 0) {
            public_string = '<div class="file_public_image" />';
        }
        var file_name = data_array.file_alias;
        var mime_icon_path = data_array.mime_icon_path;
        var $file_icon = $('<div class="file_icon"><div class="file_icon_shape' + active_string + '" style="background-image: url('+ mime_icon_path +')">' + public_string + '</div><div class="file_icon_text">' + file_name + '</div></div>');   
        $file_icon.data(data_array);
        return $file_icon;
    },
    update_file_icons: function($container, $file_field, chksum, data) {
        var public = data['public'];
        var active = data['active'];
        //Find all file icons with the correct chksum;
        var $file_icons = $file_field.find(".file_icon").filter(function(){
            return ($(this).data("chksum") == chksum);
        });
        $file_icons.data('public', public);
        $file_icons.data('active', active);
        $file_icons.data('description', data['description']);
        if (active == 1) {
            $file_icons.children(".file_icon_shape").addClass("file_active");
        }
        else {
            $file_icons.children(".file_icon_shape").removeClass("file_active");
        }
        $file_icons.find(".file_public_image").remove();
        if (public == 1) {
            $file_icons.children(".file_icon_shape").append($('<div class="file_public_image" />'));
        }
        sfb882_doc_file_section.update_file_field_title($file_field);    
        sfb882_doc_container_internal.update_progressbar($container);
    },
    create_file_sections: function($container) {
        var $file_fields = $container.find(".content_input_file_field");
        $file_fields.each(function(){
            var $file_field = $(this);
            var field_id = $file_field.attr('data-file-field-id');
            var $file_dropper = $(sfb882_doc_file_section.tmpl.file_dropper);
            $file_field.append($file_dropper);
            var $file_selected_area = $(sfb882_doc_file_section.tmpl.file_selected_area);
            $file_field.append($file_selected_area);
            var $file_info_section = $(sfb882_doc_file_section.tmpl.file_info_section);          
            $file_field.append($file_info_section);
            var $file_list = $(sfb882_doc_file_section.tmpl.file_list);
            $file_field.append($file_list);
            $file_dropper.droppable({
                accept: ".filename",
                activeClass: "dropper_active",
                hoverClass: "dropper_hover",
                tolerance: 'pointer',
                drop: function(event, ui) {
                    var file_id = ui.helper.find("[data-id]").attr("data-id");
                    //var filename = location.origin + filepath //DL-URL
                    $.ajax({
                        url: OC.filePath('sfb882_doc', 'ajax', 'storeFile.php'),
		                data: {'uid': uid, 'file_id': file_id, 'field_id': field_id},
                        dataType: "json",
		                success: function(result) {
                            $file_icon = sfb882_doc_file_section.create_file_icon(result.data);
                            sfb882_doc_file_section.connect_file_icon($file_icon, $file_info_section, $file_selected_area, uid, field_id);
                            $file_list.append($file_icon);
                            sfb882_doc_file_section.update_file_field_title($file_field);
                            sfb882_doc_container_internal.update_progressbar($container);
                            $file_icon.click();
                        }
                    });
                }
            });
            // active/public checkboxes mechanics
            var $fis_active_cb = $file_info_section.find(".fis_active_cb");
            var $fis_public_cb = $file_info_section.find(".fis_public_cb");            
            $fis_active_cb.change(function(event){
                var $file_icon = $file_selected_area.find(".file_icon");
                if ($file_icon.length > 0) {
                    var action = 'SET_INACTIVE';
                    if ($fis_active_cb.prop('checked') == true) {
                        var action = 'SET_ACTIVE';
                    }
                    $.ajax({
                        url: OC.filePath('sfb882_doc', 'ajax', 'editFile.php'),
	                    data: {'uid': uid, 'field_id': field_id, 'chksum': $file_icon.data().chksum, 'action': action},
                        dataType: "json",
	                    success: function(result) {
                           sfb882_doc_file_section.set_file_section_checkboxes($file_info_section, result.data['active'], result.data['public']);
                           sfb882_doc_file_section.update_file_icons($container, $file_field, $file_icon.data().chksum, result.data);
                        }
                    });
                }
            });
            $fis_public_cb.change(function(event){
                var $file_icon = $file_selected_area.find(".file_icon");
                if ($file_icon.length > 0) {
                    var action = 'SET_PRIVATE';
                    if ($fis_public_cb.prop('checked') == true) {
                        var action = 'SET_PUBLIC';
                    }
                    $.ajax({
                        url: OC.filePath('sfb882_doc', 'ajax', 'editFile.php'),
	                    data: {'uid': uid, 'field_id': field_id, 'chksum': $file_icon.data().chksum, 'action': action},
                        dataType: "json",
	                    success: function(result) {
                           sfb882_doc_file_section.set_file_section_checkboxes($file_info_section, result.data['active'], result.data['public']);
                           sfb882_doc_file_section.update_file_icons($container, $file_field, $file_icon.data().chksum, result.data);
                        }
                    });
                }
            });
            // comments save button mechanics
            var save_button = $file_info_section.find(".save.input_button");
            save_button.click(function(event){
                var $file_icon = $file_selected_area.find(".file_icon");
                var text = $file_info_section.find(".fis_comments").val();
                $.ajax({
                    url: OC.filePath('sfb882_doc', 'ajax', 'editFile.php'),
                    data: {'uid': uid, 'field_id': field_id, 'chksum': $file_icon.data().chksum, 'action': 'SET_DESCRIPTION', 'payload': text},
                    dataType: "json",
                    success: function(result) {
                       sfb882_doc_file_section.update_file_icons($container, $file_field, $file_icon.data().chksum, result.data);
                    }
                });
            });
        });
        //Load all stored files
        var uid = $container.data('uid');
        $.ajax({
            url: OC.filePath('sfb882_doc', 'ajax', 'loadFiles.php'),
            data: {'uid': uid},
            dataType: "json",
            success: function(result) {
                if (result.status == "success") { 
                    for (var i = 0; i < result.data.length; i++) {
                        var field_id = result.data[i].field;
                        var $file_field = $container.find('.content_input_file_field[data-file-field-id="'+ field_id +'"]');
                        var $file_info_section = $file_field.find(".file_info_section");
                        var $file_selected_area = $file_field.find(".file_selected_area");
                        var $file_list = $file_field.find(".file_list");
                        $file_icon = sfb882_doc_file_section.create_file_icon(result.data[i]);
                        sfb882_doc_file_section.connect_file_icon($file_icon, $file_info_section, $file_selected_area, uid, field_id);
                        $file_list.append($file_icon);
                        sfb882_doc_file_section.update_file_field_title($file_field);
                        sfb882_doc_container_internal.update_progressbar($container);
                    }
                }
            }
        });
    }
}
