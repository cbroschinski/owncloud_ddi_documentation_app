<DDIInstance xmlns="ddi:instance:3_2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="ddi:instance:3_2 http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/instance.xsd" xmlns:s="ddi:studyunit:3_2" xmlns:r="ddi:reusable:3_2" xmlns:d="ddi:datacollection:3_2" xmlns:a="ddi:archive:3_2" >
  <r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_ddiinstance:1</r:URN>
  <s:StudyUnit>
    <r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_studyunit:1</r:URN>
    <?php p($_['%related_theories_input%'])?>
    <r:Citation>
      <r:Title>
        <r:String><?php p($_['%study_full_name_input%'])?></r:String>
      </r:Title>
      <r:AlternateTitle>
        <r:String><?php p($_['%study_short_name_input%'])?></r:String>
      </r:AlternateTitle>
      <?php p($_['%project_head_input%'])?>
      <?php p($_['%project_members_input%'])?>
    </r:Citation>
    <r:Abstract>
			<r:Content><?php p($_['%project_short_description_input%'])?></r:Content>
    </r:Abstract>
    <r:FundingInformation>
      <r:Description>
        <r:Content><?php p($_['%funding_input%'])?></r:Content>
      </r:Description>
    </r:FundingInformation>
    <r:Purpose>
			<r:Content><?php p($_['%inquiry_contents_input%'])?></r:Content>
		</r:Purpose>
    <r:Coverage>
      <r:TopicalCoverage>
        <r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_topcov:1</r:URN>
        <?php p($_['%project_research_area_input%'])?>
        <?php p($_['%study_design_ca%'])?>
        <?php p($_['%keywords_input%'])?>
      </r:TopicalCoverage>
      <r:TemporalCoverage>
        <r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_tempcov:1</r:URN>
        <r:ReferenceDate>
          <r:StartDate><?php p($_['%time_span_begin_input%'])?></r:StartDate>
          <r:EndDate><?php p($_['%time_span_end_input%'])?></r:EndDate>
        </r:ReferenceDate>
      </r:TemporalCoverage>
    </r:Coverage>
    <?php p($_['%primary_data_sources_input%'])?>
    <?php p($_['%project_publications_input%'])?>
    <?php p($_['%project_data_publications_input%'])?>
    <r:OtherMaterial>
      <r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_projurl:1</r:URN>
      <r:ExternalURLReference><?php p($_['%project_url_input%'])?></r:ExternalURLReference>
    </r:OtherMaterial>
    <d:DataCollection>
			<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_datacolfieldphase:1</r:URN>
			<r:Note>
				<r:Relationship>
					<r:RelatedToReference>
						<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_datacolfieldphase:1</r:URN>
						<r:TypeOfObject>DataCollection</r:TypeOfObject>
					</r:RelatedToReference>
				</r:Relationship>
				<r:NoteContent>
					<r:Content>DataCollection represents fieldwork</r:Content>
				</r:NoteContent>
			</r:Note>
			<?php p($_['%documentation_field_contacts_text_input%'])?>
			<r:Description>
				<r:Content><?php p($_['%inquiry_title_input%'])?></r:Content>
			</r:Description>
			<r:Coverage>
				<r:TopicalCoverage>
					<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_fieldphasetopcov:1</r:URN>
					<?php p($_['%target_group_ca%'])?>
				</r:TopicalCoverage>
				<r:SpatialCoverage>
					<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_fieldphasespatcov:1</r:URN>
					<?php p($_['%inquiry_location_input%'])?>
				</r:SpatialCoverage>
				<r:TemporalCoverage>
					<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_fieldphasetempcov:1</r:URN>
					<r:ReferenceDate>
						<r:StartDate><?php p($_['%field_contacts_time_begin_input%'])?></r:StartDate>
						<r:EndDate><?php p($_['%field_contacts_time_end_input%'])?></r:EndDate>
					</r:ReferenceDate>
				</r:TemporalCoverage>
			</r:Coverage>
			<r:OtherMaterial>
				<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_fieldworkurl:1</r:URN>
				<r:ExternalURLReference>
					<?php p($_['%inquiry_homepage_input%'])?>
				</r:ExternalURLReference>
			</r:OtherMaterial>
			<d:Methodology>
				<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_methodology:1</r:URN>
				<?php p($_['%applied_research_methods_ca%'])?>
				<?php p($_['%field_phase_sampling_type_ca%'])?>
				<d:DeviationFromSampleDesign>
					<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_devfromsampdesign:1</r:URN>
					<r:Description>
						<r:Content><?php p($_['%original_sample_design_deviation_input%'])?></r:Content>
					</r:Description>
				</d:DeviationFromSampleDesign>
			</d:Methodology>
			<d:CollectionEvent>
				<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_collevent:1</r:URN>
				<?php p($_['%conducting_organization_link%'])?>
				<?php p($_['%applied_technologies_ca%'])?>
				<d:ActionToMinimizeLosses>
					<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_acttomin:1</r:URN>
					<r:Description>
						<r:Content><?php p($_['%response_rate_increasing_measures_input%'])?></r:Content>
					</r:Description>
				</d:ActionToMinimizeLosses>
			</d:CollectionEvent>
			<d:ProcessingEventScheme>
				<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_proceventscheme:1</r:URN>
				<?php p($_['%data_sanitization_text_input%'])?>
				<?php p($_['%weighting_factors_creation_text_input%'])?>
			</d:ProcessingEventScheme>
		</d:DataCollection>
		<?php p($_['sfb882_doc_ddi_template_pretest'])?>
		<a:Archive>
			<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_archive:1</r:URN>
			<a:OrganizationScheme>
				<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_orgscheme:1</r:URN>
				<?php p($_['%conducting_organization_input%'])?>
				<?php p($_['%pretest_conducting_organization_input%'])?>
			</a:OrganizationScheme>
		</a:Archive>
  </s:StudyUnit>
</DDIInstance>
