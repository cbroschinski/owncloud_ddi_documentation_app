<div id="container_<?php p('uid'.$_['uid'])?>" class="container closed field_phase lower_two lower_two_orig" style="display:none">
  <div class="container_sibling">
  </div>
  <div class="container_outer_div">
    <div class="container_title small_title">
      <?php p($l->t("field_phase"))?>
      <div class="close_button title_button" style="display: none"></div>
      <div class="history_button title_button" style="display: none"></div>
    </div>
    <div class="progressbar_container">
      <div class="progressbar">
        <div class="progress_label">
        </div>
      </div>
    </div>

    <div class="container_content" style="display:none">

      <div class="content_title">
        <span class="title_string"><?php p($l->t("inquiry_title_input"))?></span>
      </div>
      <div class="content_input">
        <input data-text-input-id="inquiry_title_input" data-mandatory="*+" title="Name (wenn vorhanden)" data-ddi-placeholder="%inquiry_title_input%" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("conducting_organization_input"))?></span>
      </div>
      <div class="content_input">
        <input data-text-input-id="conducting_organization_input" data-mandatory="*+" title="Name (beispielsweise Institut)" data-ddi-placeholder="%conducting_organization_input% %conducting_organization_link%" data-ddi-wrapper="{a:Organization}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_org_%CHKSUM%:1{/r:URN}{a:OrganizationIdentification}{a:OrganizationName}{r:String}%conducting_organization_input%{/r:String}{/a:OrganizationName}{/a:OrganizationIdentification}{/a:Organization}%%%{d:DataCollectorOrganizationReference}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_org_%CHKSUM%:1{/r:URN}{r:TypeOfObject}Organization{/r:TypeOfObject}{/d:DataCollectorOrganizationReference}" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("field_phase_population_group_target_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="+ *" data-checkbox-area-id="field_phase_population_group_target_ca" data-checkbox-extensible="true">
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="individuals_cb" /><?php p($l->t("individuals_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="groups_cb" /><?php p($l->t("groups_cb"))?></div> 
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="organisations_cb" /><?php p($l->t("organisations_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="situations_cb" /><?php p($l->t("situations_cb"))?></div>         
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="interactions_cb" /><?php p($l->t("interactions_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="communications_cb" /><?php p($l->t("communications_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="documents_cb" /><?php p($l->t("documents_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="artefacts_cb" /><?php p($l->t("artefacts_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="household_cb" /><?php p($l->t("household_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="family_cb" /><?php p($l->t("family_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="family_households_cb" /><?php p($l->t("family_households_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="geographic_unit_cb" /><?php p($l->t("geographic_unit_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="time_unit_cb" /><?php p($l->t("time_unit_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="text_unit_cb" /><?php p($l->t("text_unit_cb"))?></div> 
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("target_group_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="+ *" data-checkbox-area-id="target_group_ca" data-checkbox-extensible="true" data-ddi-placeholder="%target_group_ca%" data-ddi-wrapper='{r:Subject codeListName="ObjectOfStudy" codeListAgencyName="sfb882"}%target_group_ca%{/r:Subject}' >
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="face-to-face_interaction_cb" /><?php p($l->t("face-to-face_interaction_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="verbal_interaction_cb" /><?php p($l->t("verbal_interaction_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="non-verbal_interaction_cb" /><?php p($l->t("non-verbal_interaction_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="communication_cb" /><?php p($l->t("communication_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="formal_communication_mechanisms_cb" /><?php p($l->t("formal_communication_mechanisms_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="interpretation_pattern_cb" /><?php p($l->t("interpretation_pattern_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="sensual_structures_cb" /><?php p($l->t("sensual_structures_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="case_pattern_cb" /><?php p($l->t("case_pattern_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="morale_cb" /><?php p($l->t("morale_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="ethnological_methods_cb" /><?php p($l->t("ethnological_methods_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="discurses_cb" /><?php p($l->t("discurses_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="positionings_cb" /><?php p($l->t("positionings_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="ideologies_weltanschauungs_cb" /><?php p($l->t("ideologies_weltanschauungs_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="semantics_cb" /><?php p($l->t("semantics_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="knowledge_bases_cb" /><?php p($l->t("knowledge_bases_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="knowledge_production_cb" /><?php p($l->t("knowledge_production_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="koerperpraxen_cb" /><?php p($l->t("koerperpraxen_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="social_manners_cb" /><?php p($l->t("social_manners_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="emotions_cb" /><?php p($l->t("emotions_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="communication_types_cb" /><?php p($l->t("communication_types_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="networks_cb" /><?php p($l->t("networks_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="social_networks_cb" /><?php p($l->t("social_networks_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="social_groups_cb" /><?php p($l->t("social_groups_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="online_communities_cb" /><?php p($l->t("online_communities_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="rhetorics_cb" /><?php p($l->t("rhetorics_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="biografies_cb" /><?php p($l->t("biografies_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="technologies_artefacts_interaction_cb" /><?php p($l->t("technologies_artefacts_interaction_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="movies_cb" /><?php p($l->t("movies_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="action_orientation_cb" /><?php p($l->t("action_orientation_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="institutions_cb" /><?php p($l->t("institutions_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="organisations_cb" /><?php p($l->t("organisations_cb"))?></div>
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("inquiry_location_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="inquiry_location_input" data-mandatory="+ *" title="Region" data-ddi-placeholder="%inquiry_location_input%" data-ddi-wrapper="{r:Description}{r:Content}%inquiry_location_input%{/r:Content}{/r:Description}" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("field_contacts_time_begin_input"))?></span>
      </div>
      <div class="content_input date">
        <input data-text-input-id="field_contacts_time_begin_input" data-mandatory="+ *" title="Beschreibung" data-ddi-placeholder="%field_contacts_time_begin_input%" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("field_contacts_time_end_input"))?></span>
      </div>
      <div class="content_input date">
        <input data-text-input-id="field_contacts_time_end_input" data-mandatory="+ *" title="Beschreibung" data-ddi-placeholder="%field_contacts_time_end_input%" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("preparing_activities_for_inquiry_text_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="preparing_activities_for_inquiry_text_input" title="z.B. Mitarbeiterschulung, Verhaltensregeln, Intervieweranweisung" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("preparing_activities_for_inquiry_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-file-field-id="preparing_activities_for_inquiry_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("applied_research_methods_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="+ *" data-checkbox-area-id="applied_research_methods_ca" data-checkbox-extensible="true" data-ddi-placeholder="%applied_research_methods_ca%" data-ddi-wrapper='{d:DataCollectionMethodology}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_method_%CHKSUM%:1{/r:URN}{d:TypeOfDataCollectionMethodology codeListName="DataCollectionMode" codeListAgencyName="sfb882"}%applied_research_methods_ca%{/d:TypeOfDataCollectionMethodology}{/d:DataCollectionMethodology}'>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="guided_interview" /><?php p($l->t("threaded_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="focused_interview_cb" /><?php p($l->t("focused_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="half_standardized_interview_cb" /><?php p($l->t("half_standardized_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="problem_centered_interview_cb" /><?php p($l->t("problem_centered_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="expert_interview_cb" /><?php p($l->t("expert_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="ethnographic_interview_cb" /><?php p($l->t("ethnographic_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="ero_epic_conversation" /><?php p($l->t("ero_epic_conversation_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="narrations_cb" /><?php p($l->t("narrations_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="narrative_interview_cb" /><?php p($l->t("narrative_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="episodic_interview_cb" /><?php p($l->t("episodic_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="group_interviews_cb" /><?php p($l->t("group_interviews_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="group_discussion_cb" /><?php p($l->t("group_discussions_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="focus_groups_cb" /><?php p($l->t("focus_groups_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="online_focus_groups_cb" /><?php p($l->t("online_focus_groups_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="group_narrations_cb" /><?php p($l->t("collective_narrations_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="observation_cb" /><?php p($l->t("observations_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="participating_observation_cb" /><?php p($l->t("participative_observations_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="ethnography_cb" /><?php p($l->t("ethnography_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="online_interview_cb" /><?php p($l->t("online_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="online_ethnography_cb" /><?php p($l->t("online_ethnography_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="deep_interview_cb" /><?php p($l->t("depth_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="visual_ethnography_cb" /><?php p($l->t("visual_ethnography_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="receptive_interview_cb" /><?php p($l->t("receptive_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="online_group_discussion_cb" /><?php p($l->t("online_group_discussion_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="qualitative_experiment_cb" /><?php p($l->t("qualitative_experiment_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="biographic_method_cb" /><?php p($l->t("biographic_methods_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="capi_method_cb" /><?php p($l->t("capi_method_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="papi_method_cb" /><?php p($l->t("papi_method_cb"))?></div>        
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="cati_method_cb" /><?php p($l->t("cati_method_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="cawi_method_cb" /><?php p($l->t("cawi_method_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="lab_experiment_cb" /><?php p($l->t("lab_experiment_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="field_experiment_cb" /><?php p($l->t("field_experiment_cb"))?></div>
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("applied_technologies_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="+ *" data-checkbox-area-id="applied_technologies_ca" data-checkbox-extensible="true" data-ddi-placeholder="%applied_technologies_ca%" data-ddi-wrapper='{d:ModeOfCollection}{r:URN>urn:ddi:de.unibi:sfb882_%STUDY_UID%_colmode_%CHKSUM%:1{/r:URN}{d:TypeOfModeOfCollection codeListName="UsedTechnologies" codeListAgencyName="sfb882"}%applied_technologies_ca%{/d:TypeOfModeOfCollection}{/d:ModeOfCollection}' >
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="audio_recorder_cb" /><?php p($l->t("audio_recorder_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="video_recorder_cb" /><?php p($l->t("video_recorder_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="tablet_cb" /><?php p($l->t("tablet_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="laptop_computer_cb" /><?php p($l->t("laptop_computer_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="telefon_cb" /><?php p($l->t("telefon_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="printed_questionaires_cb" /><?php p($l->t("printed_questionaires_cb"))?></div>
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("final_version_inquiry_instrument_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="+*" data-file-field-id="final_version_inquiry_instrument_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("field_phase_sampling_type_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="+ *" data-checkbox-area-id="field_phase_sampling_type_ca" data-checkbox-extensible="true" data-ddi-placeholder="%field_phase_sampling_type_ca%" data-ddi-wrapper='{d:SamplingProcedure}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_sampproc_%CHKSUM%:1{/r:URN}{d:TypeOfSamplingProcedure codeListName="SelectionMethod" codeListAgencyName="sfb882"}%field_phase_sampling_type_ca%{/d:TypeOfSamplingProcedure}{/d:SamplingProcedure}'>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="a_priori_determination_cb" /><?php p($l->t("a_priori_determination_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="full_inquiry_cb" /><?php p($l->t("full_inquiry_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="theoretic_sampling_cb" /><?php p($l->t("theoretic_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="extreme_cases_sampling_cb" /><?php p($l->t("extreme_cases_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="typical_cases_sampling_cb" /><?php p($l->t("typical_cases_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="maximal_variation_sampling_cb" /><?php p($l->t("maximal_variation_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="intensity_sampling_cb" /><?php p($l->t("intensity_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="critical_cases_sampling_cb" /><?php p($l->t("critical_cases_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="sensitive_cases_sampling_cb" /><?php p($l->t("sensitive_cases_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="convenience_sampling_cb" /><?php p($l->t("convenience_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="primary_selection_cb" /><?php p($l->t("primary_selection_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="secondary_selection_cb" /><?php p($l->t("secondary_selection_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="snowball_sampling_cb" /><?php p($l->t("snowball_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="register_sample_cb" /><?php p($l->t("register_sample_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="ADM_design_cb" /><?php p($l->t("ADM_design_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="random_route_cb" /><?php p($l->t("random_route_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="cluster_sampling_cb" /><?php p($l->t("cluster_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="random_last_digits_cb" /><?php p($l->t("random_last_digits_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="accidental_sampling_cb" /><?php p($l->t("accidental_sampling_cb"))?></div>
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("samplesize_planned_input"))?></span>
      </div>
      <div class="content_input">
        <input data-text-input-id="samplesize_planned_input" data-mandatory="*+" title="Zahl" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("samplesize_real_input"))?></span>
      </div>
      <div class="content_input">
        <input data-text-input-id="samplesize_real_input" data-mandatory="*+" title="Zahl" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("response_rate_increasing_measures_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="response_rate_increasing_measures_input" title="Beschreibung" data-ddi-placeholder="%response_rate_increasing_measures_input%"/>
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("original_sample_design_deviation_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="original_sample_design_deviation_input" title="Beschreibung" data-ddi-placeholder="%original_sample_design_deviation_input%"/>
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("number_additional_inquiry_cases"))?></span>
      </div>
      <div class="content_input">
        <input data-text-input-id="number_additional_inquiry_cases" title="Falls verändert" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("reasons_additional_inquiry_cases"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="reasons_additional_inquiry_cases" title="Falls verändert" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("field_contacts_location_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="field_contacts_location_input" data-mandatory="*" title="Beschreibung" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("interview_protocols_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="*" data-file-field-id="interview_protocols_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("time_measure_data_exits_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="*+" data-checkbox-area-id="time_measure_data_exits_ca">
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="yes_cb" /><?php p($l->t("yes_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="no_cb" /><?php p($l->t("no_cb"))?></div>
      </div>      

      <div class="content_title">
        <span class="title_string"><?php p($l->t("other_metadata_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="other_metadata_input" title="Art der Daten" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("additional_inquiry_material_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-file-field-id="additional_inquiry_material_file_input" title="Dateiablage">
      </div>      

      <div class="content_title">
        <span class="title_string"><?php p($l->t("inquiry_homepage_input"))?></span>
      </div>
      <div class="content_input">
        <input data-text-input-id="inquiry_homepage_input" title="URL zur Homepage mit Hinweisen zur Erhebung (z.B. Datenschutz)" data-ddi-placeholder="%inquiry_homepage_input%"/>
      </div>
      
    </div>
  </div>
</div>
