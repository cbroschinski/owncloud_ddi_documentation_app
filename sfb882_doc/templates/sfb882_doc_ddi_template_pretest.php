<d:DataCollection>
	<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_datacolpretest_<?php p($_['%CONTAINER_UID%'])?>:1</r:URN>
	<r:Note>
		<r:Relationship>
			<r:RelatedToReference>
				<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_datacolpretest_<?php p($_['%CONTAINER_UID%'])?>:1</r:URN>
				<r:TypeOfObject>DataCollection</r:TypeOfObject>
			</r:RelatedToReference>
		</r:Relationship>
		<r:NoteContent>
			<r:Content>DataCollection represents a pretest</r:Content>
		</r:NoteContent>
	</r:Note>
	<r:Coverage>
		<r:SpatialCoverage>
			<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_<?php p($_['%CONTAINER_UID%'])?>_pretestspatcov:1</r:URN>
			<?php p($_['%geographic_area_input%'])?>
		</r:SpatialCoverage>
		<r:TemporalCoverage>
			<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_<?php p($_['%CONTAINER_UID%'])?>_pretesttempcov:1</r:URN>
			<r:ReferenceDate>
				<r:StartDate><?php p($_['%inquiry_time_span_begin_input%'])?></r:StartDate>
				<r:EndDate><?php p($_['%inquiry_time_span_end_input%'])?></r:EndDate>
			</r:ReferenceDate>
		</r:TemporalCoverage>
	</r:Coverage>
	<d:Methodology>
		<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_<?php p($_['%CONTAINER_UID%'])?>_methodology:1</r:URN>
		<?php p($_['%applied_research_methods_ca%'])?>
		<?php p($_['%pretest_sampling_type_ca%'])?>
	</d:Methodology>
	<d:CollectionEvent>
		<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_<?php p($_['%CONTAINER_UID%'])?>_pretest_collevent:1</r:URN>
		<?php p($_['%pretest_conducting_organization_link%'])?>
		<?php p($_['%technologies_used_ca%'])?>
		<d:ActionToMinimizeLosses>
			<r:URN>urn:ddi:de.unibi:sfb882_<?php p($_['%STUDY_UID%'])?>_<?php p($_['%CONTAINER_UID%'])?>_pretest_acttomin:1</r:URN>
			<r:Description>
				<r:Content>
					<?php p($_['%pretest_response_rate_increasing_measures_input%'])?>
				</r:Content>
			</r:Description>
		</d:ActionToMinimizeLosses>
	</d:CollectionEvent>
</d:DataCollection>
		
