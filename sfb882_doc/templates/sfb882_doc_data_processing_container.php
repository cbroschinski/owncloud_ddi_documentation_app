<div id="container_<?php p('uid'.$_['uid'])?>" class="data_processing container closed upper_four upper_four_orig" style="display:none">
  <div class="container_sibling"></div>
  <div class="container_outer_div">
    <div class="container_title small_title">
      <?php p($l->t("data_processing"))?>
      <div class="close_button title_button" style="display: none"></div>
      <div class="history_button title_button" style="display: none"></div>
    </div>
    <div class="progressbar_container">
      <div class="progressbar">
        <div class="progress_label">
        </div>
      </div>
    </div>
    <div class="container_content" style="display:none">

      <div class="content_title">
        <span class="title_string"><?php p($l->t("secondary_data_sources_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="secondary_data_sources_input" title="Beschreibung" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("generated_variables_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="+" data-file-field-id="generated_variables_file_input" title="Datei Codebuch">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("anomymization_measures_text_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="anomymization_measures_text_input" data-mandatory="+*" title="Beschreibung der Anonymisierungsmaßnahmen" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("anomymization_measures_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="+*" data-file-field-id="anomymization_measures_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("data_sanitization_text_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="data_sanitization_text_input" data-mandatory="+" title="bspw. Löschen unplausibler Fälle" data-ddi-placeholder="%data_sanitization_text_input%" data-ddi-wrapper="{d:ProcessingEvent}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_procevent_cleaning:1{/r:URN}{d:CleaningOperation}{r:Description}{r:Content}%data_sanitization_text_input%{/r:Content}{/r:Description}{/d:CleaningOperation}{/d:ProcessingEvent}" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("data_sanitization_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="+" data-file-field-id="data_sanitization_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("missing_values_codes_text_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="missing_values_codes_text_input" data-mandatory="+" title="z.B. 99=k.A." />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("missing_values_codes_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="+" data-file-field-id="missing_values_codes_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("weighting_factors_creation_text_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="weighting_factors_creation_text_input" data-mandatory="+" title="Beschreibung" data-ddi-placeholder="%weighting_factors_creation_text_input%" data-ddi-wrapper="{d:ProcessingEvent}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_procevent_weighting:1{/r:URN}{d:Weighting}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_weighting:1{/r:URN}{r:Description}{r:Content}%weighting_factors_creation_text_input%{/r:Content}{/r:Description}{/d:Weighting}{/d:ProcessingEvent}"/>
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("weighting_factors_creation_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="+" data-file-field-id="weighting_factors_creation_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("transscription_convention_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="*" data-checkbox-area-id="transscription_convention_ca">
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="hiat_cb" /><?php p($l->t("hiat_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="dida_cb" /><?php p($l->t("dida_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="dt_cb" /><?php p($l->t("dt_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="gat_cb" /><?php p($l->t("gat_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="gat_two_cb" /><?php p($l->t("gat_two_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="chat_cb" /><?php p($l->t("chat_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="own_convention_file_upload_cb" /><?php p($l->t("own_convention_file_upload_cb"))?></div>
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("own_transscription_convention_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-file-field-id="own_transscription_convention_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("used_transcription_software_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="*" data-checkbox-area-id="used_transcription_software_ca" data-checkbox-extensible="true">
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="clan_cb" /><?php p($l->t("clan_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="hiat_dos_cb" /><?php p($l->t("hiat_dos_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="exmaralda_cb" /><?php p($l->t("exmaralda_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="folker_cb" /><?php p($l->t("folker_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="transcriber_cb" /><?php p($l->t("transcriber_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="elan_cb" /><?php p($l->t("elan_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="f4_cb" /><?php p($l->t("f4_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="atlas_ti_cb" /><?php p($l->t("atlas_ti_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="caqdas_cb" /><?php p($l->t("caqdas_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="maxqda_cb" /><?php p($l->t("maxqda_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="nvivo_cb" /><?php p($l->t("nvivo_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="final_cut_cb" /><?php p($l->t("final_cut_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="f5_cb" /><?php p($l->t("f5_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="express_scribe_cb" /><?php p($l->t("express_scribe_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="transcribe_cb" /><?php p($l->t("transcribe_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="feldpartitur_cb" /><?php p($l->t("feldpartitur_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="transcriva_cb" /><?php p($l->t("transcriva_cb"))?></div>
      </div>

    </div>
  </div>
</div>
