<div id="container_<?php p('uid'.$_['uid'])?>" class="pretests container closed upper_two_first upper_two_first_orig" style="display:none" data-research_types_only="quantitative_with_pretest qualitative_with_pretest mixed_with_pretest" >
  <div class="container_sibling"></div>
  <div class="container_outer_div">
    <div class="container_title small_title">
      <?php p('Pretests')?>
      <div class="close_button title_button" style="display: none"></div>
      <div class="history_button title_button" style="display: none"></div>
    </div>
    <div class="progressbar_container">
      <div class="progressbar">
        <div class="progress_label">
        </div>
      </div>
    </div>
    <div class="container_sidebar" style="display: none">
      <?php p($l->t("pretests_sidebar_text"))?>
    </div>
    <div class="container_pane central_pane" data-subcontainer-class="pretest" style="display:none">
    <div id="create_area" class="icon_area">
      <div class="subcontainer_create_icon_placeholder"></div>
      <div class="container subcontainer_create_icon">
        <div class="container_sibling"></div>
        <div class="container_outer_div"></div>
      </div>
	</div>
	<div class="trash_area icon_area" />
	<div class="duplicate_area icon_area" />
  </div>
</div>
