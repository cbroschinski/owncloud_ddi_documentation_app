<div id="container_<?php p('uid'.$_['uid'])?>" class="evaluation container closed lower_five lower_five_orig" style="display:none">
  <div class="container_sibling">
  </div>
  <div class="container_outer_div">
    <div class="container_title small_title">
      <?php p($l->t("evaluation"))?>
      <div class="close_button title_button" style="display: none"></div>
      <div class="history_button title_button" style="display: none"></div>
    </div>
    <div class="progressbar_container">
      <div class="progressbar">
        <div class="progress_label">
        </div>
      </div>
    </div>
    <div class="container_content" style="display:none">

      <div class="content_title">
        <span class="title_string"><?php p($l->t("evaluation_methods_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="evaluation_methods_input" data-mandatory="+*" title="Beschreibung" />
      </div>
      
      <div class="content_title">
        <span class="title_string"><?php p($l->t("codes_text_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="codes_text_input" data-mandatory="*" title="Beschreibung" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("codes_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="*" data-file-field-id="codes_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("code_choice_memos_text_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="code_choice_memos_text_input" data-mandatory="*" title="" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("code_choice_memos_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="*" data-file-field-id="code_choice_memos_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("anonymized_transscripts_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="*" data-file-field-id="anonymized_transscripts_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("anonymized_monitoring_protocols_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-file-field-id="anonymized_monitoring_protocols_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("evaluation_memos_text_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="evaluation_memos_text_input" data-mandatory="*" title="Beschreibung" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("evaluation_memos_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="*" data-file-field-id="evaluation_memos_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("central_results_text_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="central_results_text_input" data-mandatory="*+" title="Beschreibung" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("central_results_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="*+" data-file-field-id="central_results_file_input" title="Dateiablage">
      </div>

    </div>
  </div>
</div>
