<div id="container_<?php p('uid'.$_['uid'])?>" class="cont_7 container closed after_field_phase middle_three middle_three_orig" style="display:none">
  <div class="container_sibling"></div>
  <div class="container_outer_div">
    <div class="container_title small_title">
      <?php p($l->t('after_field_phase'))?>
      <div class="close_button title_button" style="display: none"></div>
      <div class="history_button title_button" style="display: none"></div>
    </div>
    <div class="progressbar_container">
      <div class="progressbar">
        <div class="progress_label">
        </div>
      </div>
    </div>
    <div class="container_content" style="display:none">

      <div class="content_title">
        <span class="title_string"><?php p($l->t("documentation_field_contacts_text_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="documentation_field_contacts_text_input" data-mandatory="*" title="Beschreibung" data-ddi-placeholder="%documentation_field_contacts_text_input%" data-ddi-wrapper='{r:Note}{r:Relationship}{r:RelatedToReference}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_datacolfieldphase:1{/r:URN}{r:TypeOfObject}DataCollection{/r:TypeOfObject}{/r:RelatedToReference}{/r:Relationship}{r:Header}{r:String}Dokumentation Feldkontakte{/r:String}{/r:Header}{r:NoteContent}{r:Content}%documentation_field_contacts_text_input%{/r:Content}{/r:NoteContent}{/r:Note}'/>
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("documentation_field_contacts_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="*" data-file-field-id="documentation_field_contacts_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("reports_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="*+" data-file-field-id="reports_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("results_report_interviewed_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-file-field-id="results_report_interviewed_file_input" title="Ergebnisbericht, der an Befragte versendet wurde">
      </div>

    </div>
  </div>
</div>
