<div id="container_<?php p('uid_'.$_['uid'])?>" class="container closed pretest in_central_pane" style="display:none" data-research_types_only="quantitative_with_pretest qualitative_with_pretest mixed_with_pretest">
  <div class="container_sibling">
  </div>
  <div class="container_outer_div">
    <div class="container_title small_title">
			<?php p($_['container_name'])?>
      <div class="close_button title_button" style="display: none"></div>
      <div class="history_button title_button" style="display: none"></div>
      <div class="options_button title_button" style="display:none"></div>
    </div>
    <div class="progressbar_container">
      <div class="progressbar">
        <div class="progress_label">
        </div>
      </div>
    </div>
    <div class="container_content" style="display:none">
		
      <div class="content_title">
        <span class="title_string"><?php p($l->t("pretest_conducting_organization_input"))?></span>
      </div>
      <div class="content_input">
        <input data-text-input-id="pretest_conducting_organization_input" data-mandatory="+ *" title="Name (beispielsweise Institut)" data-ddi-placeholder='%pretest_conducting_organization_input% %pretest_conducting_organization_link%' data-ddi-wrapper='{a:Organization}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_org_%CHKSUM%:1{/r:URN}{a:OrganizationIdentification}{a:OrganizationName}{r:String}%pretest_conducting_organization_input%{/r:String}{/a:OrganizationName}{/a:OrganizationIdentification}{/a:Organization}%%%{d:DataCollectorOrganizationReference}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_org_%CHKSUM%:1{/r:URN}{r:TypeOfObject}Organization{/r:TypeOfObject}{/d:DataCollectorOrganizationReference}' />
      </div>
      
      <div class="content_title">
        <span class="title_string"><?php p($l->t("pretest_population_group_target_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="+ *" data-checkbox-area-id="pretest_population_group_target_ca" data-checkbox-extensible="true">
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="individuals_cb" /><?php p($l->t("individuals_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="groups_cb" /><?php p($l->t("groups_cb"))?></div> 
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="organisations_cb" /><?php p($l->t("organisations_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="situations_cb" /><?php p($l->t("situations_cb"))?></div>         
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="interactions_cb" /><?php p($l->t("interactions_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="communications_cb" /><?php p($l->t("communications_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="documents_cb" /><?php p($l->t("documents_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="artefacts_cb" /><?php p($l->t("artefacts_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="household_cb" /><?php p($l->t("household_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="family_cb" /><?php p($l->t("family_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="family_households_cb" /><?php p($l->t("family_households_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="geographic_unit_cb" /><?php p($l->t("geographic_unit_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="time_unit_cb" /><?php p($l->t("time_unit_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="text_unit_cb" /><?php p($l->t("text_unit_cb"))?></div>
      </div>
      <div class="content_title">
        <span class="title_string"><?php p($l->t("geographic_area_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="geographic_area_input" data-mandatory="+ *" title="Adresse/Geografische Region" data-ddi-placeholder="%geographic_area_input%" data-ddi-wrapper="{r:Description}{r:Content}%geographic_area_input%{/r:Content}{/r:Description}"/>
      </div>
      <div class="content_title">
        <span class="title_string"><?php p($l->t("inquiry_time_span_begin_input"))?></span>
      </div>
      <div class="content_input date">
        <input data-text-input-id="inquiry_time_span_begin_input" data-mandatory="+ *" title="Datum" data-ddi-placeholder="%inquiry_time_span_begin_input%"/>
      </div>
      <div class="content_title">
        <span class="title_string"><?php p($l->t("inquiry_time_span_end_input"))?></span>
      </div>
      <div class="content_input date">
        <input data-text-input-id="inquiry_time_span_end_input" data-mandatory="+ *" title="Datum" data-ddi-placeholder="%inquiry_time_span_end_input%"/>
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("samplesize_planned_input"))?></span>
      </div>
      <div class="content_input">
        <input data-text-input-id="samplesize_planned_input" data-mandatory="+ *" title="Zahl" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("samplesize_real_input"))?></span>
      </div>
      <div class="content_input">
        <input data-text-input-id="samplesize_real_input" data-mandatory="+ *" title="Zahl" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("pretest_sampling_type_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="+ *" data-checkbox-area-id="pretest_sampling_type_ca" data-checkbox-extensible="true" data-ddi-placeholder="%pretest_sampling_type_ca%" data-ddi-wrapper='{d:SamplingProcedure}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_%CONTAINER_UID%_pretestsampproc_%CHKSUM%:1{/r:URN}{d:TypeOfSamplingProcedure codeListName="SelectionMethod" codeListAgencyName="sfb882"}%pretest_sampling_type_ca%{/d:TypeOfSamplingProcedure}{/d:SamplingProcedure}'>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="theory_guided_selection_cb" /><?php p($l->t("a_priori_determination_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="full_inquiry_cb" /><?php p($l->t("full_inquiry_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="theoretic_sampling_cb" /><?php p($l->t("theoretic_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="extreme_cases_sampling_cb" /><?php p($l->t("extreme_cases_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="typical_cases_sampling_cb" /><?php p($l->t("typical_cases_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="maximal_variation_sampling_cb" /><?php p($l->t("maximal_variation_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="intensity_sampling_cb" /><?php p($l->t("intensity_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="critical_cases_sampling_cb" /><?php p($l->t("critical_cases_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="sensitive_cases_sampling_cb" /><?php p($l->t("sensitive_cases_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="convenience_sampling_cb" /><?php p($l->t("convenience_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="primary_selection_cb" /><?php p($l->t("primary_selection_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="secondary_selection_cb" /><?php p($l->t("secondary_selection_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="snowball_sampling_cb" /><?php p($l->t("snowball_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="register_sample_cb" /><?php p($l->t("register_sample_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="ADM_design_cb" /><?php p($l->t("ADM_design_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="random_route_cb" /><?php p($l->t("random_route_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="cluster_sampling_cb" /><?php p($l->t("cluster_sampling_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="random_last_digits_cb" /><?php p($l->t("random_last_digits_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="accidental_sampling_cb" /><?php p($l->t("accidental_sampling_cb"))?></div>
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("preparing_activities_text_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="preparing_activities_text_input" title="z.B. Mitarbeiterschulung, Verhaltensregeln" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("preparing_activities_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-file-field-id="preparing_activities_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("response_rate_increasing_measures_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="response_rate_increasing_measures_input" title="Beschreibung" data-ddi-placeholder="%pretest_response_rate_increasing_measures_input%" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("technologies_used_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="+ *" data-checkbox-area-id="technologies_used_ca" data-checkbox-extensible="true" data-ddi-placeholder="%technologies_used_ca%" data-ddi-wrapper='{d:ModeOfCollection}{r:URN>urn:ddi:de.unibi:sfb882_%STUDY_UID%_%CONTAINER_UID%_pretestcolmode_%CHKSUM%:1{/r:URN}{d:TypeOfModeOfCollection codeListName="UsedTechnologies" codeListAgencyName="sfb882"}%technologies_used_ca%{/d:TypeOfModeOfCollection}{/d:ModeOfCollection}'>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="audio_recorder_cb" /><?php p($l->t("audio_recorder_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="video_recorder_cb" /><?php p($l->t("video_recorder_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="tablet_cb" /><?php p($l->t("tablet_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="laptop_computer_cb" /><?php p($l->t("laptop_computer_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="telefon_cb" /><?php p($l->t("telefon_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="printed_questionaires_cb" /><?php p($l->t("printed_questionaires_cb"))?></div>
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("time_measure_data_exits_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="+ *" data-checkbox-area-id="time_measure_data_exits_ca">
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="yes_cb" /><?php p($l->t("yes_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="no_cb" /><?php p($l->t("no_cb"))?></div>
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("other_metadata_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="other_metadata_input" title="Art der Daten" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("pretest_evaluation_method_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="pretest_evaluation_method_input" data-mandatory="+ *" title="Beispielsweise 'Think Aloud'" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("pretest_final_version_inquiry_instrument_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="+ *" data-file-field-id="pretest_final_version_inquiry_instrument_input" title="Dateiablage">
      </div>

    </div>
  </div>
</div>
