<div id="container_<?php p('uid_'.$_['uid'])?>" class="container closed syntax_file in_central_pane" style="display:none">
  <div class="container_sibling">
  </div>
  <div class="container_outer_div">
    <div class="container_title small_title">
      <?php p($l->t("syntax_file"))?><?php p($_['container_name'])?>
      <div class="close_button title_button" style="display: none"></div>
      <div class="history_button title_button" style="display: none"></div>
      <div class="options_button title_button" style="display:none"></div>
    </div>
    <div class="progressbar_container">
      <div class="progressbar">
        <div class="progress_label">
        </div>
      </div>
    </div>
    <div class="container_content" style="display:none">

      <div class="content_title">
        <span class="title_string"><?php p($l->t("documented_syntax_file_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="+" data-file-field-id="documented_syntax_file_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("authors_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area pevz_search" data-mandatory="+" data-checkbox-area-id="authors_ca" title="Name/n" data-checkbox-extensible="true">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("last_change_date_input"))?></span>
      </div>
      <div class="content_input date">
        <input data-text-input-id="last_change_date_input" data-mandatory="+" placeholder="Kalenderauswahl..." title="Datum" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("task_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="task_input" data-mandatory="+" title="Beispielsweise Analyse, Bereinigung..." />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("used_software_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="+" data-checkbox-area-id="used_software_ca" data-checkbox-extensible="true">
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="matlab_cb" /><?php p($l->t("matlab_cb"))?></div>   
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="r_commander_cb" /><?php p($l->t("r_commander_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="r_cb" /><?php p($l->t("r_cb"))?></div>   
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="sas_cb" /><?php p($l->t("sas_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="mplus_cb" /><?php p($l->t("mplus_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="spss_cb" /><?php p($l->t("spss_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="stata_cb" /><?php p($l->t("stata_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="statistica_cb" /><?php p($l->t("statistica_cb"))?></div>
      </div>

    </div>
  </div>
</div>
