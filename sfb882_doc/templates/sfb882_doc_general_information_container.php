<div id="container_<?php p('uid_'.$_['uid'])?>" class="container closed general_information in_sidebar in_sidebar_orig" style="display:none">
  <div class="container_sibling">
  </div>
  <div class="container_outer_div">
    <div class="container_title small_title">
      <?php p($l->t('general_information'))?>
      <div class="close_button title_button" style="display: none"></div>
      <div class="history_button title_button" style="display: none"></div>
    </div>
    <div class="progressbar_container">
      <div class="progressbar">
        <div class="progress_label">
        </div>
      </div>
    </div>
    <div class="container_content" style="display:none">

      <div class="content_title">
        <span class="title_string"><?php p($l->t('study_full_name_input'))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="study_full_name_input" data-mandatory="+ *" title="Der volle, offizielle Name der Studie." data-ddi-placeholder="%study_full_name_input%" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t('study_short_name_input'))?></span>
      </div>
      <div class="content_input">
        <input data-text-input-id="study_short_name_input" data-mandatory="+ *" title="Kurztitel." data-ddi-placeholder="%study_short_name_input%" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t('contact_address_input'))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="contact_address_input" data-mandatory="+ *" title="Adresse." />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("project_url_input"))?></span>
      </div>
      <div class="content_input">
        <input data-text-input-id="project_url_input" data-mandatory="+ *" title="Link zur Homepage des Teilprojekts" data-ddi-placeholder="%project_url_input%" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("project_head_input"))?></span>
      </div>
      <div class="content_input_checkbox_area pevz_search" data-mandatory="+ *" data-checkbox-area-id="project_head_input" title="Name der Projektleiter/Projektleiterinnen." data-checkbox-extensible="true" data-ddi-placeholder="%project_head_input%" data-ddi-wrapper="{r:Contributor}{r:ContributorName}{r:String}%project_head_input%{/r:String}{/r:ContributorName}{r:ContributorRole}principal_investigator{/r:ContributorRole}{/r:Contributor}" >
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("project_members_input"))?></span>
      </div>
      <div class="content_input_checkbox_area pevz_search" data-mandatory="+ *" data-checkbox-area-id="project_members_input" data-checkbox-extensible="true" data-ddi-placeholder="%project_members_input%" data-ddi-wrapper="{r:Contributor}{r:ContributorName}{r:String}%project_members_input%{/r:String}{/r:ContributorName}{/r:Contributor}" >
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("time_span_begin_input"))?></span>
      </div>
      <div class="content_input date">
        <input data-text-input-id="time_span_begin_input" data-mandatory="+ *" title="Beginn der Projektlaufzeit" placeholder="Kalenderauswahl..." data-ddi-placeholder="%time_span_begin_input%" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("time_span_end_input"))?></span>
      </div>
      <div class="content_input date">
        <input data-text-input-id="time_span_end_input" data-mandatory="+ *" title="Ende der Projektlaufzeit" placeholder="Kalenderauswahl..." data-ddi-placeholder="%time_span_end_input%" />
      </div>

       <div class="content_title">
        <span class="title_string"><?php p($l->t("funding_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="funding_input" title="Finanzierung zusätzlich zu SFB-Mitteln" data-ddi-placeholder="%funding_input%" />
      </div>
    
      <div class="content_title">
        <span class="title_string"><?php p($l->t("project_short_description_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="project_short_description_input" data-mandatory="+ *" title="Beschreibung des Projekts" data-ddi-placeholder="%project_short_description_input%" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("keywords_input"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="+ *" data-checkbox-area-id="keywords_input" data-checkbox-extensible="true" data-ddi-placeholder="%keywords_input%" data-ddi-wrapper="{r:Keyword}%keywords_input%{/r:Keyword}" >
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("project_research_area_input"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="+ *" title="Wissenschaftliche Disziplin" data-checkbox-area-id="project_research_area_input" data-checkbox-extensible="true" data-ddi-placeholder="%project_research_area_input%" data-ddi-wrapper='{r:Subject codeListName="ResearchTopics" codeListAgencyName="sfb882"}%project_research_area_input%{/r:Subject}'>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="sociology_cb" /><?php p($l->t("sociology_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="social_science_cb" /><?php p($l->t("social_science_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="economics_cb" /><?php p($l->t("economics_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="psychology_cb" /><?php p($l->t("psychology_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="law_cb" /><?php p($l->t("law_cb"))?></div>            
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("study_design_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="+ *" title="Angabe über die zeitliche Dimension des Teilprojektes" data-checkbox-area-id="study_design_ca" data-ddi-placeholder="%study_design_ca%" data-ddi-wrapper='{r:Subject codeListName="StudyDesign" codeListAgencyName="sfb882"}%study_design_ca%{/r:Subject}'>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="longitudinal_cb" /><?php p($l->t("longitudinal_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="cross_section_cb" /><?php p($l->t("cross_section_cb"))?></div>    
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("primary_data_sources_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="primary_data_sources_input" data-mandatory="+ *" title="beispielsweise eigene Erhebung, SOEP etc." data-ddi-placeholder="%primary_data_sources_input%" data-ddi-wrapper="{r:OtherMaterial}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_primdat:1{/r:URN}{r:TypeOfMaterial}PrimaryData{/r:TypeOfMaterial}{r:Citation}{r:Title}{r:String}%primary_data_sources_input%{/r:String}{/r:Title}{/r:Citation}{/r:OtherMaterial}" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("project_publications_input"))?></span>
      </div>
      <div class="content_input_checkbox_area pub_search title_line_break" data-mandatory="+ *" data-checkbox-area-id="project_publications_input" data-checkbox-extensible="true" data-ddi-placeholder="%project_publications_input%" data-ddi-wrapper="{r:OtherMaterial}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_pub_%CHKSUM%:1{/r:URN}{r:TypeOfMaterial}Publication{/r:TypeOfMaterial}{r:Citation}{r:Title}{r:String}%project_publications_input%{/r:String}{/r:Title}{/r:Citation}{/r:OtherMaterial}" >
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("project_data_publications_input"))?></span>
      </div>
      <div class="content_input_checkbox_area data_search title_line_break" data-mandatory="+ *" data-checkbox-area-id="project_data_publications_input" data-checkbox-extensible="true" data-ddi-placeholder="%project_data_publications_input%" data-ddi-wrapper="{r:OtherMaterial}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_datapub_%CHKSUM%:1{/r:URN}{r:TypeOfMaterial}DataPublication{/r:TypeOfMaterial}{r:Citation}{r:Title}{r:String}%project_data_publications_input%{/r:String}{/r:Title}{/r:Citation}{/r:OtherMaterial}">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("data_mangagement_plans_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="+ *" data-file-field-id="data_mangagement_plans_input" title="Datenmanagementplan ablegen">
      </div>

    </div>
  </div>
</div>
