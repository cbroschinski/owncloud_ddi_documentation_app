<div id="container_<?php p('uid_'.$_['uid'])?>" class="container" data-autocreate-children="general_information">
	<div class="container_sibling"></div>
	<div class="container_outer_div">
		<div class="container_title small_title" style="display:none">
			<?php p($_['container_name'])?>
			<div class="history_button title_button" style="display: none"></div>
			<div class="close_button title_button" style="display: none"></div>
			<div class="options_button title_button" style="display:none"></div>
		</div>
		<div class="progressbar_container">
			<div class="progressbar">
				<div class="progress_label">
				</div>
			</div>
		</div>
		<div class="container_sidebar" style="display: none">
			<?php p($l->t("top_level_sidebar_text"))?>
		</div>
		<div class="container_pane left_pane" style="display:none">   
		</div>
		<div class="container_pane central_pane" data-subcontainer-class="study" style="display:none">
			<div id="create_area" class="icon_area">
				<div class="subcontainer_create_icon_placeholder"></div>
				<div class="container subcontainer_create_icon create_study" data-additional-options="research_type">
					<div class="container_sibling"></div>
					<div class="container_outer_div"></div>
				</div>
		</div>
		<div class="trash_area icon_area" />
		<div class="duplicate_area icon_area" />
		<div class="export_area icon_area" />
		</div>
	</div>
</div>
