<div id="container_<?php p('uid'.$_['uid'])?>" class="syntax_files container closed upper_five upper_five_orig" style="display:none" data-research_types_only="mixed_with_pretest mixed_without_pretest quantitative_with_pretest quantitative_without_pretest" >
  <div class="container_sibling"></div>
  <div class="container_outer_div">
    <div class="container_title small_title">
      <?php p('Syntax-Dateien')?>
      <div class="close_button title_button" style="display: none"></div>
      <div class="history_button title_button" style="display: none"></div>
    </div>
    <div class="progressbar_container">
      <div class="progressbar">
        <div class="progress_label">
        </div>
      </div>
    </div>
    <div class="container_sidebar" style="display: none">
      <?php p($l->t("syntax_files_sidebar_text"))?>
    </div>
    <div class="container_pane central_pane" data-subcontainer-class="syntax_file" style="display:none">
    <div id="create_area" class="icon_area">
      <div class="subcontainer_create_icon_placeholder"></div>
      <div class="container subcontainer_create_icon">
        <div class="container_sibling"></div>
        <div class="container_outer_div"></div>
      </div>
	</div>
	<div class="trash_area icon_area" />
	<div class="duplicate_area icon_area" />
  </div>
</div>
