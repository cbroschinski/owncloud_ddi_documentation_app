<div id="container_<?php p('uid_'.$_['uid'])?>" class="container closed study in_central_pane" data-autocreate-children="pre_field_work pretests field_phase after_field_phase data_processing syntax_files evaluation" style="display: none">
  <div class="container_sibling">
  </div>
  <div class="container_outer_div">
    <div class="container_title small_title" style="display: none">
      <?php p($_['container_name'])?>
      <div class="mand_plus mand_title" style="display: inline-block"></div>
      <div class="mand_mult mand_title" style="display: inline-block"></div>
      <div class="close_button title_button" style="display: none"></div>
      <div class="history_button title_button" style="display: none"></div>
      <div class="options_button title_button" style="display:none"></div>
    </div>
    <div class="horizontal-separator" style="display: none">
    </div>
    <div class="progressbar_container">
      <div class="progressbar">
        <div class="progress_label">
        </div>
      </div>
    </div>
  </div>
</div>
