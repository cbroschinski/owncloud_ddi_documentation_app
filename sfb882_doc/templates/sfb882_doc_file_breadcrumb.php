<!-- This template is based on the files app part.breadcrumb.php template  -->

<?php if(count($_["breadcrumb"])):?>
	<div class="crumb" data-dir=''>
		<div>
			<img src="<?php print_unescaped(OCP\image_path('core', 'places/home.svg'));?>" class="svg" />
		</div>
	</div>
<?php endif;?>
<?php for($i=0; $i<count($_["breadcrumb"]); $i++):
	$crumb = $_["breadcrumb"][$i];
	$dir = str_replace('+', '%20', urlencode($crumb["dir"]));
	$dir = str_replace('%2F', '/', $dir); ?>
	<div class="crumb <?php if($i == count($_["breadcrumb"])-1) p('last');?> svg"
		 data-dir='<?php p($dir);?>'>
	    <?php p($crumb["name"]); ?>
	</div>
<?php endfor;
