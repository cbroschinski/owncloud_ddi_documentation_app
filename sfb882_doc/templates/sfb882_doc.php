<div id="sfb882_doc_content">
	<div class="icon_bar">
		<div id="create_area" class="icon_area">
			<div class="container create_icon icon_bar_pos_1"></div>
			<div id="icon_bar_pos_1_placeholder" class="icon_bar_pos_1" style="display:none"></div>   
		</div>
		<div class="trash_area icon_area">
		</div>
    <div class="export_area icon_area">
		</div>
	</div>
  <!--Dialogs-->
	<div id="container_options_dialog" title="<?php p($l->t("options_dialog_title")) ?>" style="display:none">
		<input id="container_name_input" class="input_field" title="<?php p($l->t("options_new_name")) ?>" />
	  <div id="example_container">
		  <div id="example_container_title" class="container_title very_small_title">
        <?php p($l->t("preview"))?>
        <div class="mand_plus mand_title" style="display: inline-block"></div>
        <div class="mand_mult mand_title" style="display: inline-block"></div>
      </div>
	  </div>
	  <div id="color_slider_red" class="color_slider"></div>
	  <div id="color_slider_green" class="color_slider"></div>
	  <div id="color_slider_blue" class="color_slider"></div>
    <div id="research_type_selection" class="option_dialog_selection" style="display:none">
      <form>
        <p><?php p($l->t("research_approach"))?></p>
        <input id="quantiative_research_rb" type="radio" name="research_type_button_group" value="quantitative" title="<?php p($l->t("quantitative_research")) ?>" /><?php p($l->t("quantitative_research")) ?><br />
        <input id="qualitative_research_rb" type="radio" name="research_type_button_group" value="qualitative" title="<?php p($l->t("qualitative_research")) ?>" /><?php p($l->t("qualitative_research")) ?><br />
        <input id="mixed_research_rb" type="radio" name="research_type_button_group" value="mixed" title="<?php p($l->t("mixed_methods")) ?>" checked="checked" /><?php p($l->t("mixed_methods")) ?>
      </form>
    </div>
    <div id="pretest_conducted_selection" class="option_dialog_selection" style="display:none">
      <form>
        <p><?php p($l->t("pretest_conducted"))?></p>
        <input id="pretest_yes_rb" type="radio" name="pretest_conducted_button_group" value="with_pretest" title="Ja" /><?php p($l->t("pretest_yes")) ?><br />
        <input id="pretest_no_rb" type="radio" name="pretest_conducted_button_group" value="without_pretest" title="Nein" checked="checked" /><?php p($l->t("pretest_no")) ?><br />
      </form>
    </div>
  </div>
  <div id="export_dialog" title="Export" style="display:none">
    <p><?php p($l->t("export_info"))?></p>
    <form>
      <p><?php p($l->t("export_textfiles"))?></p>
      <input id="plain_textfile_rb" type="radio" name="export_type_button_group" value="plaintext" title="<?php p($l->t("export_simple_text")) ?>" /><?php p($l->t("export_simple_text")) ?>
      <p><?php p($l->t("export_xml_files"))?></p>    
      <input id="ddi_3_2_rb" type="radio" name="export_type_button_group" value="ddi_3_2" title="<?php p($l->t("export_ddi")) ?>" /><?php p($l->t("export_ddi")) ?>
      <p><?php p($l->t("presentation_formats"))?></p>
      <input id="pdf_rb" type="radio" name="export_type_button_group" value="pdf" title="<?php p($l->t("export_pdf")) ?>" /><?php p($l->t("export_pdf")) ?><br />
      <input id="ps_rb" type="radio" name="export_type_button_group" value="ps" title="<?php p($l->t("export_ps")) ?>" /><?php p($l->t("export_ps")) ?><br />
      <input id="xhtml_rb" type="radio" name="export_type_button_group" value="xhtml" title="<?php p($l->t("export_xhtml")) ?>" /><?php p($l->t("export_xhtml")) ?><br />
      <input id="epub_rb" type="radio" name="export_type_button_group" value="epub" title="<?php p($l->t("export_epub")) ?>" /><?php p($l->t("export_epub")) ?>
      <p><?php p($l->t("export_additional_options"))?></p>
      <input id="attachfiles_cb" type="checkbox" value="attachfiles" title="<?php p($l->t("attach_files")) ?>" /><?php p($l->t("attach_files"))?><p><?php p($l->t("attach_files_info")) ?></p>  
    </form>
  </div>

  <div id="history_dialog" title="<?php p($l->t("history_dialog_title")) ?>" style="display:none">
    <div id="history_panel"></div>
  </div>
  
  <div id="duplicate_dialog" title="<?php p($l->t("duplicate_dialog_title")) ?>" style="display:none">
		<p><?php p($l->t("duplicate_name"))?></p>
		<input id="duplicate_name_input" class="input_field" title="<?php p($l->t("duplicate_name"))?>" />
	</div>
  <!--/Dialogs-->
  <div id="share" class="minimized drawer" style="display:none">
    <div id="share_icon_area" class="drawer_icon_area">
	    <div id="group_icon" class="drawer_icon">
      </div>
    </div>
    <div id="share_drawer" class="drawer_handle">
    </div>
    <div id="share_text" class="drawer_text" style="display: none">
      <?php p($l->t("share_text"))?>
    </div>
  </div>
  <div id="files" class="minimized drawer" style="display:none">
    <div id="files_icon_area" class="drawer_icon_area">
	    <div id="files_icon" class="drawer_icon">
      </div>
    </div>
    <div id="files_drawer" class="drawer_handle">
    </div>
    <div id="testarea"></div>
  </div>
  <div id="bin" class="minimized drawer" style="display:none">
    <div id="bin_icon_area" class="drawer_icon_area">
	    <div id="bin_icon" class="drawer_icon">
      </div>
    </div>
    <div id="bin_drawer" class="drawer_handle">
    </div>
    <div id="bin_text" class="drawer_text" style="display: none">
      <?php p($l->t("bin_text"))?>
    </div>
  </div>
  <div id="options" class="minimized drawer" style="display:none">
    <div id="options_icon_area" class="drawer_icon_area">
	    <div id="options_icon" class="drawer_icon">
      </div>
    </div>
    <div id="options_drawer" class="drawer_handle">
    </div>
    <div id="options_pane" style="display:none">
			<div id="options_animation_time_pane">
				<p><?php p($l->t("options_animation_time_title"))?></p>
				<div id="animation_time_slider"></div>
			</div>
		</div>
  </div>
</div>


