<div id="container_<?php p('uid_'.$_['uid'])?>" class="container closed pre_field_work middle_one middle_one_orig" style="display:none">
  <div class="container_sibling">
  </div>
  <div class="container_outer_div">
    <div class="container_title small_title">
      <?php p($l->t('pre_field_work'))?>
      <div class="close_button title_button" style="display: none"></div>
      <div class="history_button title_button" style="display: none"></div>
    </div>
    <div class="progressbar_container">
        <div class="progressbar">
            <div class="progress_label">
            </div>
        </div>
    </div>
    <div class="container_content" style="display:none">

      <div class="content_title">
        <span class="title_string"><?php p($l->t('inquiry_contents_input'))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="inquiry_contents_input" data-mandatory="+ *" title="Beschreibung" data-ddi-placeholder="%inquiry_contents_input%" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("research_questions_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="research_questions_input" data-mandatory="+ *" title="Beschreibung" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("related_theories_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="related_theories_input" data-mandatory="+ *" title="Beschreibung" data-ddi-placeholder="%related_theories_input%" data-ddi-wrapper="{r:Note}{r:TypeOfNote}theory{/r:TypeOfNote}{r:Relationship}{r:RelatedToReference}{r:URN}urn:ddi:de.unibi:sfb882_%STUDY_UID%_studyunit:1{/r:URN}{r:TypeOfObject}StudyUnit{/r:TypeOfObject}{/r:RelatedToReference}{/r:Relationship}{r:NoteContent}{r:Content}%related_theories_input%{/r:Content}{/r:NoteContent}{/r:Note}" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("planned_methods_of_inquiry_ca"))?></span>
      </div>
      <div class="content_input_checkbox_area" data-mandatory="+ *" data-checkbox-area-id="planned_methods_of_inquiry_ca" data-checkbox-extensible="true">
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="threaded_interview_cb" /><?php p($l->t("threaded_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="focused_interview_cb" /><?php p($l->t("focused_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="half_standardized_interview_cb" /><?php p($l->t("half_standardized_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="problem_centered_interview_cb" /><?php p($l->t("problem_centered_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="expert_interview_cb" /><?php p($l->t("expert_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="ethnographic_interview_cb" /><?php p($l->t("ethnographic_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="ero_epic_conversation_cb" /><?php p($l->t("ero_epic_conversation_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="narrations_cb" /><?php p($l->t("narrations_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="narrative_interview_cb" /><?php p($l->t("narrative_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="episodic_interview_cb" /><?php p($l->t("episodic_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="group_interviews_cb" /><?php p($l->t("group_interviews_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="group_discussions_cb" /><?php p($l->t("group_discussions_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="focus_groups_cb" /><?php p($l->t("focus_groups_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="online_focus_groups_cb" /><?php p($l->t("online_focus_groups_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="collective_narrations_cb" /><?php p($l->t("collective_narrations_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="observations_cb" /><?php p($l->t("observations_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="participative_observations_cb" /><?php p($l->t("participative_observations_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="ethnography_cb" /><?php p($l->t("ethnography_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="online_interview_cb" /><?php p($l->t("online_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="online_ethnography_cb" /><?php p($l->t("online_ethnography_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="depth_interview_cb" /><?php p($l->t("depth_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="visual_ethnography_cb" /><?php p($l->t("visual_ethnography_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="receptive_interview_cb" /><?php p($l->t("receptive_interview_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="online_group_discussion_cb" /><?php p($l->t("online_group_discussion_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="qualitative_experiment_cb" /><?php p($l->t("qualitative_experiment_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="biographic_methods_cb" /><?php p($l->t("biographic_methods_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="capi_method_cb" /><?php p($l->t("capi_method_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="papi_method_cb" /><?php p($l->t("papi_method_cb"))?></div>        
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="cati_method_cb" /><?php p($l->t("cati_method_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="cawi_method_cb" /><?php p($l->t("cawi_method_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="lab_experiment_cb" /><?php p($l->t("lab_experiment_cb"))?></div>
        <div class="checkbox_unit"><input type="checkbox" data-checkbox-id="field_experiment_cb" /><?php p($l->t("field_experiment_cb"))?></div>
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("sampling_procedures_text_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="sampling_procedures_text_input" data-mandatory="+ *" title="z.B. Samplingbericht" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("sampling_procedures_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="+ *" data-file-field-id="sampling_procedures_file_input" title="Beschreibung">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("sampling_procedure_changes_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="sampling_procedure_changes_input" title="Beschreibung" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("inquiry_method_previous_versions_text_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="inquiry_method_previous_versions_text_input" data-mandatory="+ *" title="Gründe für Veränderungen des Erhebungsinstruments" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("inquiry_method_previous_versions_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="+ *" data-file-field-id="inquiry_method_previous_versions_file_input" title="Dateiablage">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("inquiry_correspondence_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="+ *" data-file-field-id="inquiry_correspondence_input">
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("inquiry_memos_text_input"))?></span>
      </div>
      <div class="content_input">
        <textarea data-text-input-id="inquiry_memos_text_input" data-mandatory="* +" />
      </div>

      <div class="content_title">
        <span class="title_string"><?php p($l->t("declaration_of_consent_file_input"))?></span>
      </div>
      <div class="content_input_file_field" data-mandatory="+ *" data-file-field-id="declaration_of_consent_file_input" title="Muster der verwendeten Einwilligungserklärungen hochladen">
      </div>

    </div>
  </div>
</div>
