<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();
$username = OCP\USER::getUser();

$methods = array("undo", "redo");

if (!isset($_POST['uid'])) {
	OCP\JSON::error(array('message' => 'uid must be specified!'));
	exit();
}

if (!is_numeric($_POST['uid'])) {
    OCP\JSON::error(array('message' => 'uid is no numeric value!'));
    exit();
}

if (!isset($_POST['field_id'])) {
	OCP\JSON::error(array('message' => 'field_id must be specified!'));
	exit();
}

if (!isset($_POST['value'])) {
	OCP\JSON::error(array('message' => 'value must be specified!'));
	exit();
}

if (!isset($_POST['method'])) {
	OCP\JSON::error(array('message' => 'method must be specified!'));
	exit();
}

if (in_array($_POST['method'], $methods)) {
    $method = $_POST['method'];
}
else {
    $method = "undo"; //undo as default
}

$uid = $_POST['uid'];
$field_id = $_POST['field_id'];
$value = $_POST['value'];

try {
    if (!Container::has_access($username, $uid)) {
        OCP\JSON::error(array('message' => 'Access to container (uid '. $uid .') denied for current user'));
        exit();
    }
    if ($method == "undo") {
        $return_value = Undo_Manager::get_undo_value($uid, $field_id, $value);
    }
    else if ($method == "redo") {
        $return_value = Undo_Manager::get_redo_value($uid, $field_id, $value);
    }

    OCP\JSON::success(array('data' => $return_value));
}
catch (Exception $e) {
    OCP\JSON::error(array('message' => "An error occured while accessing the database: ".$e->getMessage()));
}
