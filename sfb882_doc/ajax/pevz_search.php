<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();

$pevz_url = 'http://ekvv.uni-bielefeld.de/pers_publ/Personen_einer_Einrichtung3.jsp?org_id=';

$org_id = $_GET['org_id'];

$query = $_GET['query'];

$raw = file_get_contents($pevz_url.$org_id);

//Clean up. These BIS people are real whitespace character fans...
$raw = str_replace(array("\n", "\r", "\t"), '', $raw);
$raw = html_entity_decode($raw);

$dom = new DOMDocument();
$dom->loadHTML($raw);
$dom->normalizeDocument();

$links = $dom->getElementsByTagName('a');

$names = array();
foreach ($links as $link) {
    $value = $link->nodeValue;
    if (stripos($value, $query) !== false) {
        $names[] = $value;
    }
}

OCP\JSON::success(array('data' => $names));
