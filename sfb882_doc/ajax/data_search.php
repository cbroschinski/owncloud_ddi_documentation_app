<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();

$query = trim($_GET['query']) . '*';
$query = str_replace('  ', ' ', $query);
$query = str_replace(' ', '%20', $query);

$query_part = 'title%20ALL%20%22'.$query.'%22%20OR%20person%20ALL%20%22'.$query.'%22';

$pub_url = 'https://pub.uni-bielefeld.de/data?fmt=jsonintern&style=sfb882&q=' . $query_part;

$json = strip_tags(file_get_contents($pub_url));

$json = html_entity_decode($json);

OCP\JSON::success(array('url' => $pub_url, 'data' => $json));
