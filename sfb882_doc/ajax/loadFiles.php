<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();
$username = OCP\USER::getUser();

if (!isset($_GET['uid'])) {
	OCP\JSON::error(array('message' => 'uid must be specified!'));
	exit();
}

if (!is_numeric($_GET['uid'])) {
    OCP\JSON::error(array('message' => 'uid is no numeric value!'));
    exit();
}

$uid = trim($_GET['uid']);

//Check if user has access to container

if (!Container::has_access($username, $uid)) {
    OCP\JSON::error(array('message' => 'The current user does not have access to the requested unit (uid '. $parent_uid .')'));
    exit();
}

$stmt = OCP\DB::prepare('SELECT chksum, field, file_name, file_alias, file_origin, creation_date, description, public, active FROM `*PREFIX*ddi_files` WHERE `parent_uid` = ?');
$result = $stmt->execute(array($uid));

$data = array();

while ($row = $result->fetchRow()) {
    $file_path = SFB882_FILE_STORAGE_PATH .'/'.$row['file_name'];
    $row['mime_type'] = \OC_Helper::getMimeType($file_path);
    $row['mime_icon_path'] = \OC_Helper::mimetypeIcon($row['mime_type']);
    $row['file_size'] = \OC_Helper::humanFileSize(filesize($file_path));
    $row['creation_date'] = OCP\Util::formatDate($row['creation_date']);
    $data[] = $row;
}

OCP\JSON::success(array('data' => $data));

