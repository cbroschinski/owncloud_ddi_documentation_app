<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();
$username = OCP\USER::getUser();

if (!isset($_GET['uid'])) {
	OCP\JSON::error(array('message' => 'uid must be specified!'));
	exit();
}

if (!is_numeric($_GET['uid'])) {
    OCP\JSON::error(array('message' => 'uid is no numeric value!'));
    exit();
}

$uid = trim($_GET['uid']);

$stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_units` WHERE `uid` = ? AND `username` = ?;');
$result = $stmt->execute(array($uid, $username));

if ($result->numRows() != 1) {
    OCP\JSON::error(array('message' => 'The requested subcontainer (uid '. $uid .') does not exits or it does not belong to the current user'));
    exit();
}

$export_type = $_GET['export_type'];
$export_scope = $_GET['export_scope'];

$attach_files = false;
if (isset($_GET['attach_files']) && $_GET['attach_files'] === 'true') {
    $attach_files = true;
}

$scope_whitelist = array('all', 'populated');
$type_whitelist = array('plaintext', 'epub', 'ps', 'pdf', 'xhtml');

if (!in_array($export_scope, $scope_whitelist)) {
    OCP\JSON::error(array('message' => 'export_scope ' . $export_scope . ' is not supported!'));
    exit();
}


if (in_array($export_type, $type_whitelist)) {
    try {
		$link_to_files = false;
		if ($attach_files && $export_type == 'xhtml') {
			$link_to_files = true;
		}
        $result_file = Container_Export::export_to_plaintext($uid, $username, $export_scope, $link_to_files);
        if ($export_type != 'plaintext') {
            $result_file = Container_Export::convert_plaintext($result_file, $export_type);
        }
        if (!$attach_files) {
            OCP\JSON::success(array('path' => $result_file));
            exit();
        }
        $zip_archive = Container_Export::create_zip_archive($result_file, $uid, $username);
        OCP\JSON::success(array('path' => $zip_archive));
        exit();
    }
    catch (Exception $e) {
        OCP\JSON::error(array('message' => $e->getMessage()));
        exit();
    }  
}

if ($export_type === 'ddi_3_2') {
	try {
		$result_file = Container_Export::export_to_ddi_3_2($uid, $username);
		OCP\JSON::success(array('path' => $result_file));
		exit();
	}
	catch (Exception $e) {
        OCP\JSON::error(array('message' => $e->getMessage()));
        exit();
    }  
}

OCP\JSON::error(array('message' => 'export_type ' . $export_type . ' is not supported!'));
exit();

