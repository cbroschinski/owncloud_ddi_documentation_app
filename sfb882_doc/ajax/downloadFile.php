<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();
$username = OCP\USER::getUser();

if (!isset($_GET['uid']) || !isset($_GET['field_id']) || !isset($_GET['chksum']) ) {
    exit();
} 
if (!is_numeric($_GET['uid'])) {
    exit();
}

$uid = $_GET['uid'];
$field_id = $_GET['field_id'];
$chksum = $_GET['chksum'];
if (!Container::has_access($username, $uid)) {
    exit();
}

$stmt = OCP\DB::prepare('SELECT file_name FROM `*PREFIX*ddi_files` WHERE `parent_uid` = ? AND `field` = ? AND `chksum` = ?');
$result = $stmt->execute(array($uid, $field_id, $chksum));

if ($result->numRows() != 1) {
    exit();
}
$row = $result->fetchRow();
$file_name = $row['file_name'];
$file_path = SFB882_FILE_STORAGE_PATH .'/'.$file_name;

if (!file_exists($file_path)) {
    exit();
}
$ftype=\OC_Helper::getMimeType($file_path);

header('Content-Type:'.$ftype);
if ( preg_match( "/MSIE/", $_SERVER["HTTP_USER_AGENT"] ) ) {
	header( 'Content-Disposition: attachment; filename="' . rawurlencode( basename($file_path) ) . '"' );
} else {
	header( 'Content-Disposition: attachment; filename*=UTF-8\'\'' . rawurlencode( basename($file_path) )
										 . '; filename="' . rawurlencode( basename($file_path) ) . '"' );
}

OCP\Response::disableCaching();
header('Content-Length: '.filesize($file_path));

OC_Util::obEnd();

@ob_end_clean();
$handle = fopen($file_path, 'rb');
if ($handle) {
    $chunkSize = 8192; // 8 kB chunks
    while (!feof($handle)) {
        echo fread($handle, $chunkSize);
        flush();
    }
}
