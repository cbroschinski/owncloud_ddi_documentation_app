<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();
$username = OCP\USER::getUser();

if (!isset($_GET['uid'])) {
	OCP\JSON::error(array('message' => 'uid must be specified!'));
	exit();
}

if (!is_numeric($_GET['uid'])) {
    OCP\JSON::error(array('message' => 'uid is no numeric value!'));
    exit();
}
$uid = $_GET['uid'];

try {
    if (!Container::has_access($username, $uid)) {
        OCP\JSON::error(array('message' => 'Access to container (uid '. $uid .') denied for current user'));
        exit();
    }
}
catch (Exception $e) {
    OCP\JSON::error(array('message' => 'Could not determine access permissions to container '. $uid .', message: ' . $e->getMessage()));
    exit();
}



$stmt = OCP\DB::prepare( 'SELECT container_name, displayname, creation_date, lm_user, lm_date, lm_type FROM `*PREFIX*ddi_units` WHERE `uid` = ?');
$result = $stmt->execute(array($uid));
$data = $result->fetchRow();

if (isset($data['creation_date'])) {
    //$data['creation_date'] = date(DATE_COOKIE, $data['creation_date']);
    $data['creation_date'] = OCP\Util::formatDate($data['creation_date']);
}
if (isset($data['lm_date'])) {
    $data['lm_date'] = OCP\Util::formatDate($data['lm_date']);
}
if (isset($data['lm_user'])) {
    $data['lm_user'] = OCP\USER::getDisplayName($data['lm_user']);
}

OCP\JSON::success(array('data' => $data));
