<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();
$username = OCP\USER::getUser();

if (!isset($_GET['uid']) || !isset($_GET['field_id']) || !isset($_GET['file_id'])) {
	OCP\JSON::error(array('message' => 'uid, field_id and file_id must be specified!'));
	exit();
}

if (!is_numeric($_GET['uid'])) {
    OCP\JSON::error(array('message' => 'uid is no numeric value!'));
    exit();
}

$uid = trim($_GET['uid']);
$field_id = trim($_GET['field_id']);
$file_id = trim($_GET['file_id']);
$time = time();

//Check if user has access to container

if (!Container::has_access($username, $uid)) {
    OCP\JSON::error(array('message' => 'The current user does not have access to the requested unit (uid '. $parent_uid .')'));
    exit();
}

//Check if field_id is valid

$stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_units` WHERE `uid` = ?');
$result = $stmt->execute(array($uid));
$row = $result->fetchRow();
$container_type = $row['container_type'];

$template_dom = Util::generate_template($container_type);
$dom_nodes = $template_dom->getElementsByTagName('*');

foreach ($dom_nodes as $element) {
    if ($element->hasAttribute('data-file-field-id')) {     
        $element_id = $element->getAttributeNode('data-file-field-id')->value;
        if ($element_id === $field_id) {
            $file_path = OC\Files\Filesystem::getPath($file_id);
            if ($file_path == null) {
                OCP\JSON::error(array('message' => 'Invalid file id ' . $file_id ));
                exit();
            }
            $local_path = OC\Files\Filesystem::getLocalFile($file_path);
            $info = OC\Files\Filesystem::getFileInfo($file_path);
            $file_alias = $info['name'];
            try {
                $chksum = sha1_file($local_path);
                $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_files` WHERE `chksum` = ?');
                $result = $stmt->execute(array($chksum));
                if ($result->numRows() == 0) {
                    //chksum not yet in DB, copy the file and create an entry
                    $rebuild = Util::buildNotExistingFileName(SFB882_FILE_STORAGE_PATH, $file_alias);
                    $store_path = $rebuild['newpath'];
                    $file_name = $rebuild['newname'];
                    if (copy($local_path, $store_path)) {
                        chmod($store_path, 0700);
                        $stmt = OCP\DB::prepare('INSERT INTO `*PREFIX*ddi_files` (`parent_uid`,`field`,`chksum`,`file_name`,`file_alias`,`file_origin`,`creation_date`,`lm_user`,`lm_date`, `public`, `active`) VALUES(?,?,?,?,?,?,?,?,?,?,?)');
                        $result = $stmt->execute(array($uid, $field_id, $chksum, $file_name, $file_alias, $file_path, $time, $username, $time, 0, 1));
                        
                        $mime_type = \OC_Helper::getMimeType($store_path);
                        $mime_icon_path = \OC_Helper::mimetypeIcon($mime_type);
                        $file_size = \OC_Helper::humanFileSize(filesize($store_path));
                        $creation_date = OCP\Util::formatDate($time);
                        $data = array(
                            'chksum' => $chksum,
                            'file_alias' => $file_alias,
                            'file_origin' => $file_path,
                            'mime_type' => $mime_type,
                            'mime_icon_path' => $mime_icon_path,
                            'file_size' => $file_size,
                            'creation_date' => $creation_date,
                            'active' => 1,
                            'public' => 0
                        );                                                

                        OCP\JSON::success(array('message' => 'File storing successful', 'data' => $data));
                        exit();
                    }
                    else {
                        OCP\JSON::error(array('message' => 'Could not copy file from '.$localPath.' to '.$store_path.' - Operation aborted'));
                        exit();    
                    }
                }
                else {
                    //chksum already in DB. Get name of corresponding file and check if an identical entry exists
                    $row = $result->fetchRow();
                    $existing_file_name = $row['file_name'];
                    $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_files` WHERE `parent_uid` = ? AND `field` = ? AND `chksum` = ?');
                    $result = $stmt->execute(array($uid, $field_id, $chksum));
                    if ($result->numRows() == 0) {
                        //Identical entry not present, create a new one which points to the existing file
                        $stmt = OCP\DB::prepare('INSERT INTO `*PREFIX*ddi_files` (`parent_uid`,`field`,`chksum`,`file_name`,`file_alias`,`file_origin`,`creation_date`,`lm_user`,`lm_date`,`public`,`active`) VALUES(?,?,?,?,?,?,?,?,?,?,?)' );
                        $result = $stmt->execute(array($uid, $field_id, $chksum, $existing_file_name, $file_alias, $file_path, $time, $username, $time, 0, 1));
                        
                        $path = SFB882_FILE_STORAGE_PATH .'/'. $existing_file_name;
                        $mime_type = \OC_Helper::getMimeType($path);
                        $mime_icon_path = \OC_Helper::mimetypeIcon($mime_type);
                        $file_size = \OC_Helper::humanFileSize(filesize($path));
                        $creation_date = OCP\Util::formatDate($time);
                        $data = array(
                            'chksum' => $chksum,
                            'file_alias' => $file_alias,
                            'file_origin' => $file_path,
                            'mime_type' => $mime_type,
                            'mime_icon_path' => $mime_icon_path,
                            'file_size' => $file_size,
                            'creation_date' => $creation_date,
                            'active' => 1,
                            'public' => 0
                        );
                        OCP\JSON::success(array('message' => 'File storing successful (Link created)', 'data' => $data));
                        exit();
                    }
                    else {
                        $entry = $result->fetchRow();
                        OCP\JSON::error(array('message' => 'File not stored: Entry already exists', 'file_alias' => $entry['file_alias']));
                        exit();
                    }              
                }
            }
            catch (Exception $e) {
                OCP\JSON::error(array('message' => 'An exception occured while trying to store the file: '. $e->getMessage()));
                exit();
            }
        }
    }
}

OCP\JSON::error(array('message' => 'The requested field_id ('. $field_id .') could not be found in the according container template('. $container_type .')'));
exit();    

