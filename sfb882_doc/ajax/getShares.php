<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();
$username = OCP\USER::getUser();

$stmt = OCP\DB::prepare( 'SELECT uid, username, location, posx, posy, color FROM `*PREFIX*ddi_units` WHERE `permission` = ? AND `container_type` = ?' );
$result = $stmt->execute(array("shared", "top_level"));
$mygroups = OC_Group::getUserGroups($username);

$fellow_users = OC_Group::usersInGroups($mygroups);

//delete myself from the list
$self_keys = array_keys($fellow_users, $username);

foreach ($self_keys as $key) {
    unset($fellow_users[$key]);
}

$shared_units = array();

while ($row = $result->fetchRow()) {
	if (in_array($row['username'], $fellow_users)) {
        $shared_units[] = $row;
    }
}

OCP\JSON::success(array('data' => $shared_units));


