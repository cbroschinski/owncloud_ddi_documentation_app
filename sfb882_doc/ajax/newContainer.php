<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();

$username = OCP\USER::getUser();
$displayname = OCP\User::getDisplayName($username);


$container_name = $_GET['container_name'];
$container_type = $_GET['container_type'];
$posx = $_GET['posx'];
$posy = $_GET['posy'];
$color = $_GET['color'];
$location = $_GET['location'];
$time = time();

if (isset($_GET['research_type'])) {
    $research_type = $_GET['research_type'];
}
else {
    $research_type = 'mixed_with_pretest';
}

//if a parent_uid is given (subcontainer case), check if the the user has access to it
try {
	if (isset($_GET['parent_uid']) && $_GET['parent_uid'] > -1) {
		if (!is_numeric($_GET['parent_uid'])) {
		   OCP\JSON::error(array('message' => 'parent_uid is no numeric value!'));
			exit(); 
		}
		$parent_uid = $_GET['parent_uid'];
		if (!Container::has_access($username, $parent_uid)) {
			OCP\JSON::error(array('message' => 'The current user does not have access to the requested unit (uid '. $parent_uid .')'));
			exit();
		}
		
		//get username/displayname/permission from parent since it might belong to another user
		$stmt = OCP\DB::prepare( 'SELECT * FROM `*PREFIX*ddi_units` WHERE `uid` = ?');
		$result = $stmt->execute(array($parent_uid));
		$parent = $result->fetchRow();
		
		$insertid = Container::create_container($parent_uid, $parent['username'], $parent['displayname'], $container_name, $research_type, $location, $container_type, $posx, $posy, $color, $parent['permission'], $time);
	}
	else {  // No parent_uid given, new Container is a top-level unit (parent_uid = null)
		$insertid = Container::create_container(null, $username, $displayname, $container_name, $research_type, $location, $container_type, $posx, $posy, $color, 'private', $time);
	}
}
catch (Exception $e) {
	OCP\JSON::error(array('message' => 'An exception occured: '.$e->getMessage()));
}

$stmt = OCP\DB::prepare( 'SELECT * FROM `*PREFIX*ddi_units` WHERE `uid` = ?');
$result = $stmt->execute(array($insertid));

$data = $result->fetchRow();

OCP\JSON::success(array('data' => $data));
