<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();

//COPY files index.php 
// Load the files
if (isset($_GET['dir'])) {
    $dir = stripslashes($_GET['dir']);
}
else {
    $dir = "";
}

if (!\OC\Files\Filesystem::is_dir($dir . '/')) {
	OCP\JSON::error(array('message' => 'dir does not exist!'));
	exit();
}

$files = array();

$content = \OC\Files\Filesystem::getDirectoryContent($dir);
$freeSpace = \OC\Files\Filesystem::free_space($dir);
$needUpgrade = false;

foreach ($content as $i) {
	$i['date'] = OCP\Util::formatDate($i['mtime']);
	if ($i['type'] == 'file') {
		$fileinfo = pathinfo($i['name']);
		$i['basename'] = $fileinfo['filename'];
		if (!empty($fileinfo['extension'])) {
			$i['extension'] = '.' . $fileinfo['extension'];
		} else {
			$i['extension'] = '';
		}
	}
	$i['directory'] = $dir;
	$files[] = $i;
}

function fileCmp($a, $b) {
	if ($a['type'] == 'dir' and $b['type'] != 'dir') {
		return -1;
	} elseif ($a['type'] != 'dir' and $b['type'] == 'dir') {
		return 1;
	} else {
		return strnatcasecmp($a['name'], $b['name']);
	}
}

usort($files, "fileCmp");

// Make breadcrumb
$breadcrumb = array();
$pathtohere = '';
foreach (explode('/', $dir) as $i) {
	if ($i != '') {
		$pathtohere .= '/' . $i;
		$breadcrumb[] = array('dir' => $pathtohere, 'name' => $i);
	}
}

// make breadcrumb und filelist markup
$list = new OCP\Template('sfb882_doc', 'sfb882_doc_file_list', '');
$list->assign('files', $files);
$list->assign('baseURL', OCP\Util::linkTo('sfb882_doc', 'index.php') . '?dir=');
$list->assign('downloadURL', OCP\Util::linkToRoute('download', array('file' => '/')));
$list->assign('disableSharing', false);
$breadcrumbNav = new OCP\Template('sfb882_doc', 'sfb882_doc_file_breadcrumb', '');
$breadcrumbNav->assign('breadcrumb', $breadcrumb);
$breadcrumbNav->assign('baseURL', OCP\Util::linkTo('files', 'index.php') . '?dir=');

$permissions = OCP\PERMISSION_READ;
if (\OC\Files\Filesystem::isCreatable($dir . '/')) {
	$permissions |= OCP\PERMISSION_CREATE;
}
if (\OC\Files\Filesystem::isUpdatable($dir . '/')) {
	$permissions |= OCP\PERMISSION_UPDATE;
}
if (\OC\Files\Filesystem::isDeletable($dir . '/')) {
	$permissions |= OCP\PERMISSION_DELETE;
}
if (\OC\Files\Filesystem::isSharable($dir . '/')) {
	$permissions |= OCP\PERMISSION_SHARE;
}

// information about storage capacities
$storageInfo=OC_Helper::getStorageInfo($dir);
$maxUploadFilesize=OCP\Util::maxUploadFilesize($dir);
$publicUploadEnabled = \OC_Appconfig::getValue('core', 'shareapi_allow_public_upload', 'yes');
if (OC_App::isEnabled('files_encryption')) {
	$publicUploadEnabled = 'no';
}

OCP\Util::addscript('files', 'fileactions');
OCP\Util::addscript('files', 'files');
OCP\Util::addscript('files', 'keyboardshortcuts');
$tmpl = new OCP\Template('sfb882_doc', 'sfb882_doc_file_section', '');
$tmpl->assign('fileList', $list->fetchPage());
$tmpl->assign('breadcrumb', $breadcrumbNav->fetchPage());
$tmpl->assign('dir', \OC\Files\Filesystem::normalizePath($dir));
$tmpl->assign('permissions', $permissions);
$tmpl->assign('files', $files);
$tmpl->assign('allowZipDownload', intval(OCP\Config::getSystemValue('allowZipDownload', true)));
$tmpl->assign('usedSpacePercent', (int)$storageInfo['relative']);
$tmpl->assign('isPublic', false);
$tmpl->assign('publicUploadEnabled', $publicUploadEnabled);

$html_data = $tmpl->fetchPage();

//EO COPY files index.php

OCP\JSON::success(array('data' => $html_data));
