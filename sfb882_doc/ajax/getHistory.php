<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();
$username = OCP\USER::getUser();

if (!isset($_GET['uid'])) {
	OCP\JSON::error(array('message' => 'uid must be specified!'));
	exit();
}

if (!is_numeric($_GET['uid'])) {
    OCP\JSON::error(array('message' => 'uid is no numeric value!'));
    exit();
}
$uid = $_GET['uid'];

if (!Container::has_access($username, $uid)) {
    OCP\JSON::error(array('message' => 'Access to container (uid '. $uid .') denied for current user', 'data' => array()));
    exit();
}

$num_entries = 10;
if (isset($_GET['num_entries']) && is_numeric($_GET['num_entries']) && $_GET['num_entries'] > 0) {
	$num_entries = $_GET['num_entries'];
}

$history = Undo_Manager::get_history($uid, $username);

array_splice($history, $num_entries);

$l = OC_L10N::get('sfb882_doc');
for ($i = 0; $i < count($history); $i++) {
    $history[$i]['lm_date'] = OCP\Util::formatDate($history[$i]['lm_date']);
    $history[$i]['field'] = $l->t($history[$i]['field']);
    $history[$i]['lm_user'] = OCP\User::getDisplayName($history[$i]['lm_user']);
}

OCP\JSON::success(array('data' => $history));
