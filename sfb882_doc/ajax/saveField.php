<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();
$username = OCP\USER::getUser();

if (!isset($_POST['uid']) || !isset($_POST['field_id']) || !isset($_POST['value']) || !isset($_POST['action'])) {
	OCP\JSON::error(array('message' => 'uid, field_id, value and action must be specified!'));
	exit();
}

if (!is_numeric($_POST['uid'])) {
    OCP\JSON::error(array('message' => 'uid is no numeric value!'));
    exit();
}

$action_wl = array(
    'ADD', //Add an entry to a multi-value field
    'DELETE', //Delete an entry from a multi-value field
    'UPDATE' //Update an entry in a single or multi-value field
);

if (!in_array($_POST['action'], $action_wl)) {
    OCP\JSON::error(array('message' => $_POST['action'] . ' is no valid action!'));
    exit();
}

$uid = trim($_POST['uid']);
$field_id = trim($_POST['field_id']);
$value =trim($_POST['value']);
$action = $_POST['action'];
$time = time();

if ($field_id == "") {
    OCP\JSON::error(array('message' => 'field_id is empty!'));
    exit();
}

if (!Container::has_access($username, $uid)) {
    OCP\JSON::error(array('message' => 'The current user does not have access to the requested unit (uid '. $parent_uid .')'));
    exit();
}

$stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_units` WHERE `uid` = ?');
$result = $stmt->execute(array($uid));
$row = $result->fetchRow();
$container_type = $row['container_type'];

//Whitelisting: check the field_id against the classes in the according container template

$template_dom = Util::generate_template($container_type);

$dom_nodes = $template_dom->getElementsByTagName('*');

foreach ($dom_nodes as $element) {
    $type = "single";
    if ($element->hasAttribute('data-checkbox-area-id')) {     
        $element_id = $element->getAttributeNode('data-checkbox-area-id')->value;
        $type = "multi";
        $extensible = false;
        if ($element->hasAttribute('data-checkbox-extensible')) {
            if ($element->getAttributeNode('data-checkbox-extensible')->value === 'true') {
                $extensible = true;
            }
        }
    }
    else {
        $element_id = $element->getAttributeNode('data-text-input-id')->value;
    }
    if ($element_id == $field_id) {   
        try {
            if ($type === "single") {
                if ($action !== "UPDATE") {
                    OCP\JSON::error(array('message' => 'Invalid action "' .$action.'" on field '.$field_id.', single-value fields only accept the UPDATE action'));
                    exit(); 
                }
                //check if key/value pair already exists
                $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_data_'.$container_type.'` WHERE `parent_uid` = ? AND `field` = ?;');
                $result = $stmt->execute(array($uid, $field_id));     
                if ($result->numRows() == 0) {
                    if ($value != "") {   
                        $stmt = OCP\DB::prepare('INSERT INTO `*PREFIX*ddi_data_'.$container_type.'` (`parent_uid`,`field`,`value`,`lm_date`,`lm_user`,`lm_type`) VALUES(?,?,?,?,?,?)' );
                        $result = $stmt->execute(array($uid, $field_id, $value, $time, $username, 'INSERT'));
                        Undo_Manager::add_undo_value($container_type, $uid, $field_id, $value, $time, $username, 'INSERT');
                        OCP\JSON::success(array('message' => 'Insertion successful'));
                        exit();
                    }
                    else {
                       OCP\JSON::error(array('message' => 'Value was empty and no corresponding field exists => Database unchanged'));
                       exit(); 
                    }
                }
                else {
                    if ($value != "") {
                        $stmt = OCP\DB::prepare('UPDATE `*PREFIX*ddi_data_'.$container_type.'` SET `value`=?, `lm_date`=?, `lm_user`=?, `lm_type`=? WHERE `parent_uid`=? AND `field`=?' );
                        $result = $stmt->execute(array($value, $time, $username, 'UPDATE', $uid, $field_id));
                        Undo_Manager::add_undo_value($container_type, $uid, $field_id, $value, $time, $username, 'UPDATE');
                        OCP\JSON::success(array('message' => 'Update successful'));
                        exit();
                    }
                    else {
                        $stmt = OCP\DB::prepare('DELETE FROM `*PREFIX*ddi_data_'.$container_type.'` WHERE `parent_uid`=? AND `field`=?' );
                        $result = $stmt->execute(array($uid, $field_id));
                        OCP\JSON::success(array('message' => 'Deletion successful'));
                        exit();
                    }
                }
            }
            else if ($type === "multi") {
                //check if key/value pair already exists
                $stmt = OCP\DB::prepare('SELECT * FROM `*PREFIX*ddi_data_'.$container_type.'` WHERE `parent_uid` = ? AND `field` = ? AND `value` = ?;');
                $result = $stmt->execute(array($uid, $field_id, $value));     
                if ($action === "DELETE") {
                    if ($result->numRows() > 0) {
                        //value exists in DB -> delete it
                        $stmt = OCP\DB::prepare('DELETE FROM `*PREFIX*ddi_data_'.$container_type.'` WHERE `parent_uid` = ? AND `field` = ? AND `value` = ?;');
                        $result = $stmt->execute(array($uid, $field_id, $value));  
                        OCP\JSON::success(array('message' => 'Deletion successful'));
                        exit();         
                    }
                    else {
                        OCP\JSON::error(array('message' => 'Could not delete value "'. $value .'" - not found in database'));
                        exit(); 
                    }
                }
                else if ($action === "ADD") {
                    //value already exists -> Abort
                    if ($result->numRows() > 0) {
                        OCP\JSON::error(array('message' => 'Could not add value "'. $value .'" to database - entry already exists'));
                        exit();
                    }   
                    else {
                        //check if checkbox field is either hardcoded in the according template OR the checkbox area is extensible
                        if ($extensible || Util::get_element_by_attribute_value($element, 'data-checkbox-id', $value) !== null) {
                            $stmt = OCP\DB::prepare('INSERT INTO `*PREFIX*ddi_data_'.$container_type.'` (`parent_uid`,`field`,`value`,`lm_date`,`lm_user`,`lm_type`) VALUES(?,?,?,?,?,?)' );
                            $result = $stmt->execute(array($uid, $field_id, $value, $time, $username, 'INSERT'));
                            Undo_Manager::add_undo_value($container_type, $uid, $field_id, $value, $time, $username, 'INSERT');
                            OCP\JSON::success(array('message' => 'Insertion successful'));
                            exit();
                        }
                        else {
                            //Deny
                            OCP\JSON::error(array('message' => 'Could not add value "'. $value .'" to database - element is not hardcoded in template and parent is not marked extensible'));
                            exit();
                        }
                    } 
                }
            }
        }
        catch (PDOException $e) {
            OCP\JSON::error(array('message' => 'An exception occured while accessing the database: '.$e->getMessage()));
        }
    }
}

OCP\JSON::error(array('message' => 'The requested field_id ('. $field_id .') could not be found in the according container template('. $container_type .')'));
exit();


