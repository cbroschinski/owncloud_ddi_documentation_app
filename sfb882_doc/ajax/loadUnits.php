<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();

$username = OCP\USER::getUser();

$location_wl = array(
'trash' => array('`status` = ?  AND `parent_uid` = ?', array('in_trash')),
'internal' => array('(`location` = ? OR `location` = ? OR `location` = ?) AND `status` = ? AND `parent_uid` = ?', array('central_pane', 'fixed', 'sidebar', 'active')), //Extend these arrays after parent_uid was checked
'top_level' => array('`username` = ? AND (`location` = ? OR `location` = ?) AND `status` = ?', array($username, 'workbench', 'share', 'active'))
);

$values = array();
$location_selector = '';

if (!isset($_GET['location'])) {
    OCP\JSON::error(array('message' => 'No location scope specified!'));
    exit();
}
$key = $_GET['location'];
if ($key == 'trash' || $key == 'internal') {
    if (!isset($_GET['parent_uid'])) {
        OCP\JSON::error(array('message' => 'location requires a parent_uid!'));
        exit();
    }
    if (!is_numeric($_GET['parent_uid'])) {
        OCP\JSON::error(array('message' => 'parent_uid is no numeric value!'));
        exit();
    }
    $location_wl[$key][1][] = $_GET['parent_uid'];
}

if (!array_key_exists($key, $location_wl)) {       
    OCP\JSON::error(array('message' => 'Invalid location "' . $key . '"'));
    exit();
}

$location_selector .= $location_wl[$key][0];
$values = array_merge($values, $location_wl[$key][1]);

$stmt = OCP\DB::prepare( 'SELECT uid, location, container_type, container_name, research_type, posx, posy, color FROM `*PREFIX*ddi_units` WHERE '. $location_selector);
$result = $stmt->execute($values);

$data = array();

while ($row = $result->fetchRow()) {
    if (Container::has_access($username, $row['uid'])) {
        $data[] = $row;
    }	
}

OCP\JSON::success(array('data' => $data));
