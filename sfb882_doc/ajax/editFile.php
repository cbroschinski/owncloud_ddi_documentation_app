<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();
$username = OCP\USER::getUser();

$action_wl = array('SET_ACTIVE', 'SET_INACTIVE', 'SET_PUBLIC', 'SET_PRIVATE', 'SET_DESCRIPTION', 'DELETE');

if (!isset($_GET['uid']) || !isset($_GET['field_id']) || !isset($_GET['chksum']) || !isset($_GET['action'])) {
	OCP\JSON::error(array('message' => 'uid, field_id, chksum and action must be specified!'));
	exit();
}

if (!is_numeric($_GET['uid'])) {
    OCP\JSON::error(array('message' => 'uid is no numeric value!'));
    exit();
}

$uid = trim($_GET['uid']);
$field_id = trim($_GET['field_id']);
$chksum = trim($_GET['chksum']);
$action = trim($_GET['action']);

if (!in_array($action, $action_wl)) {
    OCP\JSON::error(array('message' => $action.' is no valid action!'));
    exit();
}

if ($action == 'SET_DESCRIPTION') {
    if (isset($_GET['payload'])) {
        $desc = $_GET['payload'];
    }
    else {
        $desc = '';
    }
}

$time = time();

//Check if user has access to container

if (!Container::has_access($username, $uid)) {
    OCP\JSON::error(array('message' => 'The current user does not have access to the requested unit (uid '. $uid .')'));
    exit();
}

try {
    if ($action == 'DELETE') {
        $stmt = OCP\DB::prepare('DELETE FROM `*PREFIX*ddi_files` WHERE `parent_uid`=? AND `field`=? AND `chksum`=?');
        $result = $stmt->execute(array($uid, $field_id, $chksum));
        OCP\JSON::success(array('message' => 'Deletion successful'));
        exit();
    }
    else {
        if ($action == 'SET_DESCRIPTION') {
            $stmt = OCP\DB::prepare('UPDATE `*PREFIX*ddi_files` SET `description`=? WHERE `parent_uid`=? AND `field`=? AND `chksum`=?');
            $result = $stmt->execute(array($desc, $uid, $field_id, $chksum));
        }
        else if ($action == 'SET_ACTIVE') {
            $stmt = OCP\DB::prepare('UPDATE `*PREFIX*ddi_files` SET `active`=? WHERE `parent_uid`=? AND `field`=? AND `chksum`=?');
            $result = $stmt->execute(array(1, $uid, $field_id, $chksum));
        }
        else if ($action == 'SET_INACTIVE') { //inactivation will also implicitly make the file private
            $stmt = OCP\DB::prepare('UPDATE `*PREFIX*ddi_files` SET `active`=?, `public`=? WHERE `parent_uid`=? AND `field`=? AND `chksum`=?');
            $result = $stmt->execute(array(0, 0, $uid, $field_id, $chksum));
        }
        else if ($action == 'SET_PUBLIC') { //Only active files can be set public/private
            $stmt = OCP\DB::prepare('UPDATE `*PREFIX*ddi_files` SET `public`=? WHERE `parent_uid`=? AND `field`=? AND `chksum`=? AND `active`=?');
            $result = $stmt->execute(array(1, $uid, $field_id, $chksum, 1));
        }
        else if ($action == 'SET_PRIVATE') {
            $stmt = OCP\DB::prepare('UPDATE `*PREFIX*ddi_files` SET `public`=? WHERE `parent_uid`=? AND `field`=? AND `chksum`=? AND `active`=?');
            $result = $stmt->execute(array(0, $uid, $field_id, $chksum, 1));
        }
        $stmt = OCP\DB::prepare('SELECT public, active, description FROM `*PREFIX*ddi_files` WHERE `parent_uid`=? AND `field`=? AND `chksum`=?');
        $result = $stmt->execute(array($uid, $field_id, $chksum));
        $row = $result->fetchRow();
        OCP\JSON::success(array('message' => 'Operation successful', 'data' => $row));
        exit();
    }
}
catch (PDOException $e) {
    OCP\JSON::error(array('message' => 'An exception occured while accessing the database: '.$e->getMessage()));
}
