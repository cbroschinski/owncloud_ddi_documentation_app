<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();
$username = OCP\USER::getUser();

if (!isset($_GET['uid']) || !isset($_GET['posx']) || !isset($_GET['posy'])) {
	OCP\JSON::error(array('message' => 'uid, posx and posy must be specified!'));
	return;
}

if (!is_numeric($_GET['uid'])) {
    OCP\JSON::error(array('message' => 'uid is no numeric value!'));
    exit();
}
$uid = $_GET['uid'];
$posx = $_GET['posx'];
$posy = $_GET['posy'];
$time = time();

$stmt = OCP\DB::prepare('UPDATE `*PREFIX*ddi_units` SET `location`=?, `posx`=?, `posy`=?, `lm_date`=?, `lm_user`=?, `lm_type`=? WHERE `uid`=? AND `username`=?');
$result = $stmt->execute(array('share', $posx, $posy, $time, $username, 'SHARE', $uid, $username));

//recursively update permissions
try {
    Util::recursive_property_update($username, $uid, 'permission', 'shared');
    OCP\JSON::success();
}
catch (Exception $e){
	OCP\JSON::error(array('message' => 'Error while applying recursive permission update: ' . $e->getMessage()));
}
