<?php
/**
    Copyright (C) 2015 Christoph Broschinski <broschinski@uni-bielefeld.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
OCP\JSON::callCheck();
OCP\JSON::checkAppEnabled('sfb882_doc');
OCP\JSON::checkLoggedIn();
$username = OCP\USER::getUser();

if (!isset($_GET['uid'])) {
    OCP\JSON::error(array('message' => 'uid must be specified!'));
	exit();
}

if (!is_numeric($_GET['uid'])) {
    OCP\JSON::error(array('message' => 'uid is no numeric value!'));
    exit();
}

$uid = $_GET['uid'];

if (!Container::has_access($username, $uid)) {
    OCP\JSON::error(array('message' => 'The current user does not have access to the requested unit (uid '. $uid .')'));
    exit();
}

$research_types_wl = array('qualitative_with_pretest', 'quantitative_with_pretest', 'mixed_with_pretest', 'qualitative_without_pretest', 'quantitative_without_pretest', 'mixed_without_pretest');

if (!isset($_GET['action'])) {
    OCP\JSON::error(array('message' => 'An edit action must be specified (MOVE, RECOLOR, RENAME or SET_RESEARCH_TYPE)!'));
    exit();  
}

if ($_GET['action'] === "MOVE") {
    if (!isset($_GET['posx']) || !isset($_GET['posy'])) {
        OCP\JSON::error(array('message' => "MOVE requires a 'posx' and a 'posy' argument!"));
        exit();  
    }
    if (!Util::is_px_position($_GET['posx'])) {
        OCP\JSON::error(array('message' => "posx ('".$_GET['posx']."') is not a valid position!"));
        exit(); 
    }
    if (!Util::is_px_position($_GET['posy'])) {
        OCP\JSON::error(array('message' => "posy ('".$_GET['posy']."') is not a valid position"));
        exit(); 
    }
    $db_query = "`posx`=?, `posy`=?";
    $db_params = array($_GET['posx'], $_GET['posy'], $uid);
}
else if ($_GET['action'] === "RECOLOR") {
    if (!isset($_GET['color'])) {
        OCP\JSON::error(array('message' => "RECOLOR requires a 'color' argument!"));
        exit();  
    }
    if (!Util::is_RGB_color($_GET['color'])) {
        OCP\JSON::error(array('message' => "color ('".$_GET['color']."') is no valid RGB color!"));
        exit(); 
    }
    $db_query = "`color`=?";
    $db_params = array($_GET['color'], $uid);
}
else if ($_GET['action'] === "RENAME") {
    if (!isset($_GET['name'])) {
        OCP\JSON::error(array('message' => "RENAME requires a 'name' argument!"));
        exit();  
    }
    $db_query = "`container_name`=?";
    $db_params = array($_GET['name'], $uid);
}
else if ($_GET['action'] === "SET_RESEARCH_TYPE") {
    if (!isset($_GET['research_type'])) {
        OCP\JSON::error(array('message' => "SET_RESEARCH_TYPE requires a 'research_type' argument!"));
        exit();  
    }  
    if (!in_array($_GET['research_type'], $research_types_wl)) {
        OCP\JSON::error(array('message' => "Illegal argument for SET_RESEARCH_TYPE! (Possible values: ". implode(", ",$research_types_wl) .")"));
        exit();
    }
    $db_query = "`research_type`=?";
    $db_params = array($_GET['research_type'], $uid);
}
else if ($_GET['action'] === "SET_RESEARCH_TYPE_RECURSIVE") {
    if (!isset($_GET['research_type'])) {
        OCP\JSON::error(array('message' => "SET_RESEARCH_TYPE_RECURSIVE requires a 'research_type' argument!"));
        exit();  
    }
    if (!in_array($_GET['research_type'], $research_types_wl)) {
        OCP\JSON::error(array('message' => "Illegal argument for SET_RESEARCH_TYPE_RECURSIVE! (Possible values: ". implode(", ",$research_types_wl) .")"));
        exit();
    }
    try {
        $results = Util::recursive_property_update($username, $uid, 'research_type', $_GET['research_type']);
        OCP\JSON::success(array('data' => $results));
        exit();
    }
    catch (Exception $e){
        OCP\JSON::error(array('message' => 'Error while applying recursive research_method update: ' .$e->getMessage()));
        exit();
    } 
}
else {
    OCP\JSON::error(array('message' => 'Unknown Action "'.$_GET['action'].'"'));
	exit();
}

$stmt = OCP\DB::prepare('UPDATE `*PREFIX*ddi_units` SET '.$db_query.' WHERE `uid`=?');
$result = $stmt->execute($db_params);

if ($result) {
	OCP\JSON::success();
}
else {
	OCP\JSON::error(array('message' => 'Error while updating database!'));
}
